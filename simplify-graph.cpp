//Graph structure solely for removing isolated nodes
//Removes all isolated nodes, renumbers vertices from 0, n-1
//Directed graphs only

#include<vector>
#include<string>
#include "logger.cpp"
#include<sstream>

namespace simpgraph {

   typedef uint32_t node_id;

   using namespace std;

   class simplifyNode {
   public:
      node_id id;
      vector< simplifyNode* > OUTneis;
      vector< simplifyNode* > INneis;
      vector< float > OUTnei_weights;
      vector< float > INnei_weights;
   };

   
   class simplifyGraph {
   public:
      node_id n;
      Logger logg;
      vector< simplifyNode* > adjList; 

      //directed
      void add_edge_directed( node_id from, node_id to, float wht = 1.0 ) {
	 if (from == to)
	    return;
	 
	 if (!vector_ctns( adjList[from]->OUTneis, adjList[to] ) ) {
	    adjList[from]->OUTneis.push_back( adjList[to] );
	    adjList[from]->OUTnei_weights.push_back( wht );
	 }
	 if (!vector_ctns( adjList[to]->INneis, adjList[from] ) ) {
	    adjList[to]->INneis.push_back( adjList[from] );
	    adjList[to]->INnei_weights.push_back( wht );
	 }
      }

      void add_edge_undirected( node_id from, node_id to, float wht = 1.0 ) {
	 add_edge_directed( from, to, wht );
	 add_edge_directed( to, from, wht );
      }

      void remove_isolates() {
	 vector< simplifyNode* > newAdjList;
	 for (node_id i = 0; i < n; ++i) {
	    if (!(adjList[i]->OUTneis.empty() && adjList[i]->INneis.empty()))
	       newAdjList.push_back( adjList[i] );
	 }

	 adjList.swap( newAdjList );
	 n = static_cast<node_id>( adjList.size() );
      }

      void renumber_vertices() {
	 for (node_id i = 0; i < n; ++i) {
	    adjList[i]->id = i;
	 }
      }

      size_t countEdges() {
	 size_t m = 0;
	 for (node_id i = 0; i < n; ++i) {
	    m += adjList[i]->OUTneis.size();
	    m += adjList[i]->INneis.size();
	 }

	 m = m / 2;

	 return m;
      }

      size_t count_inEdges() {
	 size_t m = 0;
	 for (node_id i = 0; i < n; ++i) {
	    m += adjList[i]->INneis.size();
	 }

	 return m;
      }

      void reweight( float delta = 0.7 ) {
	 //uniform_real_distribution< float > dist( 0.0, 1.0 );
	 //random_device rd;
	 //mt19937 gen( rd() );

	 for (node_id i = 0; i < n; ++i) {
	    vector< simplifyNode* >& INneis = adjList[i]-> INneis;
	    vector< float >& whts = adjList[i]-> INnei_weights;
	    for (size_t j = 0; j < whts.size(); ++j) {
	       whts[j] = delta; /// INneis.size();
	    }
	 }
      }

      void write_bct_files( size_t lmax, string dirName ) {
	 size_t N = n + lmax * n; // will be adding lmax new nodes for each original
	 
	 size_t M = count_inEdges() + lmax * n; //will be adding one edge for each new node

	 //write original number of nodes and lmax to info.txt
	 string ofinfo = dirName + "/info.txt";
	 ofstream ofinf( ofinfo.c_str() );
	 ofinf << n << ' ' << lmax << endl;
	 ofinf.close();
	 
	 //write edge list
	 string ofname = dirName + "/network.txt";
	 ofstream ofile( ofname.c_str() );

	 ofile << N << ' ' << M << endl;
	 for (size_t i = 0; i < n; ++i) {
	    for (size_t j = 0; j < adjList[i]->INneis.size(); ++j) {
	       ofile << adjList[i]->INneis[j]->id << ' ' << i
		     << ' ' << adjList[i]->INnei_weights[ j ] << endl;
	    }
	 }

	 size_t nn = n;
	 for (size_t i = 0; i < n; ++i) {
	    for (size_t j = 0; j < lmax; ++j) {
	       float wht = 0.5 / lmax;
	       ofile << nn << ' '
		     << i  << ' '
		     << wht << endl;
	       ++nn;
	    }
	 }

	 ofile.close();
	 //edge list written
	 //write benefits file
	 string benName = dirName + "/benefits.txt";
	 string costName = dirName + "/costs.txt";
	 ofstream oBen ( benName.c_str() );
	 ofstream oCost ( costName.c_str() );
	 for (size_t i = 0; i < n; ++i) {
	    oBen << "1\n";
	    oCost << N + 1 << endl;
	 }

	 for (size_t i = n; i < N; ++i) {
	    oBen << "0\n";
	    oCost << "1\n";
	 }
	 oBen.close();
	 oCost.close();
      }
      
      /*
       * Writes full adjacency list to
       * directed binary format of sampleGraph
       */
      void write_bin( string fname ) {
	 ofstream ifile ( fname.c_str(), ios::out | ios::binary );

	 ifile.write( (char*) &n, sizeof( node_id ) );
	 double preprocessTime = 0.0;
	 ifile.write( (char*) &preprocessTime, sizeof(double) );

	 size_t ss;
	 node_id nei_id;
	 float w;
	 for ( unsigned i = 0; i < n; ++i ) {
	    ss = adjList[i]->OUTneis.size();
	    ifile.write( (char*) &ss, sizeof( size_t ) );

	    for (unsigned j = 0; j < ss; ++j) {
	       nei_id = adjList[i]->OUTneis[j]->id;
	       w = adjList[i]->OUTnei_weights[ j ];
	       ifile.write( (char*) &nei_id, sizeof( node_id ) );
	       ifile.write( (char*) &w, sizeof( float ) );
	    }

	    ss = adjList[i]->INneis.size();
	    ifile.write( (char*) &ss, sizeof( size_t ) );

	    for (unsigned j = 0; j < ss; ++j) {
	       nei_id = adjList[i]->INneis[j]->id;
	       w = adjList[i]->INnei_weights[ j ];
	       ifile.write( (char*) &nei_id, sizeof( node_id ) );
	       ifile.write( (char*) &w, sizeof( float ) );
	    }
	 }
      }

      /*
       * Writes 
       * directed edge list format (standard weighted edge list)
       *
       */
      void write_edge_list( string fname ) {
	 ofstream ofile ( fname.c_str(), ios::out );
	 ofile << n;
	 ofile << " ";
	 size_t m = count_inEdges();
	 ofile << m << endl;
	 
	 for (node_id i = 0; i < n; ++i) {
	    for (size_t j = 0; j < adjList[i]->INneis.size(); ++j) {
	       ofile << (adjList[i]->INneis[j])-> id << ' ' << i << ' ' << (adjList[i]->INnei_weights[j]) << endl;
	    }
	 }
      }
	 
      void read_edge_list( string fname ) {
	 logg << "Reading edge list from file " << fname << endL;
	 ifstream ifile ( fname.c_str() );

	 string sline;
	 stringstream ss;
	 unsigned line_number = 0;
	 bool weighted;
	 bool directed;
	 while (getline( ifile, sline ) ) {
	    if (sline[0] != '#') {
	       ss.clear();
	       ss.str( sline );

	       if (line_number == 0) {
		  ss >> this->n;
		  this->n = 0;
		  ss >> weighted;
		  if (weighted) {
		     logg << "Graph is weighted." << endL;
		     
		  } else {
		     logg << "Graph is unweighted." << endL;
		  }
		  ss >> directed;
		  if (directed) {
		     logg << "Graph is directed." << endL;
		     
		  } else {
		     logg << "Graph is undirected." << endL;
		  }
	       }
	       else {
		  //have an edge on this line
		  unsigned from,to; float wht = 1.0;
		  ss >> from;
		  ss >> to;
		  if (weighted)
		     ss >> wht;
		  simplifyNode* emptyNode;
		  while (to >= this->n) {
		     emptyNode = new simplifyNode;
		     emptyNode->id = this->n;
		     adjList.push_back( emptyNode );
		     ++this->n;
		  }

		  while (from >= this->n) {
		     emptyNode = new simplifyNode;
		     emptyNode->id = this->n;
		     adjList.push_back( emptyNode );
		     ++this->n;
		  }

		  if (directed) {
		     add_edge_directed( from, to, wht );
		  } else {
		     add_edge_undirected( from, to, wht );
		  }

	       }

	       ++line_number;
	    }
	 }
      
	 ifile.close();
      }

      

   };
}
