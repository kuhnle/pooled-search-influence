#include "simplify-graph.cpp"
#include <iostream>
#include <string>

using namespace std;

using namespace simpgraph;

int main( int argc, char** argv ) {
   if (argc < 4) {
      cout << "Usage: " << argv[0] << " <input file> <output directory> <lmax>" << endl;
      cout << "Input must be graph in format suitable for simplifyGraph::read_edge_list" << endl;
      cout << "First line should be: <Node estimate (can be 0)> <isWeighted> <isDirected>"  << endl;
      cout << "Each line thereafter: <from_id> <to_id> <weight (float)> (Weights are ignored, this version reweights edges)"  << endl;
      cout << "Graph is simplified, then formatted for BCT and written in output dir" << endl;
      exit(1);
   }
   
  string inputGraphName ( argv[1] );
  string sLmax ( argv[3] );
  size_t lmax = stoi( sLmax );
  simplifyGraph g;

  g.read_edge_list( inputGraphName );
  Logger logg;
  logg << "Removing isolates..." << endL;
  g.remove_isolates();
  logg << "Renumbering vertices..." << endL;
  g.renumber_vertices();
  logg << "Reweighting (in weights only)..." << endL;
  g.reweight();
  cout << "Writing BCT file..." << endl;
  string outputDir( argv[2] );
  g.write_bct_files( lmax, outputDir );

  return 0;
}
