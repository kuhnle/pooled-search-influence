#ifndef results_handler
#define results_handler

#include <map>
#include <vector>
#include <iomanip>

using namespace std;

class simpleHandler {
public:
   map< double, vector<double> > data;
      
   void init( double name ) {
      auto it = data.find( name );
      if (it == data.end()) {
	 vector< double > tmp;
	 data[ name ] = tmp;
      }
   }

   void add( double name, double val ) {
      (data[ name ]).push_back( val );
   }

   void print( ostream& os, bool printStdDev = true ) {
      for (auto it = data.begin();
	   it != data.end();
	   ++it ) {
      //Print name
	 os << it->first << '\t';
	 vector<double>& tmp = it->second;
	 double mean = 0.0;
	 for (size_t i = 0; i < tmp.size(); ++i) {
	    mean += tmp[i];
	 }
	 mean = mean / tmp.size();
	 os << setw(25) << mean;

	 if (printStdDev) {
	    double stdDev = 0.0;
	    for (size_t i = 0; i < tmp.size(); ++i) {
	       stdDev += (tmp[i]- mean)*(tmp[i]- mean);
	    }
	    if (tmp.size() > 1) {
	       stdDev = stdDev / ( tmp.size() - 1 );
	       stdDev = sqrt( stdDev );
	       os << setw(25) << stdDev ;
	    } else {
	       os << setw(25) << 0.0 ;
	    }
	 }
	 os << endl;
      }
   }

} simpleResults;

#endif