CPP=g++
CPPFLAGS=-std=c++11 -Wall -Ofast 
DEBUGFLAGS=-std=c++11 -Wall -Og -g
CPLEX_INCLUDE=-I/opt/ibm/ILOG/CPLEX_Studio1271/cplex/include -I/opt/ibm/ILOG/CPLEX_Studio1271/concert/include
CPLEX_LIB=-L/opt/ibm/ILOG/CPLEX_Studio1271/cplex/lib/x86-64_linux/static_pic -L/opt/ibm/ILOG/CPLEX_Studio1271/concert/lib/x86-64_linux/static_pic -lilocplex -lcplex -lconcert -lm -lpthread
CPLEX_FLAGS=${CPLEX_INCLUDE} ${CPLEX_LIB} -DNDEBUG -DILOSTRICTPOD -DIL_STD

ifeq ($(opt),1)
CPPFLAGS+=$(CPLEX_FLAGS)
CPPFLAGS+=-DOPTVAR
endif

all: im_ft im_ic
preproc: preprocess.cpp simplify-graph.cpp util.cpp
	${CPP} preprocess.cpp -o preproc  ${CPPFLAGS}
im_ic: im_ic.cpp sample-graph-IC.cpp util.cpp generic_alg.cpp
	${CPP} -DMODIC -DLINEARINCENTIVE -DBOOSTING im_ic.cpp -o im_ic  ${CPPFLAGS} -l pthread
im_ft: im_t.cpp thresh-graph.cpp util.cpp generic_alg.cpp
	${CPP} im_t.cpp -o im_ft  ${CPPFLAGS} -l pthread
debug: im_t.cpp thresh-graph.cpp util.cpp
	${CPP} im_t.cpp -o imft_debug  ${DEBUGFLAGS} -l pthread
debug_ic: im_ic.cpp sample-graph.cpp util.cpp
	${CPP} im_ic.cpp -o imic_debug  ${DEBUGFLAGS} -l pthread

pnw: prepNweight.cpp util.cpp simplify-graph.cpp
	${CPP} prepNweight.cpp -o pnw ${CPPFLAGS}
clean:
	rm im_ic im_ft
