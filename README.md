# `pim`

Fast pareto optimization.

## Building

The source should compile with any recent version of GNU `g++` using the GNU `make` utility.
To build the necessary binaries run:
```sh
$ make preproc im_ft im_ic
```

## Preparing Data

A sample network is provided (`data/ba.1k.50`) in edge list format.
To convert the edge list into binary format, run
```sh
$ cd data
$ ../preproc ba.1k.50 ba.1k.50.bin
```

## Common parameters for `im_ft`, `im_ic`

```
-g <graph filename in binary format>
-k <total budget>
-G (run Stochastic-Greedy)
-M (run BLPO)
-L (run BLPO+)
-Q (run POSS)
-x <max number of threads (default: 1)>
-o <output filename>
-N <number of repetitions>
```

## Output

During execution, each algorithm prints
a line to the terminal indicating the best solution found so far and
the time at which it was found, normalized by the greedy values:
```
<evals / (greedy evals) > <value / (greedy value)>.
```

If an output file is specified,
the format is as follows. Each row is of the form
```<frac of Greedy iter> <mean normalized value> <stdDev>```
where the mean and standard deviation are taken over
independent repetitions of the algorithm.

## Example

```sh
$ cd data
$ ../preproc ba.1k.50 ba.1k.50.bin
$ ../im_ft -g ba.1k.50.bin -k 10 -L #Runs BLPO+ with k = 10
```
