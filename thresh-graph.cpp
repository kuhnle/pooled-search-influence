#ifndef SAMPLE_GRAPH_CPP
#define SAMPLE_GRAPH_CPP
#include <list>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>
#include <unordered_set>
#include <thread>
#include <iomanip>
#include <cmath>
#include <utility>
#include <queue>
#include <set>
#include <thread>
#include <mutex>
#include "logger.cpp"
#include "util.cpp"

using namespace std;


namespace sampgraph {
   
   random_device rd;
   mt19937 gen( rd() );
   //mt19937 gen;
   uniform_real_distribution< float > threshDist(0.0,1.0);
   
   typedef uint32_t node_id;
   bool mycompare( const node_id& a, const node_id& b ) {
      return a < b;
   }  

   class mpair {
   public:
      uint32_t id;
      double value;
   };

   class comparePair {
   public:
      bool operator() ( mpair a, mpair b ) {
	 return (a.value < b.value);
      }
   };
   
   //directed graph
   
   /*
    * Edge classes
    */
   class tinyEdge {
   public:
      
      uint32_t target;
      float weight;
     
      tinyEdge() {
	 target = 0;
      }

      tinyEdge( node_id nid, float w = 1.0) {
	 target = nid; 
	 weight = w;
      }

      tinyEdge( const tinyEdge& rhs ) {
	 target = rhs.target;
	 weight = rhs.weight;
      }
   };

   bool operator<( const tinyEdge& a, const tinyEdge& b ) {
      return a.target < b.target;
   }

   class Sample {
   public:
      vector< float > thresholds;
      vector< node_id > path;
      bool alive;
      //vector< bool > node_membership;
   };

   typedef uint32_t sampleIndex;

   class sLookup {
   public:
      sampleIndex s;
      uint32_t p;
   };
   
   //Node class
   class tinyNode {
   public:
      vector< tinyEdge > OUTneis;
      vector< tinyEdge > INneis;
      vector< sLookup > mySamps;
      
      //For each sample containing the node, the index of the sample is
      //mapped to the FIRST occurence of the node in the sample
      map< sampleIndex, uint32_t > mapSamps;
      
      /*
       * edges are preprocessed into p, s.t.
       * p[i] has threshold t[i]
       * t[i] < t[i+1]
       * for query q
       * if i* is smallest such that
       * t[i*] >= q,
       * then edge from here to p[i*] is live
       */
      vector< node_id > p;
      vector< float > t; 
      
      tinyNode () { }
     
      tinyNode ( const tinyNode& rhs ) {
	 OUTneis.assign( rhs.OUTneis.begin(), rhs.OUTneis.end() );
	 INneis.assign( rhs.INneis.begin(), rhs.INneis.end() );
	 mySamps.assign( rhs.mySamps.begin(), rhs.mySamps.end() );
	 mapSamps = rhs.mapSamps;
	 p.assign( rhs.p.begin(), rhs.p.end() );
	 t.assign( rhs.t.begin(), rhs.t.end() );
	 
      }

      // vector< tinyEdge >::iterator incident( node_id& out ) {
      // 	 vector< tinyEdge >::iterator it = neis.begin();
      // 	 do {
      // 	    if (it->getId() == out)
      // 	       return it;
      // 	    ++it;
      // 	 } while (it != neis.end());

      // 	 return it;
      // }
   };

   float pInit = 0.0;
   
   class myGraph {
   public:
      vector< tinyNode > adjList;
      vector< uint16_t > nodeLevels;
      vector< Sample > samps;
      uint16_t lmax;
      unsigned n;
      unsigned m;
      Logger logg;

      double preprocessTime;
      

      vector<double> increments;

      
      myGraph() {
	 n = 0;
	 m = 0;
	 lmax = 0;
      }

      void read_bin( string fname ) {
	 this->adjList.clear();
	 this->m = 0;
	 this->n = 0;
      
	 ifstream ifile ( fname.c_str(), ios::in | ios::binary );
	 unsigned n;
	 ifile.read( (char*) &n, sizeof( node_id ) );
	 ifile.read( (char*) &preprocessTime, sizeof(double) );
      
	 this->n = n;

	 init_empty_graph();
	 size_t ss;
	 tinyEdge temp;
	 node_id nei_id;
	 float w;

	 for ( unsigned i = 0; i < n; ++i ) {

	    ifile.read( (char*) &ss, sizeof( size_t ) );

	    adjList[i].OUTneis.assign( ss, temp );
	    this->m += ss;
	    for (unsigned j = 0; j < ss; ++j) {
	       ifile.read( (char*) &nei_id, sizeof( node_id ) );
	       ifile.read( (char*) &w, sizeof( float ) );
	       adjList[i].OUTneis[j].target = nei_id;
	       adjList[i].OUTneis[j].weight = w;
	    }

	    ifile.read( (char*) &ss, sizeof( size_t ) );
	    
	    adjList[i].INneis.assign( ss, temp );
	    this->m += ss;
	    for (unsigned j = 0; j < ss; ++j) {
	       ifile.read( (char*) &nei_id, sizeof( node_id ) );
	       ifile.read( (char*) &w, sizeof( float ) );
	       adjList[i].INneis[j].target = nei_id;
	       adjList[i].INneis[j].weight = w;
	    }
	 }

      }
      
      myGraph( const myGraph& h ) {
	 adjList.assign( h.adjList.begin(), h.adjList.end() );
	 n = h.n;
	 m = h.m;
	 lmax = h.lmax;
	 preprocessTime = h.preprocessTime;
	 samps.assign( h.samps.begin(), h.samps.end() );
      }

      void assign( const myGraph& h ) {
	 adjList.assign( h.adjList.begin(), h.adjList.end() );
	 n = h.n;
	 m = h.m;
	 preprocessTime = h.preprocessTime;
	 lmax = h.lmax;
	 samps.assign( h.samps.begin(), h.samps.end() );
      }

      void clearSamples() {
	 samps.clear();
	 for (node_id v = 0; v < n; ++v) {
	    adjList[v].mySamps.clear();
	    adjList[v].mapSamps.clear();
	 }
      }

      void init_empty_graph() {
	 tinyNode emptyNode;
	 adjList.assign(n, emptyNode);
      }
      
      void print( ostream& os ) {
	 for (size_t i = 0; i <adjList.size(); ++i) {
	    os << i << endl;
	    os << "OUT:" << endl;
	    for (size_t j = 0; j < adjList[i].OUTneis.size(); ++j) {
	       os << adjList[i].OUTneis[j].target << ' '
		  << adjList[i].OUTneis[j].weight << ' ';
	    }
	    os << endl;
	    os << "IN:" << endl;
	    for (size_t j = 0; j < adjList[i].INneis.size(); ++j) {
	       os << adjList[i].INneis[j].target << ' '
		  << adjList[i].INneis[j].weight << ' ';
	    }
	    os << endl;
	 }
      }

   };
    
}

#endif
