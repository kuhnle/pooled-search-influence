#ifdef FAST
#include "specialized_gain.cpp"
#else
#include "generic_alg.cpp"
#endif
#include "sample-graph-IC.cpp"
#include "results_handler.cpp"
#include <iostream>
#include <string>
#include <unistd.h>
#include <chrono>

#ifdef OPTVAR
#include <ilcplex/ilocplex.h>
#endif

using namespace sampgraph;
using namespace std;

typedef tArgs< sampleGraph > Args;

#ifdef OPTVAR
size_t greedy( Args& args );
size_t opt( Args& args ) {
   (*args.logg) << SECT << "CPLEX IP SOLVER" << endL;
   (*args.logg) << INFO;
   size_t n = args.g.n;
   IloEnv env;
   IloModel model(env);
   IloNumVarArray var(env);
   IloRangeArray con(env);
   vector< Sample >& samps = args.g.samps;

   //xi
   for (size_t i = 0; i < n; ++i ) {
      var.add( IloNumVar( env, 0.0, 1.0, ILOINT ) );
   }

   //yi
   for (size_t i = 0; i < samps.size(); ++i) {
      var.add( IloNumVar( env, 0.0, 1.0, ILOINT ) );
   }

   //Add cardinality constraint
   IloRangeArray c(env);
   c.add( IloRange( env, 0.0, args.k ) );
   for (size_t i = 0; i < n; ++i ) {
      c[ 0 ].setLinearCoef( var[i], 1.0 );
   }
   
   //Add coverage constraints 
   for (size_t i = 0; i < samps.size(); ++i) {
      c.add( IloRange( env, 0.0, IloInfinity ) );
      for (size_t j = 0; j < samps[i].node_membership.size(); ++j) {
	 if (samps[i].node_membership[j]) {
	    c[ i + 1 ].setLinearCoef( var[ j ], 1.0 );
	 }
      }
      c[ i + 1 ].setLinearCoef( var[ n + i ], -1.0 );
   }

   //max sum y_i
   IloObjective obj = IloMaximize( env );
   for (size_t i = 0; i < samps.size(); ++i) {
      obj.setLinearCoef( var[ n + i ], 1.0 );
   }

   model.add( obj );
   model.add( c );

   IloCplex cplex( model );

   cplex.setParam(IloCplex::IntParam::Threads, args.nThreads);
   cplex.setParam(IloCplex::IntParam::ParallelMode, -1 ); //Opportunistic
   cplex.solve();

   double objval = cplex.getObjValue();

   (*args.logg) << "OPT value: " << objval << endL;

   size_t valGreedy = greedy( args );
   (*args.logg) << "Greedy value: " << valGreedy << endL;
   (*args.logg) << "OPT / Greedy: " << objval / valGreedy << endL;


   if (args.outputFileName != "") {
      ofstream ofile;
      ofile.open( args.outputFileName.c_str() );
      ofile << "#OPT Greedy O/G\n";
      ofile << objval << '\t' << valGreedy << '\t' << objval/ valGreedy << endl;
      ofile.close();
   }
   
   return objval;
}

#endif

size_t greedy( Args& args ) {
   if (args.g.lmax != 1) {
      args.g.logg << WARN << "Cannot use BCT with lmax > 1" << endL;
      args.g.logg << "Setting lmax = 1." << endL;
      args.g.logg << INFO;
      args.g.lmax = 1;

      if (args.K > 0)
	 args.k = args.K;
   }

   size_t valS;
   vector <uint16_t > x;

   args.g.resetSamps();
   (*args.logg) << "Starting Max Coverage..." << endL;

   valS = args.g.max_covg(x, args.k );
   (*args.logg) << "Activation: " << args.g.avgAct( valS, args.g.samps.size() ) << endL;
   
   args.g.nodeLevels = x;

   return valS;
}



size_t func( size_t& nEvals, sampleGraph& g, vector< size_t >& ss ) {
   ++nEvals;

   return g.evalCvg( ss );
}

size_t funcOld( size_t& nEvals, sampleGraph& g, vector< bool >& ss ) {
   ++nEvals;

   return g.evalCvg( ss );
}

size_t margeGain( size_t& nEvals, sampleGraph& g, vector< bool >& ss, size_t v ) {
   ++nEvals;

   return g.sampsCovered( static_cast<node_id>(v) );
}

size_t margeGainBack( size_t& nEvals, sampleGraph& g, vector< bool >& ss, size_t v ) {
   ++nEvals;
   if (!ss[v])
      return 0;
   return g.sampsUncovered( ss, static_cast<node_id>(v) );
}

size_t greedyBackward( vector< vector< bool > >& pool,
		       vector< size_t >& vals,
		       size_t pos,
		       sampleGraph& g,
		       size_t& nEvals ) {
   vector< bool > ss = pool[ pos ];
   vector< node_id > ssIds;
   for (node_id v = 0; v < g.n; ++v) {
      if (ss[ v ] == 1) {
	 ssIds.push_back( v );
      }
   }

   size_t maxCov = 0;
   node_id maxV = 0;
   for (size_t i = 0; i < ssIds.size(); ++i) {
      node_id u = ssIds[ i ];
      ss[ u ] = false;
	    	    
      //compute value of new ss
      size_t cov = funcOld( nEvals, g, ss );
      if (cov > maxCov) {
	 maxCov = cov;
	 maxV = u;
      }
      ss[u] = true;
   }

   size_t newVal = maxCov;
   ss[ maxV ] = false;
  if (newVal > vals[ pos - 1 ] ) {
      pool[ pos - 1 ] = ss;
      vals[ pos - 1 ] = newVal;
   }
	 
   return vals[ pos - 1 ];
}


size_t greedyForward( vector< vector< bool > >& pool,
		      vector< size_t >& vals,
		      size_t pos,
		      sampleGraph& g,
		      size_t& nEvals ) {
   vector<bool > ss = pool[ pos ];
   
   size_t maxInc = 0;
   size_t maxV = 0;
   size_t inc;
   size_t valS;
   for (node_id v = 0; v < g.n; ++v) {
      if (!ss[v]) {
	 ss[ v ] = true;
	    
	 inc = funcOld( nEvals, g, ss );
	 
	 if ( inc > maxInc ) {
	    maxInc = inc;
	    maxV = v;
	 }

	 ss[ v ] = false;
      }
   }

   ss[ maxV ] = true;
   
   valS = maxInc;

   if ( pos == pool.size() - 1 ) {
      pool.push_back( ss );
      vals.push_back( valS );
   } else {
      size_t newVal = valS;
      if (newVal > vals[ pos + 1 ] ) {
	 pool[ pos + 1 ] = ss;
	 vals[ pos + 1 ] = newVal;
      }
   }

   return vals[ pos + 1 ];
}



size_t runGpareto( Args& args ) {
   
   if (args.g.lmax != 1) {
      args.g.lmax = 1;

      if (args.K > 0)
	 args.k = args.K;
   }
   
   bool boutput = false;
   ofstream ofile;
   if (args.outputFileName != "") {
      ofile.open( args.outputFileName.c_str() );
      boutput = true;
   }

   size_t valGreedy = greedy( args );

   vector< vector< bool > > pool;
   vector< size_t > vals;
   vector<bool> ss;
   {

      vector< bool > empty( args.g.n, false );
      pool.push_back( empty );
      vals.push_back( 0 );
      
   }

   size_t k = args.k;
   size_t maxSize = 2 * k; 
   size_t n = args.g.n;
   size_t T = args.g.n * args.k * 50;
   
   size_t nEvals = 0;
   uniform_int_distribution< size_t > dist( 0, maxSize );
   uniform_int_distribution< size_t > coinToss( 0, 1 );
   vector< bool > forward( maxSize + 1, false );
   vector< bool > backward( maxSize + 1, false );
   forward[ maxSize ] = true;
   backward[ 0 ] = true;
   size_t pos, toss, cov;
	 
   size_t maxCovered = 0;
   size_t backImps = 0;
   vector< size_t > notConverged;
   //for (size_t t = 0; t < T; ++t) {
   while (nEvals < T) {
      notConverged.clear();
      for (size_t i = 0; i < pool.size(); ++i) {
	 if (!forward[i]) {
	    notConverged.push_back( i );

	 } else {
	    if (!backward[i]) {
	       notConverged.push_back( i );
	    }
	 }
      }

      if (notConverged.size() == 0) {
	 (*args.logg) << "Pareto converged." << endL;
	 break;
      }
      
    
	    
      do {
	 //pos = dist( gen2 );
	 pos = 0;

      } while (pos >= notConverged.size());

      pos = notConverged[ pos ];

      if (forward[pos])
	 toss = 0;
      else
	 toss = 1;
      
      if (toss == 1) {
	 if (!args.randomSelection)
	    forward[ pos ] = true;

	 size_t valNext;
	 if (pos + 1 < vals.size())
	    valNext = vals[ pos + 1 ];
	 else
	    valNext = 0;

	 cov = greedyForward( pool, vals, pos, args.g, nEvals );
 	 
	 if (cov > valNext) {
	    if (pos + 1 < maxSize)
	       forward[ pos + 1 ] = false;
	    backward[ pos + 1 ] = false;

	    if (cov > maxCovered) {
	       if (pos + 1 <= k) {
		  maxCovered = cov;
		  ss = pool[ pos + 1 ];

		  cerr << "\r                                                 \r";
		  double prog = nEvals / (static_cast<double>(k) * n);
		  cerr << prog << '\t' << maxCovered / ( (double) valGreedy)
		       << ' ' << pool.size() << ' ' << backImps;

		  
	       }

	    }
	  
	 }
      } else {
	 if (!args.randomSelection)
	    backward[pos] = true;
	 
	 size_t valPrev = vals[ pos - 1 ];
	 cov = greedyBackward( pool, vals, pos, args.g, nEvals );

	 if ( cov  > valPrev ) {
	    forward[ pos - 1 ] = false;
	    if (pos - 1 > 0)
	       backward[ pos - 1 ] = false;
		  
	    ++backImps;


	    if (cov > maxCovered) {
	       if (pos - 1 <= k) {
		  maxCovered = cov;
		  ss = pool[ pos - 1 ];

		  cerr << "\r                                                 \r";
		  double prog = nEvals / (static_cast<double>(k) * n);
		  cerr << prog << '\t' << maxCovered / ( (double) valGreedy)
		       << ' ' << pool.size() << ' ' << backImps;
	       }
	    }


	 }
	       
      }

      if (boutput) {
	 //	 if (nEvals % args.k == 0) {
	 double prog = nEvals / (static_cast<double>(k) * n);
	 simpleResults.init( prog );
	 simpleResults.add( prog, maxCovered / ( (double) valGreedy) );
	 //ofile << args.k << '\t' << prog << '\t' << maxCovered / ( (double) valGreedy)
	 //<< ' ' << pool.size() << ' ' << backImps << endl;
	    //}
      }
   }

   //return
   size_t totCovered = funcOld( nEvals, args.g, ss );

   (*args.logg) << "Pareto value: " << totCovered << endL;
   if (boutput)
      ofile.close();
   return totCovered;

   
}

// void mutate( vector< bool >& ss,
// 	     vector< size_t >& ssId,
// 	     size_t n,
// 	     binomial_distribution< size_t >& binDist,
// 	     uniform_int_distribution< size_t >& allDist,
// 	     mt19937& gen2 ) {
//    size_t nMutate = binDist( gen2 );

//    vector< size_t > mutateIdx;

//    if (nMutate >= n)
//       nMutate = 0;
   
//    for (size_t i = 0; i < nMutate; ++i) {
//       size_t idx;
//       do {
// 	 idx = allDist( gen2 );
//       } while ( vcontains( mutateIdx, idx ) );

//       mutateIdx.push_back( idx );
//    }

//    for (size_t j = 0; j < nMutate; ++j) {
//       size_t& i = mutateIdx[ j ];
//       if ( ss[i] ) {
// 	 ss[i] = false;
// 	 erase_from_vector( ssId, i );
//       } else {
// 	 ss[i] = true;
// 	 ssId.push_back( i );
//       }
//    }
   
//    // for (size_t i = 0; i < n; ++i) {
//    //    rnum = uni( gen2 );
//    //    if (rnum < prob) {
//    // 	 if ( ss[i] ) {
//    // 	    ss[i] = false;
//    // 	    erase_from_vector( ssId, i );
//    // 	 } else {
//    // 	    ss[i] = true;
//    // 	    ssId.push_back( i );
//    // 	 }
//    //    }
//    // }
// }


// bool replace( vector< bool >& ss,
// 	      vector< size_t >& ssId,
// 	      vector< vector< bool > >& pool,
// 	      vector< vector< size_t > >& poolId,
// 	      vector< size_t >& vals, size_t& tmpPos, size_t& tmpVal,
// 	      size_t maxSize, size_t& nEvals, sampleGraph& g ) {
//    tmpPos = ssId.size();
//    tmpVal = 0;

//    if (tmpPos <= maxSize) {
//       size_t valPos = vals[ tmpPos ];
//       tmpVal = func( nEvals, g, ssId );
//       if (tmpVal > valPos) {
// 	 vals[ tmpPos ] = tmpVal;
// 	 pool[ tmpPos ].swap( ss );
// 	 poolId[ tmpPos ].swap( ssId );
// 	 return true;
//       }
//    }
//    return false;
// }

// size_t runQian( Args& args ) {
//    random_device rd;
//    mt19937 gen2 ( rd() );
//    //run Greedy first, for comparison
//    size_t valGreedy;
//    if (args.valGreedy > 0.0)
//       valGreedy = args.valGreedy;
//    else {
//       valGreedy = greedy( args );
//       args.valGreedy = valGreedy;
//    }
   
//    vector< vector< bool > > pool;
//    vector< vector< size_t > > poolId;
//    vector< size_t > vals;
//    size_t maxVal = 0;
//    size_t pos;
//    size_t maxSize = 2 * args.k;
//    uniform_int_distribution< size_t > dist( 0, maxSize );
//    uniform_int_distribution< size_t > distAll( 0, args.g.n - 1 );
//    binomial_distribution< size_t > binDist( args.g.n, 1.0 / args.g.n );

//    bool boutput = false;
//    ofstream ofile;
//    if (args.outputFileName != "") {
//       ofile.open( args.outputFileName.c_str() );
//       boutput = true;
//    }
   
//    {

//       vector< bool > empty( args.g.n, false );
//       pool.assign( maxSize + 1, empty );
//       vals.assign( maxSize + 1, 0 );
//       vector< size_t > emptyId;
//       poolId.assign( maxSize + 1, emptyId );
      
//    }

//    size_t T = args.g.n * args.k * args.T;
//    size_t nEvals = 0;
//    size_t tmpPos, tmpVal;

//    double outTenth = 0.0;
//    size_t iter = 0;
//    while (nEvals <= T) {
   
//       pos = dist( gen2 );

//       vector< bool > ss = pool[ pos ];
//       vector< size_t > ssId = poolId[ pos ];
//       mutate( ss, ssId, args.g.n, binDist, distAll, gen2 );

//       replace( ss,
// 	       ssId,
// 	       pool,
// 	       poolId,
// 	       vals,
// 	       tmpPos,
// 	       tmpVal,
// 	       maxSize,
// 	       nEvals,
// 	       args.g );
      
//       if ( ( tmpVal > maxVal ) && (tmpPos <= args.k) ) {
// 	 maxVal = tmpVal;
// 	 cerr << "\r                                                 \r";
// 	 double prog = nEvals / (static_cast<double>(args.k) * args.g.n);
// 	 cerr << prog << '\t' << maxVal / ( (double) valGreedy)
// 	      << ' ' << pool.size();

	 
//       }

//       if (boutput) {
// 	 bool outputNow = false;
// 	 double prog = nEvals / (static_cast<double>(args.k) * args.g.n);
// 	 while (outTenth < prog) {
// 	    outTenth += 0.1;
// 	    outputNow = true;
// 	 }
// 	 if (outputNow) {
// 	    mtx.lock();
// 	    simpleResults.init( outTenth - 0.1 );
// 	    simpleResults.add( outTenth - 0.1, maxVal / ( (double) valGreedy) );
// 	    mtx.unlock();
// 	    //ofile << args.k << '\t' << iter << '\t' << prog << '\t' << outTenth - 0.1 << '\t' 
// 	    //	  << maxVal / ( (double) valGreedy)
// 	    //	  << ' ' << args.alg << ' ' << args.ell << '\n';
// 	 }
//       }

//       ++iter;
//    }

//    cerr << '\n';
   
//    (*args.logg) << "Qian Pareto: " << maxVal << endL;

//    if (boutput)
//       ofile.close();
   
//    return maxVal;
// }

void runGparetoOld( Args& args ) {
   if (args.g.lmax != 1) {
      args.g.lmax = 1;

      if (args.K > 0)
	 args.k = args.K;
   }

   args.g.preprocess();
   args.g.clearSamples();

   size_t initSamps = 50000;

   (*args.logg) << "Samples: " << initSamps << endL;
   vector< uint16_t > vZero( args.g.n, 0 );
   vector< uint16_t > vSol;
   args.g.increaseSamples( vZero, initSamps, false, args.nThreads );

   double outAct;
   vector <uint16_t > x;

   (*args.logg) << "Starting Greedy..." << endL;
   outAct = args.g.max_covg(x, args.k );
   (*args.logg) << "Greedy Activation: " << outAct << endL;
   args.g.resetSamps();
   (*args.logg) << "Starting Pareto..." << endL;

   outAct = args.g.greedy_pareto(x, args.k, outAct );
   (*args.logg) << "Pareto Activation: " << outAct << endL;

   args.g.nodeLevels = x;
}


void testSample( Args& args ) {
   vector< uint16_t > x;
   if (args.testVectorFName != "") {
      ifstream ifs( args.testVectorFName.c_str() );
      unsigned tmp;
      ifs >> tmp;
      args.g.lmax = tmp;
      x.assign( args.g.n , 0 );

      while (ifs >> tmp ) {
	 unsigned index = tmp;
	 ifs >> tmp;
	 x[index] = tmp;
      }
      
      ifs.close();
      args.g.preprocess();
   } else {
      x.assign( args.g.nodeLevels.begin(), args.g.nodeLevels.end() );
   }

   size_t count = 0;
   for (size_t i = 0; i < x.size(); ++i) {
      count += x[i];
      //if (x[i] > 0) {
      //cout << (unsigned) i << ' ' << (unsigned) x[i] << endl;
      //}
   }
   (*args.logg) << "Size of x: " << count << endL;
   (*args.logg) << "lmax: " << args.g.lmax << endL;
   double epsi = 0.01;
   double delta = 0.01;

   double lambda = exp(1) - 2.0;
   double upsilon = 4.0 * lambda * log(2.0 / delta) / pow(epsi, 2);
   double upsilon_1 = 1.0 + (1.0 + epsi) * upsilon;

   int sum = 0;
   int nSamps = 0;
   int goal = ceil(upsilon_1);

#define BATCH_SIZE 500000
   args.g.clearSamples();
   while(sum < goal) {
       nSamps += BATCH_SIZE;
       sum += args.g.increaseSamples(x, BATCH_SIZE, true, args.nThreads);
   }

   (*args.logg) << "Samples: " << nSamps << endL;
   args.estInfluence = args.g.n * static_cast<double>(sum) / nSamps;
   (*args.logg) << args.estInfluence  << endL;
   
}

void print_help() {
   cout << "Options: " << endl;
  cout << "-g <graph filename in binary format>" << endl
     //<< "-p <outfilename> (print graph info and edge list to file and exit)" << endl
       << "-k <total budget>" << endl
       << "-G (run GREEDY)" << endl
       << "-P (run GPARETO)" << endl
       << "-x <max number of threads (default: 1)> " << endl
       << "-q (quiet mode, suppress logging)" << endl
       << "-t (independently estimate the influence of solution)" << endl
       << "-o <output filename> (write solution vector to specified file)" << endl;
}

void parseArgs( int argc, char** argv, Args& arg ) {
   int c;
  extern char *optarg;

  if (argc == 1) {
    print_help();
    exit( 2 );
  }

  string sarg;
  while ((c = getopt( argc, argv, ":g:DK:OT:k:p:vx:PHQS:EBMN:GLl:q:to:d:ri:e:b:h") ) != -1) {
    switch(c) {
    case 'b':
       arg.logFileName.assign( optarg );
       break;
    case 'o':
       arg.outputFileName.assign( optarg );
       break;
    case 't':
       arg.testInfluence = true;
       break;
    case 'p':
       sarg.assign( optarg );
       arg.p = stod( sarg );
       break;
    case 'q':
       sarg.assign( optarg );
       arg.ew = stod( sarg );
       break;
    case 'h':
       arg.lazySearchHeuristic = true;
       break;
    case 'e':
       sarg.assign( optarg );
       arg.epsi = stod( sarg );
       break;
    case 'x':
       sarg.assign( optarg );
       arg.nThreads = stoi( sarg );
       break;
    case 'l':
       sarg.assign( optarg );
       arg.ell = stoi( sarg );
       break;
    case 'k':
       sarg.assign( optarg );
       arg.k = stoi( sarg );
       break;
    case 'i':
       sarg.assign( optarg );
       arg.restartInterval = stod( sarg );
       break;
    case 'r':
       arg.randomSelection = true;
       break;
    case 'K':
       sarg.assign( optarg );
       arg.K = stoi( sarg );
       break;
    case 'g':
      //graph specification
      arg.graphFileName.assign( optarg );
      break;
    case 'G':
       arg.alg = Algs::GREEDY;
       break;
    case 'Q':
       arg.alg = Algs::QIAN;
       break;
    case 'P':
       arg.alg = Algs::GPARETO;
       break;
    case 'L':
       arg.alg = Algs::LAPS;
       break;
    case 'M':
       arg.alg = Algs::LAPS;
       arg.randomSelection = true;
       break;
    case 'O':
       arg.alg = Algs::OPT;
       break;
    case 'N':
       sarg.assign( optarg );
       arg.N = stoi( sarg );
       break;
    case 'S':
       sarg.assign( optarg );
       arg.S = stoi( sarg );
       break;
    case 'T':
       sarg.assign( optarg );
       arg.T = stoi( sarg );
       break;
    case '?':
      print_help();
      exit( 0 );
      break;
    }
  }

  arg.g.lmax = 1;

  
  if (arg.K > 0) {
     arg.k = arg.K * arg.g.lmax;
     
  }

  if (arg.logFileName != "") {
     arg.ofLog = new ofstream( arg.logFileName.c_str() );
     arg.logg = new Logger( INFO, *arg.ofLog, true );
  } else {
     arg.logg = new Logger();
  }

  arg.g.logg.enabled = false;
}

void readGraph( Args& args ) {
   (*args.logg) << SECT << "INITIALIZATION" << endL;
   (*args.logg) << INFO;
   (*args.logg) << "Loading graph " << args.graphFileName << "..." << endL;
   args.g.read_bin( args.graphFileName, args.ew );
   args.g.preprocess();
   args.g.clearSamples();

   if (file_exists( "oracle.txt" )) {
      (*args.logg) << "Loading oracle from file..." << endL;
      args.g.loadOracle( "oracle.txt" );
      args.g.resetSamps();
   } else {
      (*args.logg) << "Generating oracle..." << endL;
      size_t initSamps = args.S;
      (*args.logg) << "Samples: " << initSamps << endL;
      //vector< uint16_t > vZero( args.g.n, 0 );
      //vector< uint16_t > vSol;
      //args.g.increaseSamples( vZero, initSamps, false, args.nThreads );
      args.g.RIS( initSamps );
      args.g.resetSamps();
      args.g.saveOracle( "oracle.txt" );
   }
}

void runAlg( Args& args );

void runAlgMaster( Args& args ) {
   if (args.nThreads > 1 && args.alg != OPT) {
      (*args.logg) << "Initializing " << args.nThreads << " threads..." << endL;
      vector< Args > threadArgs( args.nThreads, args );
      thread* workThreads = new thread[ args.nThreads ];
      for (size_t i = 0; i < args.nThreads; ++i) {
	 
	 if (i < args.nThreads - 1) {
	    threadArgs[i].N = args.N / args.nThreads;
	 } else {
	    threadArgs[i].N = args.N - (args.nThreads - 1)* args.N / args.nThreads;
	 }
	 workThreads[i] = thread( runAlg,
				  ref( threadArgs[i] ) );
				 
      }


      for (size_t i = 0; i < args.nThreads; ++i) {
	 workThreads[i].join();
      }

      delete [] workThreads;
      
   } else {
      runAlg( args );
   }
}

void runAlg( Args& args ) {
   

   for (size_t i = 0; i < args.N; ++i) {
      (*args.logg) << "Repetition = " << i << endL;
      switch (args.alg) {
      case Algs::GREEDY:
	 lazy_greedy< sampleGraph >( args, func );
	 break;

#ifdef OPTVAR
      case Algs::OPT:
	 opt( args );
	 break;
#endif
	 
      case Algs::GPARETO:
	 runGpareto( args );
	 break;

      case Algs::LAPS:
	 rps_main< sampleGraph >( args, func );
     
	 break;

      case Algs::QIAN:
	 run_qian< sampleGraph >( args, func );
	 break;
      default:
	 (*args.logg) << "Unrecognized algorithm." << endL;
	 break;
      }

   }

  
}

void outputResults( Args& args, ostream& os ) {
   os << "#params:\n"
      << "#input=" << args.graphFileName << endl
      << "#k=" << args.k << endl
      << "#ell=" << args.ell << endl
      << "#alg=" << args.alg << endl
      << "#Reps=" << args.N << endl
      << "#epsi=" << args.epsi << endl
      << "#results: <frac of Greedy iter> <mean> <stdDev>" << endl;

   simpleResults.print( os, true );
}

int main(int argc, char** argv) {
   Args args;
   //   parseArgs( argc, argv, args );
   parse_args< sampleGraph >( argc, argv, args );
   readGraph( args );
   runAlgMaster( args );
   
   if (args.outputFileName != "") {
      //ofstream of( args.outputFileName.c_str(), ofstream::out | ofstream::app );
      ofstream of( args.outputFileName.c_str(), ofstream::out | ofstream::app );
      outputResults( args, of );
      of.close();
   } else {
      //outputResults( args, cout );
   }

   if (args.testInfluence) {
      (*args.logg) << "Influence of solution vector: " << endL;
      testSample( args );
   }

   return 0;
}
