#ifndef UTIL_CPP
#define UTIL_CPP

#include <thread>
#include <vector>
#include <mutex>
#include <ctime>
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <fstream>
#include <string>
#if __cplusplus > 199711L
#include <numeric>
#include <cstdio>
#endif

inline bool file_exists (const std::string& name) {
   std::ifstream f(name.c_str());
   return f.good();
}

template< typename TContainer, typename TArgs, typename TWorker >
void parallelWork( TContainer& v, TArgs& args,
		   TWorker& worker,
		   size_t nThreads,
		   size_t blocksize = 0) {
   if (blocksize == 0) {
      // //divide work evenly into threads
      // auto itBegin = v.begin();
      // auto itEnd = itBegin;

      // if (nThreads > v.size())
      // 	 nThreads = v.size();
  
      // std::thread* wThreads = new std::thread[ nThreads ];
      // std::mutex mtx;  
      // for (size_t i = 0; i < nThreads; ++i) {
      // 	 if (i == nThreads - 1) {
      // 	    itEnd = v.end();
      // 	 } else {
      // 	    itEnd = itBegin;
      // 	    for (size_t j = 0; j < (v.size())/nThreads; ++j)
      // 	       ++itEnd;
      // 	 }

	 
      // 	 wThreads[i] = std::thread( &TWorker::run,
      // 				    itBegin,
      // 				    itEnd,
      // 				    std::ref( args ),
      // 				    std::ref( mtx )
      // 				    );

      // 	 itBegin = itEnd;
      // }

      // for (size_t i = 0; i < nThreads; ++i) {
      // 	 wThreads[i].join();
      // }
    
      // delete [] wThreads;
   } else {
      //Give each thread a block of size blocksize
      auto itBegin = v.begin();
      auto itEnd = itBegin;

      std::vector<typename TContainer::iterator > vThreadPts( 2*nThreads, itBegin );
      
      size_t totalWork = v.size();

      std::thread* wThreads = new std::thread[ nThreads ];
      std::mutex mtx;  
      size_t workDone = 0;
      std::vector< unsigned > thrDone( nThreads, 1 ); //1 = true, 0 = false
      std::vector< unsigned > thrTerm( nThreads, 0 );

      //create the threads. They do not start working yet though
      for (size_t i = 0; i < nThreads; ++i) {
	 wThreads[i] = std::thread( &TWorker::run,
      				    std::ref( vThreadPts[ 2*i ] ),
      				    std::ref( vThreadPts[ 2*i + 1 ] ),
      				    std::ref( args ),
      				    std::ref( mtx ),
				    std::ref( thrDone[ i ] ),
				    std::ref( thrTerm[ i ] )
      				    );
      }
      while (workDone < totalWork) {
	 //find first available thread
	 
      	 for (size_t i = 0; i < nThreads; ++i) {
	    if (thrDone[ i ] == 1) {
	       mtx.lock();
	       //give this thread another block
	       if ( totalWork - workDone < blocksize ) {
		  blocksize = totalWork - workDone;
	       }
	       itEnd = itBegin;
	       for (size_t j = 0; j < blocksize; ++j) {
		  ++itEnd;
	       }
	       
	       vThreadPts[ 2*i ] = itBegin;
	       vThreadPts[ 2*i + 1 ] = itEnd;
	       workDone += blocksize;
	       thrDone[i] = 0;
	       itBegin=itEnd;
	       mtx.unlock();
	       break;
	    }
	 }
	 
      }

      //all work is assigned
      //tell threads to terminate when they finish current batch
      mtx.lock();
      for (size_t i = 0; i < nThreads; ++i) {
	 thrTerm[i] = 1;
      }
      mtx.unlock();
      
      for (size_t i = 0; i < nThreads; ++i) {
	 wThreads[i].join();
      }
    
      delete [] wThreads;
   }
   
}


double elapsedTime( clock_t& t_start ) {
   return double(clock() - t_start) / CLOCKS_PER_SEC;
}

template<typename T>
void print_vector( std::vector< T >& v, std::ostream& os = std::cout ) {
   for (size_t i = 0; i < v.size(); ++i) {
      os << v[i] << ' ';
   }
   os << std::endl;
}

template<typename T>
void print_vector_vector( std::vector< std::vector< T > >& v, std::ostream& os = std::cout ) {
   for (size_t i = 0; i < v.size(); ++i) {
      os << i << ": ";
      print_vector( v[i] );
   }
   os << std::endl;
}

template<typename T>
void erase_from_vector( std::vector< T >& v, T obj ) {
   for (auto it = v.begin(); it != v.end(); ++it) {
      if (*it == obj) {
	 v.erase( it );
	 break;
      }
   }
}

template<typename T>
bool vcontains( std::vector< T >& v, T obj ) {
   for (auto it = v.begin(); it != v.end(); ++it) {
      if (*it == obj) {
	 return true;
      }
   }

   return false;
}

template <typename T, typename Compare>
std::vector<std::size_t> sort_permutation(
					  const std::vector<T>& vec,
					  Compare& compare)
{
   std::vector<std::size_t> p(vec.size());
   std::iota(p.begin(), p.end(), 0);
   std::sort(p.begin(), p.end(),
	     [&](std::size_t i, std::size_t j){ return compare(vec[i], vec[j]); });
   return p;
}

template <typename T>
void apply_permutation_in_place(
				std::vector<T>& vec,
				const std::vector<std::size_t>& p)
{
   std::vector<bool> done(vec.size());
   for (std::size_t i = 0; i < vec.size(); ++i)
      {
	 if (done[i])
	    {
	       continue;
	    }
	 done[i] = true;
	 std::size_t prev_j = i;
	 std::size_t j = p[i];
	 while (i != j)
	    {
	       std::swap(vec[prev_j], vec[j]);
	       done[j] = true;
	       prev_j = j;
	       j = p[j];
	    }
      }
}


template <typename T>
size_t vector_ctns( std::vector<T>& vec, T item ) {
   for (std::size_t i = 0; i < vec.size(); ++i) {
      if (vec[i] == item) {
	 return i + 1;
      }
   }

   return 0;
}

#endif
