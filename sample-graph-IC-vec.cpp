#ifndef SAMPLE_GRAPH_CPP
#define SAMPLE_GRAPH_CPP
#include <list>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>
#include <unordered_set>
#include <thread>
#include <iomanip>
#include <cmath>
#include <utility>
#include <queue>
#include <set>
#include <thread>
#include <mutex>
#include "logger.cpp"
#include "util.cpp"

using namespace std;


namespace sampgraph {


   
   random_device rd;
   mt19937 gen( rd() );
   uniform_real_distribution< float > sampleDist(0.0,1.0);
   
   typedef uint32_t node_id;
   bool mycompare( const node_id& a, const node_id& b ) {
      return a < b;
   }  

   class mpair {
   public:
      uint32_t id;
      double value;
   };

   class comparePair {
   public:
      bool operator() ( mpair a, mpair b ) {
	 return (a.value < b.value);
      }
   };
   
   //directed graph
   
   /*
    * Edge classes
    */
   class tinyEdge {
   public:
      
      uint32_t target;
      float weight;
     
      tinyEdge() {
	 target = 0;
      }

      tinyEdge( node_id nid, float w = 1.0) {
	 target = nid; 
	 weight = w;
      }

      tinyEdge( const tinyEdge& rhs ) {
	 target = rhs.target;
	 weight = rhs.weight;
      }
   };

   bool operator<( const tinyEdge& a, const tinyEdge& b ) {
      return a.target < b.target;
   }

   /* 
    * weights[ i ] is of length
    * in-deg( nodes[ i ] ).
    * weights[ i ][ j ] is the sampled weight of
    * the j-th in-edge.
    * edge is "active" iff sampled weight <= actual edge weight
    *
    */
   class SampleStage {
   public:
      vector< node_id > nodes;
      //vector< float > nodeWeights;
      //vector< vector< float > > edgeWeights;
      //vector< vector< bool > > liveEdges;
   };
   
   class Sample {
   public:
      vector < SampleStage > stages;
      bool alive;
      
   };

   typedef uint32_t sampleIndex;

   class sLookup {
   public:
      sampleIndex s;
      uint32_t p;
   };

   bool operator==( const sLookup& lhs, const sLookup& rhs ) {
      return lhs.s == rhs.s;
   }
   
   //Node class
   class tinyNode {
   public:
      vector< tinyEdge > OUTneis;
      vector< tinyEdge > INneis;
      //For each sample containing the node, the index of the sample is
      //mapped to the stage in the sample that contains this node
      vector< sLookup > mySamps;

      tinyNode () { }
     
      tinyNode ( const tinyNode& rhs ) {
	 OUTneis.assign( rhs.OUTneis.begin(), rhs.OUTneis.end() );
	 INneis.assign( rhs.INneis.begin(), rhs.INneis.end() );
	 mySamps.assign( rhs.mySamps.begin(), rhs.mySamps.end() );
      }

      // vector< tinyEdge >::iterator incident( node_id& out ) {
      // 	 vector< tinyEdge >::iterator it = neis.begin();
      // 	 do {
      // 	    if (it->getId() == out)
      // 	       return it;
      // 	    ++it;
      // 	 } while (it != neis.end());

      // 	 return it;
      // }
   };

   float pInit = 0.0;
   
   float getLevelOffset( uint16_t& l, uint16_t& lmax ) {
      //return static_cast<float>( l ) / (2.0 * lmax);
      #ifdef LINEARINCENTIVE
      return pInit + (1.0 - pInit)* static_cast<float>( l ) / (lmax);
      #else
      return 2.0*l/lmax - ((1.0*l/lmax)*(1.0*l/lmax));
      #endif
      

      //return (-1.0 / (lmax * lmax ) )*(l)*(l - 2.0 * lmax);
      
      //return 1.0 - exp( -4.0 * l / lmax );
   }

   float getEdgeWeight( float& pInit, uint16_t& l, uint16_t& lmax ) {
      //return static_cast<float>( l ) / (2.0 * lmax);

      #ifdef BOOSTING
      return pInit + (1.0 - pInit)* static_cast<float>( l ) / (lmax);
      #else
      return pInit;
      #endif

      //return (-1.0 / (lmax * lmax ) )*(l)*(l - 2.0 * lmax);
      //return 2.0*l/lmax - ((1.0*l/lmax)*(1.0*l/lmax));
      //return 1.0 - exp( -4.0 * l / lmax );
   }
   
   size_t ext_add_samp( vector< uint16_t >& x,
			vector< tinyNode >& adjList,
			vector< Sample >& samps,
			uniform_int_distribution< node_id >& nodeDist,
			size_t sampleOffset,
			bool dry,
			uint16_t lmax,
			vector< vector< sLookup > >& Maps,
			uniform_real_distribution< float >& sampleDist,
			mt19937& gen ) {
      node_id n = adjList.size();
      Sample ns;
      vector< bool > node_membership( n, false ); //empty sample
      node_id target = nodeDist( gen );
      float samp;
      node_membership[ target ] = true;
      vector< node_id > nextLevel( 1, target );
      vector< node_id > nextLevel2;
      ns.alive = false;
      
      while( nextLevel.size() > 0 ) {
	 SampleStage nextStage;
	 for (size_t i = 0; i < nextLevel.size(); ++i ) {
	    node_id& curr = nextLevel[ i ];

	    //get a weight for this node
	    samp = sampleDist( gen );
	    if (!dry) {
	      nextStage.nodes.push_back( curr );
	      
	    }

	    vector< float > currEdgeWeights;
	    vector< bool > currLiveEdges;
	    if (samp <= getLevelOffset( x[ curr ], lmax )) {
	       //this node is alive
	       //hence so is the sample
	       ns.alive = true;
	       
	       //no point in tracing back from this node
	       if (!dry) {
		 continue;
	       } else {
		 return 1;
	       }
	       
	    }
	    

	    vector< tinyEdge >& in_neis = adjList[ curr ].INneis;
	    for (size_t j = 0; j < in_neis.size(); ++j) {
	       samp = sampleDist( gen );

	       if (samp <= getEdgeWeight( in_neis[ j ].weight, x[ curr ], lmax ) ) {
		  //this edge is live
		 if (!node_membership[ in_neis[ j ].target ] ) {
		   nextLevel2.push_back( in_neis[ j ].target );
		   node_membership[ in_neis[ j ].target ] = true;
		     
		 }
	       } else {

	       }
	       
	    }

	 }

	 nextLevel.swap( nextLevel2 );
	 nextLevel2.clear();
	 if (!dry)
	   ns.stages.push_back( nextStage );
      }

      if (!dry) {
	 //Add Sample to constituent nodes
	 for (size_t i = 0; i < ns.stages.size(); ++i) {
	    for (size_t j = 0; j < ns.stages[ i ].nodes.size(); ++j) {
	       //maps sampleID to the level
	       sLookup tmp;
	       tmp.s = samps.size() + sampleOffset;
	       tmp.p = i;
	       Maps[ ns.stages[i ].nodes[j] ].push_back( tmp ); //[ samps.size() + sampleOffset ] = i; 
	    }
	 }

	 //Add Sample to samps
	 samps.push_back( ns );
      }

      if (ns.alive)
	 return 1;

      return 0;
   }

   void thread_add_samps( vector< Sample >& samps,
			  size_t& nActive,
			  size_t nSamps,
			  bool dry,
			  size_t sampOffset,
			  vector< uint16_t >& x,
			  vector< vector< sLookup > >& myMaps,
			  vector< tinyNode >& adjList,
			  size_t lmax ) {
      nActive = 0;
      uniform_int_distribution< node_id > nodeDist( 0, adjList.size() - 1);
      uniform_real_distribution< float > sampleDist(0.0,1.0);
      random_device rd;
      mt19937 gen( rd() );
      for (size_t i = 0; i < nSamps; ++i) {
	 nActive += ext_add_samp( x,
				  adjList,
				  samps,
				  nodeDist,
				  sampOffset,
				  dry,
				  lmax,
				  myMaps,
				  sampleDist,
				  gen );
				 
      }

   }
   
   
   bool ext_update_samp( Sample& ss,
			 uint32_t level,
			 vector< tinyNode >& adjList,
			 uint16_t& lmax,
			 vector< uint16_t >& nodeLevels,
			 node_id& v,
			 uniform_real_distribution< float >& sampleDist,
			 mt19937& gen ) {
      //only need to consider samples becoming activated, not de-activated
      if (ss.alive == true) {
	 return true; //no change
      }
	 
      //find the node
      SampleStage& stage = ss.stages[ level ];
      size_t i = 0;
      while (i < stage.nodes.size()) {
	 if (stage.nodes[i] == v) {
	    break;
	 }
	 ++i;
      }

      if (i == stage.nodes.size()) {
	 cerr << "Node "
	      << v << " incorrectly reporting membership in sample "
	      << "..." << endl;
	 return ss.alive;
      } 

      float nodeWeight = sampleDist( gen );
      
      if ( nodeWeight <= getLevelOffset( nodeLevels[ v ], lmax ) ) {
	 return true; //sample is alive
      } 

      //vector< float >& currEdgeSamps = stage.edgeWeights[i];
      //vector< bool > currLiveEdges = stage.liveEdges[i];
      vector< node_id > nextLevel;
      vector< bool > node_membership( adjList.size(), false );
      for (size_t j = 0; j <= level; ++j) {
	 SampleStage& stagej = ss.stages[j];
	 for (size_t k = 0; k < stagej.nodes.size(); ++k) {
	    node_membership[ stagej.nodes[k] ] = true;
	 }
      }
      vector< tinyEdge >& inneis = adjList[ v ].INneis;
      for (size_t j = 0; j < inneis.size(); ++j) {
	 float weight = sampleDist( gen );
	 //edge is dead. Is it alive now at the current incentive level?
	 if (weight <= getEdgeWeight( adjList[v].INneis[ j ].weight, nodeLevels[ v ], lmax ) ) {
	    node_id target = adjList[ v ].INneis[ j ].target;
	    if (!(node_membership[target])) {
	       nextLevel.push_back( target );
	    }
	 }
	 
      }

      vector< node_id > nextLevel2;
      while( nextLevel.size() > 0 ) {
	 SampleStage nextStage;
	 for (size_t i = 0; i < nextLevel.size(); ++i ) {
	    node_id& curr = nextLevel[ i ];

	    //get a weight for this node
	    float samp = sampleDist( gen );
	    	    
	    if (samp <= getLevelOffset( nodeLevels[ curr ], lmax )) {
	       //this node is alive
	       //hence so is the sample
	       return true;
	    }
	    
	    vector< tinyEdge >& in_neis = adjList[ curr ].INneis;
	    for (size_t j = 0; j < in_neis.size(); ++j) {
	       samp = sampleDist( gen );
	    
	       if (samp <= getEdgeWeight( in_neis[ j ].weight, nodeLevels[ curr ], lmax ) ) {
		  //this edge is live
		  
		  if (!node_membership[ in_neis[ j ].target ] ) {
		     nextLevel2.push_back( in_neis[ j ].target );
		     node_membership[ in_neis[ j ].target ] = true;
		     
		  }
	       } else {

	       }
	       
	    }

	 }

	 nextLevel.swap( nextLevel2 );
	 nextLevel2.clear();
      }
      
      return false; //sample remains dead
   }
   
   double ext_marginal_gain( node_id& v,
			     vector< Sample >& samps,
			     vector< uint16_t >& nodeLevels,
			     vector< tinyNode >& adjList,
			     uint16_t& lmax,
			     uniform_real_distribution< float >& sampleDist,
			     mt19937& gen,
			     size_t l = 1 ) {
      
      double res = 0.0;
      set< sampleIndex > nodeSamples;
      if ( nodeLevels[ v ] <= lmax - l ) {
	 nodeLevels[ v ] += l;
	 vector< sLookup >::iterator it1 = adjList[ v ].mySamps.begin();
	 while ( it1 != adjList[ v ].mySamps.end() ) {
	    Sample& ss = samps[ it1->s ];
	    bool oldStatus = ss.alive;
	    bool newStatus = ext_update_samp( ss,
					      it1->p,
					      adjList,
					      lmax,
					      nodeLevels,
					      v,
					      sampleDist,
					      gen );
	    if ( newStatus != oldStatus ) {
	       if (oldStatus == true) {
		  //we lost one
		  res -= 1.0;
	       }
	       else {
		  //Who once was lost, is now saved
		  res += 1.0;
	       }
	    }

	    ++it1;
	 }

	 nodeLevels[ v ] -= l;
	       
      } else {
	 res = -1.0 * samps.size();  
      }

      return res;
   }
   
   class sampleGraph {
   public:
      vector< tinyNode > adjList;
      vector< uint16_t > nodeLevels;
      vector< Sample > samps;
      uint16_t lmax;
      unsigned n;
      unsigned m;
      Logger logg;

      double preprocessTime;
      

      vector<double> increments;

      
      sampleGraph() {
	 n = 0;
	 m = 0;
	 lmax = 0;
      }

      void preprocess() {
	 logg << "n = " << n << endL;
	 nodeLevels.assign( n, 0 );
      }

      sampleGraph( const sampleGraph& h ) {
	 adjList.assign( h.adjList.begin(), h.adjList.end() );
	 n = h.n;
	 m = h.m;
	 lmax = h.lmax;
	 preprocessTime = h.preprocessTime;
	 samps.assign( h.samps.begin(), h.samps.end() );
	 nodeLevels = h.nodeLevels;
	 logg.enabled = h.logg.enabled;
      }

      void assign( const sampleGraph& h ) {
	 adjList.assign( h.adjList.begin(), h.adjList.end() );
	 n = h.n;
	 m = h.m;
	 preprocessTime = h.preprocessTime;
	 lmax = h.lmax;
	 samps.assign( h.samps.begin(), h.samps.end() );
      }

      void clearSamples() {
	 samps.clear();
	 for (node_id v = 0; v < n; ++v) {
	    adjList[v].mySamps.clear();
	    
	 }
      }
      
  
      
      void removeSampFromNode( node_id& v, sampleIndex& sampIndex ) {
	 auto it = adjList[v].mySamps.begin();
	 while ( it != adjList[v].mySamps.end() ) {
	    if ( (*it).s == sampIndex) {
	       it = adjList[v].mySamps.erase( it );
	       break;
	    }

	    ++it;
	 }
	 
      }


      void updateSample( sampleIndex& sampIndex, size_t& nodeIndex ) {
	 
      }
      
      
      void updateLevelWet( node_id v, uint16_t newLev ) {
	 nodeLevels[ v ] = newLev;
	 tinyNode& vv = adjList[ v ];
	 
	 
	 vector< sLookup >& mySamps = vv.mySamps;
	 vector< sLookup >::iterator it = mySamps.begin();
	 while (it != mySamps.end() ) {
	    updateSampleWet( it->s, it->p, v );
	    ++it;
	 }
      }

      void resample( vector< uint16_t >& x , size_t nThreads = 1 ) {
	 size_t nSamps = samps.size();
	 clearSamples();
	 increaseSamples( x, nSamps, false, nThreads );
      }
      
      size_t increaseSamples( vector< uint16_t >& x , size_t S, bool dry = false, size_t nThreads = 1 ) {
	 size_t nActive = 0;
	 size_t newNSamps = S - samps.size();
	 uniform_int_distribution< node_id > nodeDist(0, n - 1);
	 uniform_real_distribution< float > threshDist(0.0,1.0);

	 thread* wThreads = new thread[ nThreads ];
	 size_t* thrActive = new size_t[ nThreads ];
	 

	 vector< vector< Sample > > thrSamps( nThreads, vector< Sample >() );
	 vector< vector< vector< sLookup > > > thrMaps( nThreads,
							vector< vector< sLookup > >( n ) );

	 //logg << "Sampling..." << endL;
	 
	 size_t thrSampNum = newNSamps / nThreads;
	 for (size_t i = 0; i < nThreads; ++i) {
	    size_t offset = samps.size() + thrSampNum * i;
	    if (i == (nThreads - 1)) {
	       thrSampNum = newNSamps - thrSampNum * i;
	       
	    }
	    wThreads[i] = thread( thread_add_samps,
				  ref( thrSamps[ i ] ),
				  ref( thrActive[ i ] ),
				  thrSampNum,
				  dry,
				  offset,
				  ref( x ),
				  ref( thrMaps[ i ] ),
				  ref( adjList ),
				  lmax );
	 }

	 for (size_t i = 0; i < nThreads; ++i) {
	    wThreads[i].join();
	 }

	 //logg << "Merging samples..." << endL;
	 //merge thrAdj to adjList
	 for ( size_t i = 0; i < nThreads; ++i) {
	    samps.insert( samps.end(), thrSamps[i].begin(), thrSamps[i].end() );
	    nActive += thrActive[ i ];
	    for (size_t v = 0; v < n; ++v ) {
	       adjList[v].mySamps.insert( adjList[ v ].mySamps.end(),
					  thrMaps[ i ][ v ].begin(),
					  thrMaps[ i ][ v ].end() );
	    }
	 }

	 delete [] wThreads;
	 delete [] thrActive;

	 
	 return nActive;
      }

      double avgAct( size_t nAct = 0, size_t nTot = 0 ) {
	 if (nTot == 0) {
	    for (sampleIndex s = 0; s < samps.size(); ++s) {
	       if (samps[s].alive)
		  ++nAct;
	    }
	    nTot = samps.size();
	 } 
	 return n * static_cast<double>( nAct ) / nTot;
	 
      }

      
      void clearLevels() {
	 //reset all samples to dead 
	 
	 for (node_id i = 0; i < n; ++i ) {
	    if (nodeLevels[i] > 0) {
	       nodeLevels[ i ] = 0;
	       //updateLevelWet( i, 0 );
	    }
	 }
      }


      bool updateSampleDry( Sample& ss, uint32_t position ) {

	 return false;
      }

      void updateSampleWet( const sampleIndex& ssIndex, uint32_t level, node_id& v ) {
	 Sample& ss = samps[ ssIndex ];

	 //only need to consider samples becoming activated, not de-activated
	 if (ss.alive == true) {
	    return; //no change
	 }
	 
	 //find the node
	 SampleStage& stage = ss.stages[ level ];
	 size_t i = 0;
	 while (i < stage.nodes.size()) {
	    if (stage.nodes[i] == v) {
	       break;
	    }
	    ++i;
	 }

	 if (i == stage.nodes.size()) {
	    cerr << "Node "
		 << v << " incorrectly reporting membership in sample "
		 << "..." << endl;
	    return;
	 }

	 float nodeWeight = sampleDist( gen );
	 if ( nodeWeight <= getLevelOffset( nodeLevels[ v ], lmax ) ) {
	    ss.alive = true;
	    return; //sample is alive, nothing further to do here
	 }

	 vector< node_id > nextLevel;
	 vector< bool > node_membership( n, false );
	 for (size_t j = 0; j < ss.stages.size(); ++j) {
	    SampleStage& stagej = ss.stages[j];
	    for (size_t k = 0; k < stagej.nodes.size(); ++k) {
	       node_membership[ stagej.nodes[k] ] = true;
	    }
	 }

	 size_t oldStages = ss.stages.size();

	 vector< tinyEdge >& in_neis = adjList[ v ].INneis;
	 for (size_t j = 0; j < in_neis.size(); ++j) {
	    float samp = sampleDist( gen );
	    //Is it alive now at the current incentive level?
	    if (samp <= getEdgeWeight( adjList[v].INneis[ j ].weight,
				       nodeLevels[ v ], lmax ) ) {
	       
	       node_id target = adjList[ v ].INneis[ j ].target;
	       if (!(node_membership[target])) {
		  nextLevel.push_back( target );

	       }
	    }
	 }

	 vector< node_id > nextLevel2;

	 while( nextLevel.size() > 0 ) {
	    ++level;
	    SampleStage nextStage;
	    for (size_t i = 0; i < nextLevel.size(); ++i ) {
	       node_id& curr = nextLevel[ i ];

	       //get a weight for this node
	       float samp = sampleDist( gen );
	       nextStage.nodes.push_back( curr );
	       
	       sLookup tmp;
	       tmp.s = ssIndex;
	       tmp.p = level;
	       //size_t poss = vector_ctns( adjList[ curr ].mySamps, tmp );
	       //if (poss > 0) {
	       //--poss;
	       //adjList[ curr ].mySamps[ poss ].p = level;
	       //} else {
	       adjList[ curr ].mySamps.push_back( tmp ); //DANGER
	       //}
	       
	       if (samp <= getLevelOffset( nodeLevels[ curr ], lmax )) {
		  //this node is alive
		  //hence so is the sample
		  ss.alive = true;
		  return;
	       }
	    
	       vector< tinyEdge >& in_neis = adjList[ curr ].INneis;
	       for (size_t j = 0; j < in_neis.size(); ++j) {
		  samp = sampleDist( gen );
		  
		  if (samp <= getEdgeWeight( in_neis[ j ].weight, nodeLevels[ curr ], lmax ) ) {
		     //this edge is live
		     if (!node_membership[ in_neis[ j ].target ] ) {
 			nextLevel2.push_back( in_neis[ j ].target );
			node_membership[ in_neis[ j ].target ] = true;
			
			//nextStage.nodes.push_back( target );
		     }
		  } else {
		  }
	       
	       }
	       
	    }

	    nextLevel.swap( nextLevel2 );
	    nextLevel2.clear();
	    if ( level < oldStages ) {
	       ss.stages[level].nodes.insert( ss.stages[ level ].nodes.end(),  nextStage.nodes.begin(),
					      nextStage.nodes.end() );
	       
	    } else {
	       ss.stages.push_back( nextStage );
	    }

	    
	    
	 }

	 return;
      }
      
      double marginalGainNoUpdate( node_id& v ) {
	 double res = 0.0;
	 set< sampleIndex > nodeSamples;
	 if ( nodeLevels[ v ] < lmax ) {
	    ++nodeLevels[ v ];
	    for (size_t i = 0 ; i < adjList[v].mySamps.size(); ++i ) {
	       Sample& ss = samps[ adjList[ v ].mySamps[ i ].s ];
	       bool oldStatus = ss.alive;
	       bool newStatus = updateSampleDry( ss, adjList[ v ].mySamps[ i ].p );
	       if ( newStatus != oldStatus ) {
		  if (oldStatus == true) {
		     //we lost one
		     res -= 1.0;
		  }
		  else {
		     //Who once was lost, is now saved
		     res += 1.0;
		  }
	       }
	    
	    }
	    --nodeLevels[ v ];
	       
	 } else {
	    res = -1.0 * samps.size();  
	 }

	 return res;
      }

      double marginalGainNoUpdateWithMap( node_id& v, size_t l = 1 ) {
	 //logg << "margin gain: " << v << ", " << l << endL;
	 double res = 0.0;
	 set< sampleIndex > nodeSamples;
	 if ( nodeLevels[ v ] <= lmax - l ) {
	    nodeLevels[ v ] += l;
	    vector< sLookup >::iterator it1 = adjList[ v ].mySamps.begin();
	    while ( it1 != adjList[ v ].mySamps.end() ) {
	       Sample& ss = samps[ it1->s ];
	       bool oldStatus = ss.alive;
	       bool newStatus = updateSampleDry( ss, it1->p );
	       if ( newStatus != oldStatus ) {
		  if (oldStatus == true) {
		     //we lost one
		     res -= 1.0;
		  }
		  else {
		     //Who once was lost, is now saved
		     res += 1.0;
		  }
	       }

	       ++it1;
	    }

	    nodeLevels[ v ] -= l;
	       
	 } else {
	    res = -1.0 * samps.size();  
	 }

	 return res;
      }
      

      double lazyGreedy( size_t k ) {
	 size_t solSize = 0;

	 priority_queue< mpair, vector< mpair >, comparePair > Q;
	 
	 for ( node_id i = 0; i < n; ++i ) {
	    mpair tmp;
	    tmp.id = i;
	    tmp.value = samps.size() + 1;

	    Q.push( tmp );
	 }

	 vector< bool > b_valid; 
	 while (solSize < k) {
          b_valid.assign( n, false );

	    while ( true ) {
	       mpair pmax = Q.top();
	       Q.pop();
	       if ( b_valid[ pmax.id ] ) {
		  updateLevelWet( pmax.id, nodeLevels[ pmax.id ] + 1 );
		  //updateLevel( pmax.id, nodeLevels[ pmax.id ] + 1 );
		  if ( nodeLevels[ pmax.id ] < lmax ) {
		     pmax.value = samps.size() + 1;
		     b_valid[ pmax.id ] = false;
		     Q.push( pmax );
		  }
		  break;
	       } else {
		  double tmpGain = marginalGainNoUpdateWithMap( pmax.id );
		  //double tmpGain = marginalGainNoUpdate( pmax.id );
		  pmax.value = tmpGain;
		  Q.push( pmax );
		  b_valid[ pmax.id ] = true;
	       }
	    }
	    ++solSize;
	 }
	 
	 return avgAct();
      }
      
      double simpleGreedy( size_t k ) {
	 size_t solSize = 0;
	 double maxGain = -1.0 * samps.size();
	 node_id maxV = 0;
	 while ( solSize < k ) {
	    //cerr << "Greedy at: " << solSize << endl;
	    maxV = 0;
	    maxGain = -1.0 * samps.size();
	    for (node_id i = 0; i < n; ++i) {
	       if (!(nodeLevels[i] == lmax)) {
		  //double tmpGain = marginalGainNoUpdate( i );
		  double tmpGain = marginalGainNoUpdateWithMap( i );
		  //cerr << tmpGain << ' ' << maxGain << endl;
		  if (tmpGain >= maxGain) {
		     maxGain = tmpGain;
		     maxV = i;
		  }
	       }
	    }

	    //Add in maxV
	    //updateLevel( maxV, nodeLevels[ maxV ] + 1 );
	    updateLevelWet( maxV, nodeLevels[ maxV ] + 1 );
	    //cerr << maxV << ' ' << (unsigned) lmax << ' ' << (unsigned) nodeLevels[ maxV ] << endl;
	    ++solSize;
	 }

	 return avgAct();
      }


      double maxIncrement( node_id& v ) {
	 logg << "Starting maxInc..." << endL;
	 double maxGain = -1.0* samps.size();
	 double tmpGain;
	 for (node_id i = 0; i < n; ++i) {
	    tmpGain = marginalGainNoUpdateWithMap( i );
	    increments[ i ] = tmpGain;
	    if (tmpGain >= maxGain) {
	       maxGain = tmpGain;
	       v = i;
	    }
	 }

	 return n * maxGain / samps.size();
      }


      
			   
      
      double max_inc_parallel( node_id& v, size_t nThreads = 1, size_t amt = 1 ) {
			      
	 struct wkArgs {
	    vector< uint16_t >& wknodeLevels;
	    uint16_t& llmax;
	    vector< tinyNode >& aadjList;
	    vector< Sample >& ssamps;
	    vector< double >& Increments;
	    size_t aamt;
	 } wkargs = { nodeLevels, lmax, adjList, samps, increments, amt };

	 class wkWork {
	 public:
	    static void run(
			    vector< node_id >::iterator& itBegin,
			    vector< node_id >::iterator& itEnd,
			    wkArgs& wkargs,
			    mutex& mtx,
			    unsigned& finished,
			    unsigned& term ) {
	       uniform_real_distribution< float > sampleDist(0.0,1.0);
	       mt19937 gen( rd() );
	       while (!(term == 1)) {
		  if (!(finished == 1)) {
		     while (itBegin != itEnd) {
			wkargs.Increments[ *itBegin ] = ext_marginal_gain( *itBegin,
									   wkargs.ssamps,
									   wkargs.wknodeLevels,
									   wkargs.aadjList,
									   wkargs.llmax,
									   sampleDist,
									   gen,
									   wkargs.aamt );
			++itBegin;
		     }
		  }
		  mtx.lock();
		  finished = 1;
		  mtx.unlock();
	       }
	    }
	 } wkwork;

	 increments.assign(n, 0.0);
	 vector< node_id > vNodes(n,0);
	 for (size_t i = 1; i < n; ++i) {
	    vNodes[ i ] = vNodes[ i - 1 ] + 1;
	 }

	 logg << "Starting maxInc, nThreads = " << nThreads << endL;

	 size_t blocksize = 100;
	 parallelWork( vNodes, wkargs, wkwork, nThreads, blocksize );

	 //increments vector should now be computed.
	 //get max
	 double maxGain = -1.0* samps.size();
	 double tmpGain;
	 for (node_id i = 0; i < n; ++i) {
	    tmpGain = increments[ i ];
	    if (tmpGain >= maxGain) {
	       maxGain = tmpGain;
	       v = i;
	    }
	 }

	 return n * maxGain / samps.size();
      }

      size_t binSearchPivot( node_id& i, double tau, size_t& k, size_t& sizeSol ) {
	//cerr << "starting bsp..." << endl;
	 tau = tau * samps.size() / n;
	 
	 size_t ls = 1;
	 
	 size_t lt1 = lmax - nodeLevels[ i ];
	 size_t lt2 = k - sizeSol;
	 
	 if (lt1 == 0)
	    return 0;
	 
	 size_t lt;
	 if (lt1 < lt2) 
	    lt = lt1;
	 else
	    lt = lt2;


	 if ( marginalGainNoUpdateWithMap( i, 2 ) < 2*tau) {
	   return 1;
	 }
	 
	 if ( marginalGainNoUpdateWithMap( i, lt ) >= lt * tau ) {
	    return lt;
	 }

	 while (lt > ls + 1) {
	    //logg << "BSP: " << ls << " " << lt << endL;
	    
	    size_t mp = (lt + ls) / 2;
	    if (marginalGainNoUpdateWithMap( i, mp ) >= mp * tau ) {
	       ls = mp;
	    } else {
	       lt = mp;
	    }
	 }


	 //cerr << "maxinc done." << endl;
	 return ls;
	 
      }

      size_t fastGreedy( size_t k, double epsi,
			 double LB,
			 size_t nThreads,
			 double kappa = 0.9,
			 double delta = 0.9 ) {
	 node_id v;
	 increments.assign( n, 0.0 );
	 //double M = maxIncrement( v );
	 double M = max_inc_parallel( v, nThreads );
	 double m = M;
	 double mPrime;
	 double beta = 1.0;
	 size_t sizeSol = 0;
	 do {
	    logg << "FastGreedy iteration, m = " << m
		 << ", beta = " << beta
		 << ", sizeSol = " << sizeSol << endL;
		 
	    
	    double tau = beta * kappa * m;

	    //handle all of the elements that we add 0 copies of first
	    vector< node_id > toConsider;
	    double tauAct = tau * samps.size() / n;
	    for (node_id i = 0; i < n; ++i) {
	      if (increments[i] < tauAct) {
		//add zero copies
	      } else {
		//need to consider this element
		toConsider.push_back( i );
	      }
	    }
	    //logg << "Considering " << toConsider.size() << " elements." << endL;
		      
	    for (size_t i = 0; i < toConsider.size(); ++i) {
	      node_id& v = toConsider[i];
	      //logg << "Starting BSP..." << endL;
	       size_t l = binSearchPivot( v, tau, k, sizeSol );
	 
	       if (l > 0) {
		  logg << "FastGreedy adding l,v: " << l << "," << v << endL;
		  //nodeLevels[ v ] += l;
		  updateLevelWet( v, nodeLevels[ v ] + l );
		  sizeSol += l;
		  if (sizeSol == k) {
		    logg << "FastGreedy terminating, sizeSol = " << sizeSol << endL;
		    return sizeSol;
		  }
	       }
	    }
	    //logg << "FastGreedy resampling..." << endL;
	    //resample( nodeLevels, nThreads );
	    
	    mPrime = m;
	    m = max_inc_parallel( v, nThreads );
	    
	    if (m > kappa * mPrime) 
	       beta = beta * delta;

	    //cerr << m << ' ' << epsi*LB / 4;
	 } while ( true ); //(m > 4*epsi* (LB + 1) / beta) && (m > epsi * epsi * M / k) ); //while ( m > epsi*LB / 4 ); // epsi*LB);

	 logg << "FastGreedy terminating, sizeSol = " << sizeSol << endL;

 
	 return sizeSol;
      }
      
      double estimateInfluence( vector< uint16_t >& x, size_t nSamples = 10000, bool dry = false,
				size_t nThreads = 1 ) {
	 clearSamples();

	 size_t nAct = increaseSamples( x, nSamples, dry, nThreads );

	 //size_t actSamples = static_cast< size_t > (nSamples / nThreads ) * nThreads;


	 return avgAct( nAct, nSamples );
      }

      // double estimateInfluenceDist( vector< uint16_t >& x, size_t nSamples = 10000 ) {
      // 	 clearSamples();

      // 	 increaseSamplesDist( x, nSamples );

      // 	 return avgActDist();
      // }
      
      void init_empty_graph() {
	 tinyNode emptyNode;
	 adjList.assign(n, emptyNode);
      }

      size_t getDegree( node_id v ) {
	 return adjList[v].OUTneis.size() + adjList[v].INneis.size();
      }

      // unsigned char getEdgeWeight( node_id from, node_id to ) {
      // 	 unsigned char w;
      // 	 vector< tinyEdge >& v1 = adjList[ from ].neis;

      // 	 auto it = v1.begin();
      // 	 while (it != v1.end()) {
      // 	    if (it->getId() >= to)
      // 	       break;

      // 	    ++it;
      // 	 }

      // 	 if (it != v1.end()) {
      // 	    if (it->getId() == to) {
      // 	       w = it->weight;
      // 	       return w;
      // 	    }
      // 	 }

      // 	 return 0;
      // }
    
      void read_bin( string fname ) {
	 this->adjList.clear();
	 this->m = 0;
	 this->n = 0;
      
	 ifstream ifile ( fname.c_str(), ios::in | ios::binary );
	 unsigned n;
	 ifile.read( (char*) &n, sizeof( node_id ) );
	 ifile.read( (char*) &preprocessTime, sizeof(double) );
      
	 this->n = n;

	 init_empty_graph();
	 size_t ss;
	 tinyEdge temp;
	 node_id nei_id;
	 float w;

	 for ( unsigned i = 0; i < n; ++i ) {

	    ifile.read( (char*) &ss, sizeof( size_t ) );

	    adjList[i].OUTneis.assign( ss, temp );
	    this->m += ss;
	    for (unsigned j = 0; j < ss; ++j) {
	       ifile.read( (char*) &nei_id, sizeof( node_id ) );
	       ifile.read( (char*) &w, sizeof( float ) );
	       adjList[i].OUTneis[j].target = nei_id;
	       adjList[i].OUTneis[j].weight = w;
	    }

	    ifile.read( (char*) &ss, sizeof( size_t ) );
	    
	    adjList[i].INneis.assign( ss, temp );
	    this->m += ss;
	    for (unsigned j = 0; j < ss; ++j) {
	       ifile.read( (char*) &nei_id, sizeof( node_id ) );
	       ifile.read( (char*) &w, sizeof( float ) );
	       adjList[i].INneis[j].target = nei_id;
	       adjList[i].INneis[j].weight = w;
	    }
	 }

	 //      logg(INFO, "Sorting neighbor lists..." );
	 // if (preprocessTime == 0.0) {
	 //    clock_t t_start = clock();
	 //    for (unsigned i = 0; i < n; ++i) {
	 //       sort( adjList[i].neis.begin(), adjList[i].neis.end(), tinyEdgeCompare );
	 //    }
	 //    preprocessTime = double (clock() - t_start) / CLOCKS_PER_SEC;
	 //    cout << "Preprocessing took " << preprocessTime << "s\n";
	 // }
      }

    
      // void write_bin( string fname ) {
      // 	 ofstream ifile ( fname.c_str(), ios::out | ios::binary );
      // 	 ifile.write( (char*) &n, sizeof( node_id ) );
      // 	 ifile.write( (char*) &preprocessTime, sizeof(double) );
      
      // 	 size_t ss;
      // 	 tinyEdge temp;
      // 	 node_id nei_id;
      // 	 unsigned char w;
      // 	 for ( unsigned i = 0; i < n; ++i ) {
	
      // 	    ss = adjList[i].neis.size();
      // 	    ifile.write( (char*) &ss, sizeof( size_t ) );

      // 	    for (unsigned j = 0; j < ss; ++j) {
      // 	       nei_id = adjList[i].neis[j].target;
      // 	       w = adjList[i].neis[j].weight;
      // 	       ifile.write( (char*) &nei_id, sizeof( node_id ) );
      // 	       ifile.write( (char*) &w, sizeof( unsigned char ) );
      // 	    }
      // 	 }

      // 	 ifile.close();
      // }

      // vector< tinyEdge >::iterator findEdgeInList( node_id source, node_id target ) {
      // 	 vector< tinyEdge >& v1 = adjList[source].neis;
      // 	 for (auto it = v1.begin();
      // 	      it != v1.end();
      // 	      ++it ) {
      // 	    if (it->target == target)
      // 	       return it;
      // 	 }

      // 	 return v1.end(); //Edge not found
      // }

      void print( ostream& os ) {
	 for (size_t i = 0; i <adjList.size(); ++i) {
	    os << i << endl;
	    os << "OUT:" << endl;
	    for (size_t j = 0; j < adjList[i].OUTneis.size(); ++j) {
	       os << adjList[i].OUTneis[j].target << ' '
		  << adjList[i].OUTneis[j].weight << ' ';
	    }
	    os << endl;
	    os << "IN:" << endl;
	    for (size_t j = 0; j < adjList[i].INneis.size(); ++j) {
	       os << adjList[i].INneis[j].target << ' '
		  << adjList[i].INneis[j].weight << ' ';
	    }
	    os << endl;
	 }
      }

      void printCIM( ostream& os ) {
	 //count edges
	 size_t m = 0;
	 for (node_id i = 0; i < n; ++i) {
	    m += adjList[ i ].INneis.size();
	 }
	 os << n << " " << m << endl;
	 for (size_t i = 0; i < adjList.size(); ++i) {
	    for (size_t j = 0; j < adjList[i].INneis.size(); ++j) {
	       os << adjList[i].INneis[j].target << ' '
		  << i << ' ' << adjList[i].INneis.size() << endl;
	    }
	 }
      }

   };
    
}

#endif
