#ifdef MODIC
#include "sample-graph-IC.cpp"
//#include "sample-graph-IC-vec.cpp"
#else
#include "sample-graph.cpp"
#endif

#include <iostream>
#include <string>
#include <unistd.h>
#include <chrono>

using namespace sampgraph;
using namespace std;

enum Algs {TESTSAMPLE, LIM, LIMHEU, MCGREEDY, MAXDEGINT, BCT, MAXDEGFRAC, GPARETO};
struct Args {
   Algs alg;
   sampleGraph g;
   string graphFileName;
   string outputFileName = "";
   string testVectorFName = "";
   size_t nThreads = 1;
   bool printOnly = false;
   Logger logg;
   double tElapsed;
   double wallTime;
   bool testSampling = false;
   size_t k = 0;
   size_t K = 0;
   bool quiet = false;
   bool testInfluence = false;
   double estInfluence = 0.0;
   double delta = 0.5;
};

void discount_frac( Args& args ) {
   vector <uint16_t > x( args.g.n, 0 );
   

}

void max_deg_int( Args& args ) {
   args.logg << "Starting max degree (int)..." << endL;

   size_t spent = 0;
   vector< size_t > degs;
   degs.assign( args.g.n, 0 );
   vector< uint16_t > x( args.g.n, 0 );
   
   for (node_id i = 0; i < args.g.n; ++i) {
      degs[ i ] = args.g.adjList[i].OUTneis.size();
   }
   
   while (spent < args.k) {
      size_t maxdeg = 0;
      node_id selectNode = 0;
      for (size_t j = 0; j < args.g.n; ++j) {
	 if (degs[ j ] >= maxdeg) {
	    selectNode = j;
	    maxdeg = degs[ j ];
	 }
      }

      //select the node
      spent += args.g.lmax;
      x[ selectNode ] = args.g.lmax;
      degs[ selectNode ] = 0;
   }

   args.g.preprocess();
   args.g.nodeLevels = x;
   
}

void max_deg_frac( Args& args ) {
   args.logg << "Starting max degree (frac)..." << endL;

   vector< uint16_t > x;
   x.assign( args.g.n, 0 );

   size_t K = args.k / args.g.lmax;
   
   //count edges
   size_t m = 0;
   for (node_id i = 0; i < args.g.n; ++i) {
      m += args.g.adjList[ i ].OUTneis.size();
   }
   
   for (node_id i = 0; i < args.g.n; ++i) {
      double frac = static_cast<double>( args.g.adjList[ i ].OUTneis.size() ) / m;
      
      x[ i ] = static_cast< uint16_t > ( K * frac * args.g.lmax );
      if (x [ i ] > args.g.lmax)
	 x[ i ] = args.g.lmax;
   }

   args.g.preprocess();
   
   args.g.nodeLevels = x;
}

void mcGreedy( Args& args ) {
   args.logg << "Starting MC..." << endL;
   args.g.preprocess();

   args.g.clearSamples();

   size_t initSamps = 10000;//100 * log( args.g.n ) / epsi * epsi;

   vector< uint16_t >& x = args.g.nodeLevels;
   x.assign( args.g.n, 0 );

   size_t solSize = 0;
   while (solSize < args.k) {
      cerr << "mc: " << solSize << endl;
      args.g.clearSamples();
      args.g.increaseSamples(x, initSamps, false, args.nThreads );
      //args.g.nodeLevels = x;
      node_id v;
      args.g.max_inc_parallel( v, args.nThreads );
      ++x[ v ];

      ++solSize;
	    
   }

   args.g.nodeLevels = x;

}

void greedyRIS( Args& args ) {
   if (args.g.lmax != 1) {
      args.g.logg << WARN << "Cannot use BCT with lmax > 1" << endL;
      args.g.logg << "Setting lmax = 1." << endL;
      args.g.logg << INFO;
      args.g.lmax = 1;

      if (args.K > 0)
	 args.k = args.K;
   }

   args.g.preprocess();
   args.g.clearSamples();

   size_t initSamps = 200000;

   args.logg << "Initial Samples: " << initSamps << endL;
   vector< uint16_t > vZero( args.g.n, 0 );
   vector< uint16_t > vSol;
   args.g.increaseSamples( vZero, initSamps, false, args.nThreads );

   double LB = args.g.n;

   double outAct;
   vector <uint16_t > x;
   do {
      args.g.resetSamps();
      args.logg << "Starting Max Coverage..." << endL;

      outAct = args.g.max_covg(x, args.k );
      args.logg << "Activation: " << outAct << endL;
      LB = LB / 2;
   } while ( false );

   args.logg << outAct << endL;
   args.g.nodeLevels = x;
}

void runGpareto( Args& args ) {
   if (args.g.lmax != 1) {
      args.g.lmax = 1;

      if (args.K > 0)
	 args.k = args.K;
   }

   args.g.preprocess();
   args.g.clearSamples();

   size_t initSamps = 50000;

   args.logg << "Samples: " << initSamps << endL;
   vector< uint16_t > vZero( args.g.n, 0 );
   vector< uint16_t > vSol;
   args.g.increaseSamples( vZero, initSamps, false, args.nThreads );

   double outAct;
   vector <uint16_t > x;

   args.logg << "Starting Greedy..." << endL;
   outAct = args.g.max_covg(x, args.k );
   args.logg << "Greedy Activation: " << outAct << endL;
   args.g.resetSamps();
   args.logg << "Starting Pareto..." << endL;

   outAct = args.g.greedy_pareto(x, args.k, outAct );
   args.logg << "Pareto Activation: " << outAct << endL;

   args.g.nodeLevels = x;
}


void runlim( Args& args ) {
   args.logg << "Starting LIM..." << endL;
   args.g.preprocess();

   args.g.clearSamples();
   double B = 2.0 * args.g.n;
   
   double delta = args.delta;
   double epsi = delta / sqrt( args.k );
   double kappa = 0.9;
   
   //double M = args.g.n * log2( args.g.n ) * log2( args.k ) * log2 ( epsi * epsi / args.k ) / log2( kappa );
   //if ( args.g.n * args.k < M)
   double M = args.g.n * args.k;
   
   double numerator = log (2* M ) + log( args.g.n );

   numerator = numerator * args.g.n;
   
   args.g.logg << "numerator = " << numerator << endL;
   
   vector< uint16_t > vZero( args.g.n, 0 );
   vector< uint16_t > vSol;
   
   double outAct;
   vector< uint16_t > x;
   bool decreaseB = true;

   {
      args.logg << "Getting initial estimate for B" << endL;
      size_t numSamps = 2 * numerator / (epsi * epsi * B ) + numerator / (epsi*B);
      
      args.logg << "Initial samples: " << numSamps << endL;
      args.g.increaseSamples( vZero, numSamps, false, args.nThreads );
      
      args.g.fastGreedySizeK( args.k, epsi, B, args.nThreads, kappa  );
      args.g.clearSamples();

      outAct = args.g.estimateInfluenceDagum( args.g.nodeLevels,
					      100000,
					      0.1,
					      1.0 / args.g.n,
					      args.nThreads );
      args.logg << "Activation: " << outAct << endL;
      B = outAct * 1.5;
   }

   do {
      args.g.clearLevels();
      args.logg << "B: " << B << endL;

      size_t numSamps = 2 * numerator / (epsi * epsi * B ) + numerator / (epsi*B);
      
      args.logg << "Resampling to: " << numSamps << endL;
      args.g.clearSamples();
      args.g.increaseSamples( vZero, numSamps, false, args.nThreads );
      
      args.logg << "Starting FastGreedy..." << endL;
      
      args.g.fastGreedySizeK( args.k,
			      epsi,
			      B,
			      args.nThreads,
			      kappa );
      outAct = args.g.avgAct();
      args.logg << "Activation: " << outAct << endL;
      args.logg << "Sampling threshold: " << B - epsi* sqrt( B ) * sqrt( args.g.n ) << endL;
      if ( outAct >= B - epsi* sqrt( B ) * sqrt( args.g.n ) ) {
	 args.logg << "At sampling threshold..." << endL;
	 args.logg << "Increasing B..." << endL;
	 B = 1.5 * B;
	 continue;
      }
      
      args.logg << "LIM terminating..." << endL;
      vector< double >& fgMG = args.g.fastGreedyMarginalGains;
      vector< double >& fgBD = args.g.fastGreedyBounds;
      double alpha = 1.0;
      for (size_t ii = 0; ii < fgMG.size(); ++ii) {
	 double tmp = (fgBD[ ii ] / (fgMG[ ii ] - epsi * B / args.k));
	 if ( tmp > alpha ) {
	    alpha = tmp;
	 }
      }
      
      args.logg << "Performance ratio: (original) / " << 1 + alpha << endL;
      x = args.g.nodeLevels;
      break;
           
   } while (true);
   
   args.g.nodeLevels = x;
   args.logg << endL;
}

void runlimheu( Args& args ) {
   args.logg << "Starting LIM (heuristic mode)..." << endL;
   args.g.preprocess();

   args.g.clearSamples();
   double B = 2.0 * args.g.n;
   
   double delta = 0.1;
   double epsi = delta;
   double kappa = 0.9;
   
   double M = args.g.n * log2( args.g.n ) * log2( args.k ) * log2 ( epsi * epsi / args.k ) / log2( kappa );
   if ( args.g.n * args.k < M)
      M = args.g.n * args.k;
   
   double numerator = log ( M );// + log( args.g.n

   numerator = numerator * args.g.n;
   
   args.g.logg << "numerator = " << numerator << endL;
   
   vector< uint16_t > vZero( args.g.n, 0 );
   vector< uint16_t > vSol;
   
   double outAct;
   vector< uint16_t > x;
   do {
      B = B / 2.0;
            
      args.g.clearLevels();
      args.logg << "B: " << B << endL;
      size_t numSamps = 0.67 * numerator / (epsi * B) + 2 * numerator / (epsi * epsi * B );
      
      args.logg << "Increasing samples to: " << numSamps << endL;
      args.g.increaseSamples( vZero, numSamps, false, args.nThreads );
      args.logg << "Starting FastGreedy..." << endL;
      //copy g
      sampleGraph h( args.g );

      size_t sizeSol = h.fastGreedy( args.k, epsi, B, args.nThreads );
      outAct = h.avgAct();
      args.logg << "Activation: " << outAct << endL;
      
      if ( outAct > (2 + epsi)*B ) {
	 x = h.nodeLevels;
	 break;
      }
     
   } while (true);
   
   args.g.nodeLevels = x;
   args.logg << endL;
}

void testSample( Args& args ) {
   vector< uint16_t > x;
   if (args.testVectorFName != "") {
      ifstream ifs( args.testVectorFName.c_str() );
      unsigned tmp;
      ifs >> tmp;
      args.g.lmax = tmp;
      x.assign( args.g.n , 0 );

      while (ifs >> tmp ) {
	 unsigned index = tmp;
	 ifs >> tmp;
	 x[index] = tmp;
      }
      
      ifs.close();
      args.g.preprocess();
   } else {
      x.assign( args.g.nodeLevels.begin(), args.g.nodeLevels.end() );
   }

   size_t count = 0;
   for (size_t i = 0; i < x.size(); ++i) {
      count += x[i];
      //if (x[i] > 0) {
      //cout << (unsigned) i << ' ' << (unsigned) x[i] << endl;
      //}
   }
   args.logg << "Size of x: " << count << endL;
   args.logg << "lmax: " << args.g.lmax << endL;
   double epsi = 0.01;
   double delta = 0.01;

   double lambda = exp(1) - 2.0;
   double upsilon = 4.0 * lambda * log(2.0 / delta) / pow(epsi, 2);
   double upsilon_1 = 1.0 + (1.0 + epsi) * upsilon;

   int sum = 0;
   int nSamps = 0;
   int goal = ceil(upsilon_1);

#define BATCH_SIZE 500000
   args.g.clearSamples();
   while(sum < goal) {
       nSamps += BATCH_SIZE;
       sum += args.g.increaseSamples(x, BATCH_SIZE, true, args.nThreads);
   }

   args.logg << "Samples: " << nSamps << endL;
   args.estInfluence = args.g.n * static_cast<double>(sum) / nSamps;
   args.logg << args.estInfluence  << endL;
   
}

void print_help() {
   cout << "Options: " << endl;
  cout << "-G <graph filename in binary format>" << endl
     //<< "-p <outfilename> (print graph info and edge list to file and exit)" << endl
       << "-k <total budget>" << endl
       << "-l <maximum number of levels>" << endl
       << "-L (run LIM)" << endl
       << "-H (run LIM-based heuristic)" << endl
       << "-D (run MAXDEG INT)" << endl
       << "-E (run MAXDEG FRAC)" << endl
       << "-M (run MCGREEDY)" << endl
       << "-x <max number of threads (default: 1)> " << endl
       << "-q (quiet mode, suppress logging)" << endl
       << "-t (independently estimate the influence of solution)" << endl
       << "-o <output filename> (write solution vector to specified file)" << endl;
}

void parseArgs( int argc, char** argv, Args& arg ) {
  int c;
  extern char *optarg;

  if (argc == 1) {
    print_help();
    exit( 2 );
  }

  string sarg;
  while ((c = getopt( argc, argv, ":G:DK:OT:k:p:vx:PHSEBMNLl:qto:d:") ) != -1) {
    switch(c) {
    case 'o':
       arg.outputFileName.assign( optarg );
       break;
    case 't':
       arg.testInfluence = true;
       break;
    case 'q':
       arg.quiet = true;
       break;
    case 'x':
       sarg.assign( optarg );
       arg.nThreads = stoi( sarg );
       break;
    case 'l':
       sarg.assign( optarg );
       arg.g.lmax = stoi( sarg );
       break;
    case 'k':
       sarg.assign( optarg );
       arg.k = stoi( sarg );
       break;
    case 'K':
       sarg.assign( optarg );
       arg.K = stoi( sarg );
       break;
    case 'G':
      //graph specification
      arg.graphFileName.assign( optarg );
      break;
    case 'p':
       //target specification
       arg.printOnly = true;
       arg.outputFileName.assign( optarg );
       break;
    case 'T':
       arg.alg = Algs::TESTSAMPLE;
       arg.testVectorFName.assign( optarg );
       break;
    case 'L':
       arg.alg = Algs::LIM;
       break;
    case 'P':
       arg.alg = Algs::GPARETO;
       break;
    case 'H':
       arg.alg = Algs::BCT;
       break;
    case 'D':
       arg.alg = Algs::MAXDEGINT;
       break;
    case 'E':
       arg.alg = Algs::MAXDEGFRAC;
       break;
    case 'M':
       arg.alg = Algs::MCGREEDY;
       break;
    case 'd':
       sarg.assign( optarg );
       arg.delta = stod(sarg);
       break;
    case '?':
      print_help();
      exit( 0 );
      break;
    }
  }

  if (arg.K > 0) {
     arg.k = arg.K * arg.g.lmax;
     
  }

  if (arg.quiet) {
     arg.g.logg.enabled = false;
     arg.logg.enabled = false;
  }
}

void readGraph( Args& args ) {
   args.g.read_bin( args.graphFileName );
}

void runAlg( Args& args ) {
   if (args.printOnly) {
      return;
   }
   
  clock_t t_start = clock();
  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
  switch (args.alg) {
  case Algs::TESTSAMPLE:
     testSample( args );
     //print estimated influence, even if running in quiet mode
     if (args.quiet)
	cout << args.estInfluence;
     break;
  case Algs::LIM:
     if (args.k == 0) {
	args.logg << ERROR << "k not set" << endL;
	exit( 1 );
     }
     runlim( args );
     
     break;
  case Algs::BCT:
     greedyRIS( args );
     break;

  case Algs::GPARETO:
     runGpareto( args );
     break;
  case Algs::LIMHEU:
     if (args.k == 0) {
	args.logg << ERROR << "k not set" << endL;
	exit( 1 );
     }
     runlimheu( args );
     
     break;

  case Algs::MCGREEDY:
     if (args.k == 0) {
	args.logg << ERROR << "k not set" << endL;
	exit( 1 );
     }
     mcGreedy( args );
     
     break;
  case Algs::MAXDEGINT:
     if (args.k == 0) {
	args.logg << ERROR << "k not set" << endL;
	exit( 1 );
     }

     max_deg_int( args );
     
     break;
  case Algs::MAXDEGFRAC:
     if (args.k == 0) {
	args.logg << ERROR << "k not set" << endL;
	exit( 1 );
     }

     max_deg_frac( args );
     
     break;
  default:
     args.logg << "Unrecognized algorithm." << endL;
     break;
  }

  args.tElapsed = elapsedTime( t_start );
  std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
  size_t WallTimeMillis = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
  args.wallTime = WallTimeMillis / 1000.0;

  
}

void outputResults( Args& args, ostream& os ) {
   if (args.printOnly) {
      args.g.printCIM( os );
      return;
   }

   if (args.outputFileName == "") {
      args.logg << "Solution vector (nonzero entries only):" << endL;
      args.logg << "lmax: " << args.g.lmax << endL;
      for (size_t v = 0; v < args.g.n; ++v) {
	 if (args.g.nodeLevels[ v ] > 0) {
	    args.logg << "(" << v << "," << args.g.nodeLevels[ v ] << ") ";
	 }
      }
      args.logg << endL;
   } else {
      args.logg << "Writing solution vector to file: " << args.outputFileName << endL;
      os << args.g.lmax << endl;
      for (size_t v = 0; v < args.g.n; ++v) {
	 if (args.g.nodeLevels[ v ] > 0) {
	    os << v << ' ' << args.g.nodeLevels[ v ] << endl;
	 }
      }
   }
   
}

int main(int argc, char** argv) {
   Args args;
   parseArgs( argc, argv, args );
   readGraph( args );
   runAlg( args );
   
   if (args.outputFileName != "") {
      //ofstream of( args.outputFileName.c_str(), ofstream::out | ofstream::app );
      ofstream of( args.outputFileName.c_str(), ofstream::out );
      outputResults( args, of );
      of.close();
   } else {
      outputResults( args, cout );
   }

   if (args.testInfluence) {
      args.logg << "Influence of solution vector: " << endL;
      testSample( args );
   }

   return 0;
}
