#ifndef SAMPLE_GRAPH_CPP
#define SAMPLE_GRAPH_CPP
#include <list>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>
#include <unordered_set>
#include <thread>
#include <iomanip>
#include <cmath>
#include <utility>
#include <queue>
#include <set>
#include <thread>
#include <mutex>
#include "logger.cpp"
#include "util.cpp"

using namespace std;


namespace sampgraph {
   
   random_device rd;
   mt19937 gen( rd() );
   uniform_real_distribution< float > threshDist(0.0,1.0);
   
   typedef uint32_t node_id;
   bool mycompare( const node_id& a, const node_id& b ) {
      return a < b;
   }  

   class mpair {
   public:
      uint32_t id;
      double value;
   };

   class comparePair {
   public:
      bool operator() ( mpair a, mpair b ) {
	 return (a.value < b.value);
      }
   };
   
   //directed graph
   
   /*
    * Edge classes
    */
   class tinyEdge {
   public:
      
      uint32_t target;
      float weight;
     
      tinyEdge() {
	 target = 0;
      }

      tinyEdge( node_id nid, float w = 1.0) {
	 target = nid; 
	 weight = w;
      }

      tinyEdge( const tinyEdge& rhs ) {
	 target = rhs.target;
	 weight = rhs.weight;
      }
   };

   bool operator<( const tinyEdge& a, const tinyEdge& b ) {
      return a.target < b.target;
   }

   class Sample {
   public:
      vector< float > thresholds;
      vector< node_id > path;
      bool alive;
      //vector< bool > node_membership;
   };

   typedef uint32_t sampleIndex;

   class sLookup {
   public:
      sampleIndex s;
      uint32_t p;
   };
   
   //Node class
   class tinyNode {
   public:
      vector< tinyEdge > OUTneis;
      vector< tinyEdge > INneis;
      vector< sLookup > mySamps;
      
      //For each sample containing the node, the index of the sample is
      //mapped to the FIRST occurence of the node in the sample
      map< sampleIndex, uint32_t > mapSamps;
      
      /*
       * edges are preprocessed into p, s.t.
       * p[i] has threshold t[i]
       * t[i] < t[i+1]
       * for query q
       * if i* is smallest such that
       * t[i*] >= q,
       * then edge from here to p[i*] is live
       */
      vector< node_id > p;
      vector< float > t; 
      
      tinyNode () { }
     
      tinyNode ( const tinyNode& rhs ) {
	 OUTneis.assign( rhs.OUTneis.begin(), rhs.OUTneis.end() );
	 INneis.assign( rhs.INneis.begin(), rhs.INneis.end() );
	 mySamps.assign( rhs.mySamps.begin(), rhs.mySamps.end() );
	 mapSamps = rhs.mapSamps;
	 p.assign( rhs.p.begin(), rhs.p.end() );
	 t.assign( rhs.t.begin(), rhs.t.end() );
	 
      }

      // vector< tinyEdge >::iterator incident( node_id& out ) {
      // 	 vector< tinyEdge >::iterator it = neis.begin();
      // 	 do {
      // 	    if (it->getId() == out)
      // 	       return it;
      // 	    ++it;
      // 	 } while (it != neis.end());

      // 	 return it;
      // }
   };

   float pInit = 0.0;
   
   float getLevelOffset( uint16_t& l, uint16_t& lmax ) {
      //return static_cast<float>( l ) / (2.0 * lmax);
      return pInit + (1.0 - pInit)* static_cast<float>( l ) / (lmax);

      

      //return (-1.0 / (lmax * lmax ) )*(l)*(l - 2.0 * lmax);
      //return 1.0 - exp( -4.0 * l / lmax );
   }
   
   /*
    * returns neither = 0, alive = 1, dead = 2
    * also, if neither updates v to have been traced back
    */
   uint8_t ext_trace_back( vector< uint16_t >& x,
			   vector< tinyNode >& adjList,
			   node_id& v,
			   float& thresh,
			   vector< bool >& sample_membership,
			   uint16_t& lmax ) {
      float ext = getLevelOffset( x[ v ], lmax );
      float ask = thresh - ext;
	 	 
      if ( ask < 0.0 ) {
	 //aLive!
	 return 1;
      }

      //lookup the edge, if any
      //TODO: improve to binary search
      size_t whichEdge = 0;
      size_t tSize = adjList[v].t.size();
      if (tSize == 0) {
	 return 2; //dead, not alive and no way to go backwards
      }
	 
      //while ( adjList[v].t[whichEdge] * (1.0 - ext) < ask ) {
      while ( adjList[v].t[whichEdge] * (1.0) < ask ) {
	 ++whichEdge;
	 if (whichEdge == tSize) {
	    //dead
	    return 2;
	 }
      }

      //having made it here, whichEdge is first i* with t[i*] >= ask
      v = adjList[v].p[ whichEdge ];
      if ( sample_membership[ v ] ) {
	 //node already in sample.
	 //dead, since no node in sample
	 //is live
	 return 2;
      }
	 
      return 0;
   }

   size_t ext_add_samp( vector< uint16_t >& x,
			vector< tinyNode >& adjList,
			vector< Sample >& samps,
			uniform_int_distribution< node_id >& nodeDist,
			size_t sampleOffset,
			bool dry,
			uint16_t lmax,
			vector< map< sampleIndex, uint32_t > >& Maps,
			uniform_real_distribution< float >& threshDist,
			mt19937& gen ) {
      node_id n = adjList.size();
      Sample ns;
      vector< bool > node_membership( n, false ); //empty sample
      node_id target = nodeDist( gen );
      float sthresh;
      uint8_t aLiveDeadNeither = 0;
      do {
	 sthresh = threshDist( gen );
	 ns.thresholds.push_back( sthresh );
	 ns.path.push_back( target );
	 node_membership[ target ] = true;
	 aLiveDeadNeither = ext_trace_back( x,
					    adjList,
					    target,
					    sthresh,
					    node_membership,
					    lmax ); // 0 = neither, 1 = alive, 2 = dead
	    
      } while( aLiveDeadNeither == 0);

      node_membership[ target ] = true;
      ns.path.push_back( target );
      ns.thresholds.push_back( sthresh );
      
      if (aLiveDeadNeither == 1) {
	 //this sample is live!
	 ns.alive = true;
      } else {
	 //this sample is dead
	 ns.alive = false;
      }


      if (!dry) {
	 //Add Sample to constituent nodes
	 for (uint32_t i = 0; i < ns.path.size(); ++i) {
	    //sLookup tmp;
	    //tmp.s = samps.size() + sampleOffset ;
	    //tmp.p = i;
	    //Maps[ ns.path[i] ].mySamps.push_back( tmp );
	    Maps[ ns.path[i ] ][ samps.size() + sampleOffset ] = i;
	 }

	 //Add Sample to samps
	 samps.push_back( ns );
      }

      if (ns.alive)
	 return 1;

      return 0;
   }

   void thread_add_samps( vector< Sample >& samps,
			  size_t& nActive,
			  size_t nSamps,
			  bool dry,
			  size_t sampOffset,
			  vector< uint16_t >& x,
			  vector< map< sampleIndex, uint32_t > >& myMaps,
			  vector< tinyNode >& adjList,
			  size_t lmax ) {
      nActive = 0;
      uniform_int_distribution< node_id > nodeDist( 0, adjList.size() - 1);
      uniform_real_distribution< float > sampleDist(0.0,1.0);
      random_device rd;
      mt19937 gen( rd() );
      for (size_t i = 0; i < nSamps; ++i) {
	 nActive += ext_add_samp( x,
				  adjList,
				  samps,
				  nodeDist,
				  sampOffset,
				  dry,
				  lmax,
				  myMaps,
				  sampleDist,
				  gen );
				 
      }

   }
   
   
   bool ext_update_samp( Sample& ss,
			 uint32_t position,
			 vector< tinyNode >& adjList,
			 uint16_t& lmax,
			 vector< uint16_t >& nodeLevels,
			 uniform_real_distribution< float >& threshDist,
			 mt19937& gen ) {
      node_id target = ss.path[ position ];
      float sthresh;
      uint8_t aLiveDeadNeither = 0;
      node_id n = nodeLevels.size();
      vector< bool > node_membership( n, false );
      for (size_t i = 0; i <= position; ++i) {
	 node_membership[ ss.path[ i ] ] = true;
      }

      do {

	 if (position < ss.path.size()) {
	    //cerr << position << ' ' << ss.thresholds.size() << endl;
	    //sthresh = threshDist( gen );
	    sthresh = ss.thresholds[ position ];
	    node_membership[ target ] = true;
	    aLiveDeadNeither = ext_trace_back( nodeLevels,
					       adjList,
					       target,
					       sthresh,
					       node_membership,
					       lmax ); // 0 = neither, 1 = alive, 2 = dead
	    if (position < ss.path.size() - 1) {
	       if (target == ss.path[ position + 1 ]) {
		  //sample does not change from here.
		  return ss.alive;
	       }
	    }
	 } else {
	    sthresh = threshDist( gen );
	    //ss.thresholds.push_back( sthresh );
	    node_membership[ target ] = true;
	    aLiveDeadNeither = ext_trace_back( nodeLevels,
					       adjList,
					       target,
					       sthresh,
					       node_membership,
					       lmax ); // 0 = neither, 1 = alive, 2 = dead
	 }
	 ++position;
      } while ( aLiveDeadNeither == 0 );

      if (aLiveDeadNeither == 1) {
	 //this sample is live!
	 return true;
      } else {
	 //this sample is dead
	 return false;
      }

      return false;
   }
   
   double ext_marginal_gain( node_id& v,
			     vector< Sample >& samps,
			     vector< uint16_t >& nodeLevels,
			     vector< tinyNode >& adjList,
			     uint16_t& lmax,
			     uniform_real_distribution< float >& threshDist,
			     mt19937& gen,
			     size_t l = 1 ) {
      
      double res = 0.0;
      set< sampleIndex > nodeSamples;
      if ( nodeLevels[ v ] <= lmax - l ) {
	 nodeLevels[ v ] += l;
	 map< sampleIndex, uint32_t >::iterator it1 = adjList[ v ].mapSamps.begin();
	 while ( it1 != adjList[ v ].mapSamps.end() ) {
	    Sample& ss = samps[ it1->first ];
	    bool oldStatus = ss.alive;
	    bool newStatus = ext_update_samp( ss,
					      it1->second,
					      adjList,
					      lmax,
					      nodeLevels,
					      threshDist,
					      gen);
	    if ( newStatus != oldStatus ) {
	       if (oldStatus == true) {
		  //we lost one
		  res -= 1.0;
	       }
	       else {
		  //Who once was lost, is now saved
		  res += 1.0;
	       }
	    }

	    ++it1;
	 }

	 nodeLevels[ v ] -= l;
	       
      } else {
	 res = -1.0 * samps.size();  
      }

      return res;
   }
   
   class sampleGraph {
   public:
      vector< tinyNode > adjList;
      vector< uint16_t > nodeLevels;
      vector< Sample > samps;
      uint16_t lmax;
      unsigned n;
      unsigned m;
      Logger logg;

      double preprocessTime;
      

      vector<double> increments;

      
      sampleGraph() {
	 n = 0;
	 m = 0;
	 lmax = 0;
      }

      void preprocess() {
	 logg << "Beginning preprocessing..." << endL;
	 logg << "n = " << n << endL;
	 nodeLevels.assign( n, 0 );
	 for (node_id v = 0; v < n; ++v) {
	    tinyNode& vv = adjList[ v ];
	    vv.p.clear();
	    vv.t.clear();
	    for (size_t i = 0; i < vv.INneis.size(); ++i) {
	       vv.p.push_back( vv.INneis[ i ].target );
	       if (!vv.t.empty())
		  vv.t.push_back( vv.t.back() + vv.INneis[i].weight );
	       else
		  vv.t.push_back( vv.INneis[i].weight );
	    }
	    //print_vector( vv.t );
	    vv.OUTneis.clear();
	    vv.INneis.clear();
	 }
	 logg << "Preprocessing complete." << endL;
      }

      sampleGraph( const sampleGraph& h ) {
	 adjList.assign( h.adjList.begin(), h.adjList.end() );
	 n = h.n;
	 m = h.m;
	 lmax = h.lmax;
	 preprocessTime = h.preprocessTime;
	 samps.assign( h.samps.begin(), h.samps.end() );
      }

      void assign( const sampleGraph& h ) {
	 adjList.assign( h.adjList.begin(), h.adjList.end() );
	 n = h.n;
	 m = h.m;
	 preprocessTime = h.preprocessTime;
	 lmax = h.lmax;
	 samps.assign( h.samps.begin(), h.samps.end() );
      }

      void clearSamples() {
	 samps.clear();
	 for (node_id v = 0; v < n; ++v) {
	    adjList[v].mySamps.clear();
	    adjList[v].mapSamps.clear();
	 }
      }
      
      uint8_t traceAlive( vector< uint16_t >& x, node_id& v, float& thresh ) {
	 float& ask = thresh;

	 if ( ask < getLevelOffset( x[ v ], lmax ) ) {
	    //aLive!
	    return 1;
	 }

	 //dead
	 return 2;
      }
      
      uint8_t traceBackNei( vector< uint16_t >& x, node_id& v, float& thresh ) {
	 float& ask = thresh;

	 //lookup the edge, if any
	 //TODO: improve to binary search
	 size_t whichEdge = 0;
	 size_t tSize = adjList[v].t.size();
	 if (tSize == 0) {
	    return 2; //dead, not alive and no way to go backwards
	 }
	 while ( adjList[v].t[whichEdge] < ask ) {
	    ++whichEdge;
	    if (whichEdge == tSize) {
	       //dead
	       return 2;
	    }
	 }

	 //having made it here, whichEdge is first i* with t[i*] >= ask
	 v = adjList[v].p[ whichEdge ];
	 return 0;
      
      }
      
      
      /*
       * returns neither = 0, alive = 1, dead = 2
       * also, if neither updates v to have been traced back
       */
      uint8_t traceBack( node_id& v, float& thresh, vector< bool >& sample_membership ) {
	 return ext_trace_back( nodeLevels,
				adjList,
				v,
				thresh,
				sample_membership,
				lmax );
      }
	 
	 // float ask = thresh - getLevelOffset( nodeLevels[ v ] );
	 
      // 	 if ( ask < 0.0 ) {
      // 	    //aLive!
      // 	    return 1;
      // 	 }

      // 	 //lookup the edge, if any
      // 	 //TODO: improve to binary search
      // 	 size_t whichEdge = 0;
      // 	 size_t tSize = adjList[v].t.size();
      // 	 if (tSize == 0) {
      // 	    return 2; //dead, not alive and no way to go backwards
      // 	 }
      // 	 while ( adjList[v].t[whichEdge] < ask ) {
      // 	    ++whichEdge;
      // 	    if (whichEdge == tSize) {
      // 	       //dead
      // 	       return 2;
      // 	    }
      // 	 }

      // 	 //having made it here, whichEdge is first i* with t[i*] >= ask
      // 	 v = adjList[v].p[ whichEdge ];
      // 	 return 0;
      // }

            
      void removeSampFromNode( node_id& v, sampleIndex& sampIndex ) {
	 auto it = adjList[v].mySamps.begin();
	 while ( it != adjList[v].mySamps.end() ) {
	    if ( (*it).s == sampIndex) {
	       it = adjList[v].mySamps.erase( it );
	       break;
	    }

	    ++it;
	 }
	 
      }


      void updateLevel( node_id v, uint16_t newLev ) {
	 nodeLevels[ v ] = newLev;
	 tinyNode& vv = adjList[ v ];
	 //size_t nSamps = vv.mySamps.size();
	 vector< sLookup > oldMySamps = vv.mySamps;
	 
	 for (size_t i = 0; i < oldMySamps.size(); ++i) {
	    //updateSample( oldMySamps[i].s , v );
	 }
      }

      void updateLevelWet( node_id v, uint16_t newLev ) {
	 nodeLevels[ v ] = newLev;
	 tinyNode& vv = adjList[ v ];
	 //size_t nSamps = vv.mySamps.size();
	 //map< sampleIndex, uint32_t > oldSamps = vv.mapSamps;
	 map< sampleIndex, uint32_t >& mapSamps = vv.mapSamps;
	 map< sampleIndex, uint32_t >::iterator it = mapSamps.begin();
	 while (it != mapSamps.end() ) {
	    updateSampleWet( it->first, it->second );
	    ++it;
	 }
      }
      
      size_t increaseSamples( vector< uint16_t >& x , size_t S, bool dry = false, size_t nThreads = 1 ) {
	 logg << "Starting increaseSamples, nThreads = " << nThreads << endL;
	 
	 size_t nActive = 0;
	 size_t newNSamps = S - samps.size();
	 uniform_int_distribution< node_id > nodeDist(0, n - 1);
	 uniform_real_distribution< float > threshDist(0.0,1.0);

	 thread* wThreads = new thread[ nThreads ];
	 size_t* thrActive = new size_t[ nThreads ];

	 logg << "Prepping for parallelization..." << endL;
	 vector< vector< Sample > > thrSamps( nThreads, vector< Sample >() );
	 vector< vector< map< sampleIndex, uint32_t > > > thrMaps( nThreads,
								   vector< map< sampleIndex, uint32_t > >( n ) );

	 logg << "Sampling..." << endL;
	 size_t thrSampNum = newNSamps / nThreads;
	 for (size_t i = 0; i < nThreads; ++i) {
	    wThreads[i] = thread( thread_add_samps,
				  ref( thrSamps[ i ] ),
				  ref( thrActive[ i ] ),
				  thrSampNum,
				  dry,
				  samps.size() + thrSampNum * i,
				  ref( x ),
				  ref( thrMaps[ i ] ),
				  ref( adjList ),
				  lmax );
	 }

	 for (size_t i = 0; i < nThreads; ++i) {
	    wThreads[i].join();
	 }

	 logg << "Merging samples..." << endL;
	 //merge thrAdj to adjList
	 for ( size_t i = 0; i < nThreads; ++i) {
	    samps.insert( samps.end(), thrSamps[i].begin(), thrSamps[i].end() );
	    nActive += thrActive[ i ];
	    for (size_t v = 0; v < n; ++v ) {
	       adjList[v].mapSamps.insert( thrMaps[ i ][ v ].begin(),
					   thrMaps[ i ][ v ].end() );
	    }
	 }

	 delete [] wThreads;
	 delete [] thrActive;

	 return nActive;
      }

      double avgAct( size_t nAct = 0, size_t nTot = 0 ) {
	 if (nTot == 0) {
	    for (sampleIndex s = 0; s < samps.size(); ++s) {
	       if (samps[s].alive)
		  ++nAct;
	    }
	    nTot = samps.size();
	 } 
	 return n * static_cast<double>( nAct ) / nTot;
	 
      }

      void clearLevels() {
	 for (node_id i = 0; i < n; ++i ) {
	    if (nodeLevels[i] > 0) {
	       updateLevelWet( i, 0 );
	    }
	 }
      }


      bool updateSampleDry( Sample& ss, uint32_t position ) {
	 //cerr << position << ' ' << ss.path.size() << endl;
	 node_id target = ss.path[ position ];
	 //float sthresh = ss.thresholds[ position ];
	 float sthresh;
	 uint8_t aLiveDeadNeither = 0;
	 //vector< node_id > usPath( ss.path.begin(), ss.path.begin() + position );
	 vector< bool > node_membership( n, false );
	 for (size_t i = 0; i <= position; ++i) {
	    node_membership[ ss.path[ i ] ] = true;
	 }

	 do {

	    if (position < ss.path.size() - 1) {
	       
	       sthresh = threshDist( gen );//ss.thresholds[ position ];
	       
	       node_membership[ target ] = true;
	       aLiveDeadNeither = traceBack(target, sthresh, node_membership ); // 0 = neither, 1 = alive, 2 = dead
	       if (target == ss.path[ position + 1 ]) {
		  //sample does not change from here.
	       	  return ss.alive;
	       }
	    } else {
	       sthresh = threshDist( gen );
	       node_membership[ target ] = true;
	       aLiveDeadNeither = traceBack(target, sthresh, node_membership ); // 0 = neither, 1 = alive, 2 = dead
	    }
	    ++position;
	 } while ( aLiveDeadNeither == 0 );

	 if (aLiveDeadNeither == 1) {
	    //this sample is live!
	    return true;
	 } else {
	    //this sample is dead
	    return false;
	 }

	 return false;
      }

      void updateSampleWet( sampleIndex ssIndex, uint32_t position ) {

	 Sample& ss = samps[ ssIndex ];
	 node_id target = ss.path[ position ];
	 float sthresh;// = ss.thresholds[ position ];
	 uint8_t aLiveDeadNeither = 0;

	 size_t oldPathLength = ss.path.size();
	 //REMOVE NODES AFTER THIS POSITION ON THE PATH
	 for (size_t i = position + 1; i < ss.path.size(); ++i) {
	    //sample has changed. Need to possibly update node at this position
	    tinyNode& vv = adjList[ ss.path[ i ] ];
	    map< sampleIndex, uint32_t >::iterator itM = vv.mapSamps.find( ssIndex );
	    if ( itM != vv.mapSamps.end() ) {
	       if (itM->second > position) {
		  vv.mapSamps.erase( itM );
	       }
	    } else {
	       //this should not happen...right?
	    }
	 }

	 vector<bool> node_membership(n, false);
	 for (size_t i = 0; i <= position; ++i) {
	    node_membership[ ss.path[ i ] ] = true;
	 }
	 
	 do {
	    sthresh = threshDist( gen );//ss.thresholds[ position ];
	    //target = ss.path[ position ];
	       
	    aLiveDeadNeither = traceBack(target, sthresh, node_membership ); // 0 = neither, 1 = alive, 2 = dead
	    
	    // if (aLiveDeadNeither == 0) {
	    //    tinyNode& vv = adjList[ target ]; //adjList[ ss.path[ position  ] ];
	    //    map< sampleIndex, uint32_t >::iterator itM = vv.mapSamps.find( ssIndex );
	    //    if (itM == vv.mapSamps.end()) {
	    // 	  vv.mapSamps[ ssIndex ] = position;
		  
	    //    } else {
	    // 	  //already in this sample.
		  
	    //    }
	    // }

	    if (!(node_membership[ target ])) {
	       ss.path.push_back( target );
	       node_membership[ target ] = true;
	       tinyNode& vv = adjList[ target ]; //adjList[ ss.path[ position  ] ];
	       vv.mapSamps[ ssIndex ] = position;
	    } else {
	       //did not trace back (sample should be alive or dead and loop terminating)
	       
	    }
	    ++position;
	 } while ( aLiveDeadNeither == 0 );

	 if (aLiveDeadNeither == 1) {
	    //this sample is live!
	    ss.alive = true;
	 } else {
	    //this sample is dead
	    ss.alive = false;
	 }

      }
      
      double marginalGainNoUpdate( node_id& v ) {
	 double res = 0.0;
	 set< sampleIndex > nodeSamples;
	 if ( nodeLevels[ v ] < lmax ) {
	    ++nodeLevels[ v ];
	    for (size_t i = 0 ; i < adjList[v].mySamps.size(); ++i ) {
	       Sample& ss = samps[ adjList[ v ].mySamps[ i ].s ];
	       bool oldStatus = ss.alive;
	       bool newStatus = updateSampleDry( ss, adjList[ v ].mySamps[ i ].p );
	       if ( newStatus != oldStatus ) {
		  if (oldStatus == true) {
		     //we lost one
		     res -= 1.0;
		  }
		  else {
		     //Who once was lost, is now saved
		     res += 1.0;
		  }
	       }
	    
	    }
	    --nodeLevels[ v ];
	       
	 } else {
	    res = -1.0 * samps.size();  
	 }

	 return res;
      }

      double marginalGainNoUpdateWithMap( node_id& v, size_t l = 1 ) {
	 //logg << "margin gain: " << v << ", " << l << endL;
	 double res = 0.0;
	 set< sampleIndex > nodeSamples;
	 if ( nodeLevels[ v ] <= lmax - l ) {
	    nodeLevels[ v ] += l;
	    map< sampleIndex, uint32_t >::iterator it1 = adjList[ v ].mapSamps.begin();
	    while ( it1 != adjList[ v ].mapSamps.end() ) {
	       Sample& ss = samps[ it1->first ];
	       bool oldStatus = ss.alive;
	       bool newStatus = updateSampleDry( ss, it1->second );
	       if ( newStatus != oldStatus ) {
		  if (oldStatus == true) {
		     //we lost one
		     res -= 1.0;
		  }
		  else {
		     //Who once was lost, is now saved
		     res += 1.0;
		  }
	       }

	       ++it1;
	    }

	    nodeLevels[ v ] -= l;
	       
	 } else {
	    res = -1.0 * samps.size();  
	 }

	 return res;
      }
      
      double marginalGain( node_id& v ) {
	 	 
	 size_t nVAct = 0;
	 for (size_t i = 0 ; i < adjList[v].mySamps.size(); ++i ) {
	    if ( samps[ adjList[ v ].mySamps[ i ].s ].alive )
	       ++nVAct;
	 }

	 updateLevel( v, nodeLevels[ v ] + 1 );
	 size_t nVActAfter = 0;
	 for (size_t i = 0 ; i < adjList[v].mySamps.size(); ++i ) {
	    if ( samps[ adjList[ v ].mySamps[ i ].s ].alive )
	       ++nVActAfter;
	 }

	 updateLevel( v, nodeLevels[ v ] - 1 );

	 return ( nVActAfter - nVAct );
      }

      double lazyGreedy( size_t k ) {
	 size_t solSize = 0;

	 priority_queue< mpair, vector< mpair >, comparePair > Q;
	 
	 for ( node_id i = 0; i < n; ++i ) {
	    mpair tmp;
	    tmp.id = i;
	    tmp.value = samps.size() + 1;

	    Q.push( tmp );
	 }

	 vector< bool > b_valid; 
	 while (solSize < k) {
          b_valid.assign( n, false );

	    while ( true ) {
	       mpair pmax = Q.top();
	       Q.pop();
	       if ( b_valid[ pmax.id ] ) {
		  updateLevelWet( pmax.id, nodeLevels[ pmax.id ] + 1 );
		  //updateLevel( pmax.id, nodeLevels[ pmax.id ] + 1 );
		  if ( nodeLevels[ pmax.id ] < lmax ) {
		     pmax.value = samps.size() + 1;
		     b_valid[ pmax.id ] = false;
		     Q.push( pmax );
		  }
		  break;
	       } else {
		  double tmpGain = marginalGainNoUpdateWithMap( pmax.id );
		  //double tmpGain = marginalGainNoUpdate( pmax.id );
		  pmax.value = tmpGain;
		  Q.push( pmax );
		  b_valid[ pmax.id ] = true;
	       }
	    }
	    ++solSize;
	 }
	 
	 return avgAct();
      }
      
      double simpleGreedy( size_t k ) {
	 size_t solSize = 0;
	 double maxGain = -1.0 * samps.size();
	 node_id maxV = 0;
	 while ( solSize < k ) {
	    //cerr << "Greedy at: " << solSize << endl;
	    maxV = 0;
	    maxGain = -1.0 * samps.size();
	    for (node_id i = 0; i < n; ++i) {
	       if (!(nodeLevels[i] == lmax)) {
		  //double tmpGain = marginalGainNoUpdate( i );
		  double tmpGain = marginalGainNoUpdateWithMap( i );
		  //cerr << tmpGain << ' ' << maxGain << endl;
		  if (tmpGain >= maxGain) {
		     maxGain = tmpGain;
		     maxV = i;
		  }
	       }
	    }

	    //Add in maxV
	    //updateLevel( maxV, nodeLevels[ maxV ] + 1 );
	    updateLevelWet( maxV, nodeLevels[ maxV ] + 1 );
	    //cerr << maxV << ' ' << (unsigned) lmax << ' ' << (unsigned) nodeLevels[ maxV ] << endl;
	    ++solSize;
	 }

	 return avgAct();
      }


      double max_inc_parallel( node_id& v, size_t nThreads = 1, size_t amt = 1 ) {
			      
	 struct wkArgs {
	    vector< uint16_t >& wknodeLevels;
	    uint16_t& llmax;
	    vector< tinyNode >& aadjList;
	    vector< Sample >& ssamps;
	    vector< double >& Increments;
	    size_t aamt;
	 } wkargs = { nodeLevels, lmax, adjList, samps, increments, amt };

	 class wkWork {
	 public:
	    static void run(
			    vector< node_id >::iterator itBegin,
			    vector< node_id >::iterator itEnd,
			    wkArgs& wkargs,
			    mutex& mtx ) {
	       uniform_real_distribution< float > sampleDist(0.0,1.0);
	       random_device rd;
	       mt19937 gen( rd() );
	       while (itBegin != itEnd) {
		  
		  wkargs.Increments[ *itBegin ] = ext_marginal_gain( *itBegin,
								     wkargs.ssamps,
								     wkargs.wknodeLevels,
								     wkargs.aadjList,
								     wkargs.llmax,
								     sampleDist,
								     gen,
								     wkargs.aamt );
		  ++itBegin;
	       }
	    }
	 } wkwork;

	 increments.assign(n, 0.0);
	 vector< node_id > vNodes(n,0);
	 for (size_t i = 1; i < n; ++i) {
	    vNodes[ i ] = vNodes[ i - 1 ] + 1;
	 }

	 logg << "Starting maxInc, nThreads = " << nThreads << endL;
	 
	 parallelWork( vNodes, wkargs, wkwork, nThreads );

	 //increments vector should now be computed.
	 //get max
	 double maxGain = -1.0* samps.size();
	 double tmpGain;
	 for (node_id i = 0; i < n; ++i) {
	    tmpGain = increments[ i ];
	    if (tmpGain >= maxGain) {
	       maxGain = tmpGain;
	       v = i;
	    }
	 }

	 return n * maxGain / samps.size();
      }

      size_t binSearchPivot( node_id& i, double tau, size_t& k, size_t& sizeSol ) {
	 //cerr << "starting bsp..." << endl;
	 tau = tau * samps.size() / n;
	 if ( increments[ i ] < tau )
	    return 0;
	 
	 size_t ls = 1;
	 
	 size_t lt1 = lmax - nodeLevels[ i ];
	 size_t lt2 = k - sizeSol;
	 
	 if (lt1 == 0)
	    return 0;

	 
	 size_t lt;
	 if (lt1 < lt2) 
	    lt = lt1;
	 else
	    lt = lt2;

	 //if ( marginalGainNoUpdateWithMap( i, 1 ) < tau )
	 //return 0;
	 
	 
	 if ( marginalGainNoUpdateWithMap( i, lt ) >= lt * tau )
	    return lt;

	 ///sscerr << "point 1\n";

	 while (lt > ls + 1) {
	    logg << "BSP: " << ls << " " << lt << endL;
	    
	    size_t mp = (lt + ls) / 2;
	    if (marginalGainNoUpdateWithMap( i, mp ) >= mp * tau ) {
	       ls = mp;
	    } else {
	       lt = mp;
	    }
	 }


	 //cerr << "maxinc done." << endl;
	 return ls;
	 
      }


      void coverSamps( node_id & v ) {
	 map< sampleIndex, uint32_t >::iterator it1 = adjList[ v ].mapSamps.begin();
	 while ( it1 != adjList[v].mapSamps.end() ) {
	    samps[it1 -> first].alive = true;
	    
	    ++it1;
	 }
      }

      size_t sampsCovered( node_id& v ) {
	 if (adjList[ v ].mapSamps.empty()) {
	    return 0;
	 }
	 
	 //iterate through all samples of v,
	 //and count how many are not alive
	 size_t scov = 0;
	 map< sampleIndex, uint32_t >::iterator it1 = adjList[ v ].mapSamps.begin();
	 
	 while ( it1 != adjList[v].mapSamps.end() ) {

	    if ( !(samps[it1->first].alive) ) {
	       //if ( !(samps[it1->first].path[0] == v ) ) {
	       ++scov;
	       //}
	    }
	    
	    ++it1;
	 }

	 return scov;
      }

      void resetSamps() {
	 for (size_t s = 0; s < samps.size(); ++s) {
	    samps[ s ].alive = false;
	 }
      }
      
      double max_covg( vector< uint16_t >& ss, size_t k ) {
	 ss.assign( n, 0 );
	 //All samples are initially dead.
	 //When a sample is covered, we will set its status to "live"
	 size_t totCovered = 0;
	 size_t maxCovered;
	 node_id maxV;
	 for (size_t i = 0; i < k; ++i) {
	    maxCovered = 0;
	    maxV = 0;
	    for (node_id v = 0; v < n; ++v) {
	       size_t vSampsCovered = sampsCovered( v );
	       if (vSampsCovered > maxCovered) {
		  maxCovered = vSampsCovered;
		  maxV = v;
	       }
	    }

	    ss[ maxV ] = 1;
	    totCovered += maxCovered;
	    coverSamps( maxV );
	 }

	 resetSamps();
	 
	 return avgAct( totCovered, samps.size() );
      }

      double best_single_seed( double epsi, size_t nThreads ) {
	 size_t nSamps = log ( n )/ log(10) / (epsi*epsi);
	 logg << "bss, nSamps: " << nSamps << endL;
	 vector< uint16_t > vZero( n, 0 );
	 increaseSamples( vZero, nSamps, false, nThreads );

	 double zeroAct = avgAct();
	 logg << "bss, zeroAct: " << zeroAct << endL;
	 increments.assign( n, 0.0 );
	 node_id v;
	 //clearSamples();
	 return (zeroAct + max_inc_parallel( v, nThreads, lmax ));
      }
      
      double fastGreedy( size_t k, double epsi,
			 double LB,
			 size_t nThreads,
			 double kappa = 0.9,
			 double delta = 0.9 ) {
	 node_id v;
	 increments.assign( n, 0.0 );
	 //double M = maxIncrement( v );
	 double M = max_inc_parallel( v, nThreads );
	 double m = M;
	 double mPrime;
	 double beta = 1.0;
	 size_t sizeSol = 0;
	 do {
	    logg << "FastGreedy iteration, m = " << m << endL;
	    
	    double tau = beta * kappa * m;
	    
	    for (node_id i = 0; i < n; ++i) {
	       size_t l = binSearchPivot( i, tau, k, sizeSol );
	       ///x[ i ] += l;
	       if (l > 0) {
		  logg << "FastGreedy adding l,i: " << l << "," << i << endL;
		  updateLevelWet( i, nodeLevels[ i ] + l );
		  sizeSol += l;
		  if (sizeSol == k) {
		     return avgAct();
		  }
	       }
	    }
	    
	    mPrime = m;
	    m = max_inc_parallel( v, nThreads );
	    
	    if (m > kappa * mPrime) 
	       beta = beta * delta;

	    //cerr << m << ' ' << epsi*LB / 4;
	 } while ( m > epsi*LB / 4 ); // epsi*LB);

	 logg << "FastGreedy terminating: " << m << " " << epsi*LB / 4 << endL;
	 
	 return avgAct();
      }
      
      double estimateInfluence( vector< uint16_t >& x, size_t nSamples = 10000, bool dry = false,
				size_t nThreads = 1 ) {
	 clearSamples();

	 size_t nAct = increaseSamples( x, nSamples, dry, nThreads );

	 size_t actSamples = (nSamples / nThreads);
	 actSamples *= nThreads;

	 return avgAct( nAct, actSamples );
      }

      // double estimateInfluenceDist( vector< uint16_t >& x, size_t nSamples = 10000 ) {
      // 	 clearSamples();

      // 	 increaseSamplesDist( x, nSamples );

      // 	 return avgActDist();
      // }
      
      void init_empty_graph() {
	 tinyNode emptyNode;
	 adjList.assign(n, emptyNode);
      }

      size_t getDegree( node_id v ) {
	 return adjList[v].OUTneis.size() + adjList[v].INneis.size();
      }

      // unsigned char getEdgeWeight( node_id from, node_id to ) {
      // 	 unsigned char w;
      // 	 vector< tinyEdge >& v1 = adjList[ from ].neis;

      // 	 auto it = v1.begin();
      // 	 while (it != v1.end()) {
      // 	    if (it->getId() >= to)
      // 	       break;

      // 	    ++it;
      // 	 }

      // 	 if (it != v1.end()) {
      // 	    if (it->getId() == to) {
      // 	       w = it->weight;
      // 	       return w;
      // 	    }
      // 	 }

      // 	 return 0;
      // }
    
      void read_bin( string fname ) {
	 this->adjList.clear();
	 this->m = 0;
	 this->n = 0;
      
	 ifstream ifile ( fname.c_str(), ios::in | ios::binary );
	 unsigned n;
	 ifile.read( (char*) &n, sizeof( node_id ) );
	 ifile.read( (char*) &preprocessTime, sizeof(double) );
      
	 this->n = n;

	 init_empty_graph();
	 size_t ss;
	 tinyEdge temp;
	 node_id nei_id;
	 float w;

	 for ( unsigned i = 0; i < n; ++i ) {

	    ifile.read( (char*) &ss, sizeof( size_t ) );

	    adjList[i].OUTneis.assign( ss, temp );
	    this->m += ss;
	    for (unsigned j = 0; j < ss; ++j) {
	       ifile.read( (char*) &nei_id, sizeof( node_id ) );
	       ifile.read( (char*) &w, sizeof( float ) );
	       adjList[i].OUTneis[j].target = nei_id;
	       adjList[i].OUTneis[j].weight = w;
	    }

	    ifile.read( (char*) &ss, sizeof( size_t ) );
	    
	    adjList[i].INneis.assign( ss, temp );
	    this->m += ss;
	    for (unsigned j = 0; j < ss; ++j) {
	       ifile.read( (char*) &nei_id, sizeof( node_id ) );
	       ifile.read( (char*) &w, sizeof( float ) );
	       adjList[i].INneis[j].target = nei_id;
	       adjList[i].INneis[j].weight = w;
	    }
	 }

	 //      logg(INFO, "Sorting neighbor lists..." );
	 // if (preprocessTime == 0.0) {
	 //    clock_t t_start = clock();
	 //    for (unsigned i = 0; i < n; ++i) {
	 //       sort( adjList[i].neis.begin(), adjList[i].neis.end(), tinyEdgeCompare );
	 //    }
	 //    preprocessTime = double (clock() - t_start) / CLOCKS_PER_SEC;
	 //    cout << "Preprocessing took " << preprocessTime << "s\n";
	 // }
      }

    
      // void write_bin( string fname ) {
      // 	 ofstream ifile ( fname.c_str(), ios::out | ios::binary );
      // 	 ifile.write( (char*) &n, sizeof( node_id ) );
      // 	 ifile.write( (char*) &preprocessTime, sizeof(double) );
      
      // 	 size_t ss;
      // 	 tinyEdge temp;
      // 	 node_id nei_id;
      // 	 unsigned char w;
      // 	 for ( unsigned i = 0; i < n; ++i ) {
	
      // 	    ss = adjList[i].neis.size();
      // 	    ifile.write( (char*) &ss, sizeof( size_t ) );

      // 	    for (unsigned j = 0; j < ss; ++j) {
      // 	       nei_id = adjList[i].neis[j].target;
      // 	       w = adjList[i].neis[j].weight;
      // 	       ifile.write( (char*) &nei_id, sizeof( node_id ) );
      // 	       ifile.write( (char*) &w, sizeof( unsigned char ) );
      // 	    }
      // 	 }

      // 	 ifile.close();
      // }

      // vector< tinyEdge >::iterator findEdgeInList( node_id source, node_id target ) {
      // 	 vector< tinyEdge >& v1 = adjList[source].neis;
      // 	 for (auto it = v1.begin();
      // 	      it != v1.end();
      // 	      ++it ) {
      // 	    if (it->target == target)
      // 	       return it;
      // 	 }

      // 	 return v1.end(); //Edge not found
      // }

      void print( ostream& os ) {
	 for (size_t i = 0; i <adjList.size(); ++i) {
	    os << i << endl;
	    os << "OUT:" << endl;
	    for (size_t j = 0; j < adjList[i].OUTneis.size(); ++j) {
	       os << adjList[i].OUTneis[j].target << ' '
		  << adjList[i].OUTneis[j].weight << ' ';
	    }
	    os << endl;
	    os << "IN:" << endl;
	    for (size_t j = 0; j < adjList[i].INneis.size(); ++j) {
	       os << adjList[i].INneis[j].target << ' '
		  << adjList[i].INneis[j].weight << ' ';
	    }
	    os << endl;
	 }
      }

      void printCIM( ostream& os ) {
	 //count edges
	 size_t m = 0;
	 for (node_id i = 0; i < n; ++i) {
	    m += adjList[ i ].INneis.size();
	 }
	 os << n << " " << m << endl;
	 for (size_t i = 0; i < n; ++i) {
	    for (size_t j = 0; j < adjList[i].INneis.size(); ++j) {
	       os << adjList[i].INneis[j].target << ' '
		  << i << ' ' << adjList[i].INneis.size() << endl;
	    }
	 }
      }

   };
    
}

#endif
