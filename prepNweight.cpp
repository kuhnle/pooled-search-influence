#include "simplify-graph.cpp"
#include <iostream>
#include <string>

using namespace std;

using namespace simpgraph;

int main( int argc, char** argv ) {
   if (argc < 2) {
      cout << "Usage: " << argv[0] << " <input file> <output file> <alpha>" << endl;
      cout << "Input must be graph in format suitable for simplifyGraph::read_edge_list" << endl;
      cout << "First line should be: <Node estimate (can be 0)> <isWeighted> <isDirected>"  << endl;
      cout << "Each line thereafter: <from_id> <to_id> <weight (float)>"  << endl;
      cout << "Graph is simplified, then written to output file (as directed binary)" << endl;
      exit(1);
   }
   
  string inputGraphName ( argv[1] );
  string alpha (argv[ 3 ] );
  float falpha = stod( alpha );
		
  simplifyGraph g;

  g.read_edge_list( inputGraphName );
  Logger logg;
  logg << "Removing isolates..." << endL;
  g.remove_isolates();
  logg << "Renumbering vertices..." << endL;
  g.renumber_vertices();
  logg << "Reweighting (in weights only)..." << endL;
  g.reweight ( falpha );
  cout << "Writing binary file..." << endl;
  string outputFName( argv[2] );
  g.write_bin( outputFName );

  return 0;
}
