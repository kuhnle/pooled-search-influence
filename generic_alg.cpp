#ifndef GENERIC_ALG
#define GENERIC_ALG

#include <random>
#include "util.cpp"
#include "logger.cpp"
#include "results_handler.cpp"
#include <thread>
#include <unistd.h>
using namespace std;

mutex mtx;

enum Algs {GREEDY, GPARETO, LAPS, QIAN, OPT, LOCAL, LAPSFB, LAPSLM};
random_device rdAlg;

//mt19937 genAlg;
uniform_real_distribution< double > uni( 0.0, 1.0 );

template <typename graph_type>
struct tArgs {
   Algs alg;
   graph_type g;
   string graphFileName;
   string outputFileName = "";
   string logFileName = "";
   ofstream* ofLog;
   string testVectorFName = "";
   size_t nThreads = 1;
   bool printOnly = false;
   Logger* logg;
   double tElapsed;
   double wallTime;
   bool testSampling = false;
   size_t k = 0;
   size_t K = 0;
   bool quiet = false;
   bool testInfluence = false;
   double estInfluence = 0.0;
   double delta = 0.5;
   bool randomSelection = false;
   size_t ell = 0;
   double epsi = 0.3;
   size_t valGreedy = 0;
   double p = 0.25;
   double ew = 0.1;
   size_t N = 1;
   size_t S = 50000;
   double T = 50.0; //Number of greedy iterations
   double restartInterval = static_cast<double>(T);
   bool lazySearchHeuristic = false;
   double outputInterval = 0.01;
   bool largeMutations = false;
   
   tArgs( const tArgs& rhs ) {
      alg = rhs.alg;
      g.assign( rhs.g );
      graphFileName = rhs.graphFileName;
      outputFileName = rhs.outputFileName;
      logFileName = rhs.logFileName;
      ofLog = rhs.ofLog;
      logg = new Logger();
      k = rhs.k;
      delta = rhs.delta;
      nThreads = rhs.nThreads;
      randomSelection = rhs.randomSelection;
      ell = rhs.ell;
      epsi = rhs.epsi;
      valGreedy = rhs.valGreedy;
      p = rhs.p;
      ew = rhs.ew;
      N = rhs.N;
      S = rhs.S;
      T = rhs.T;
      restartInterval = rhs.restartInterval;
      lazySearchHeuristic = rhs.lazySearchHeuristic;
      outputInterval = rhs.outputInterval;
   }

   tArgs() {

   }

   void print( ostream& os ) {
      os << "#params:\n"
	 << "#input=" << graphFileName << endl
	 << "#k=" << k << endl
	 << "#ell=" << ell << endl
	 << "#alg=" << alg << endl
	 << "#Reps=" << N << endl
	 << "#nThreads=" << nThreads << endl
	 << "#epsi=" << epsi << endl
	 << "#T=" << T << endl
	 << "#randSelect=" << randomSelection << endl
	 << "#p=" << p << endl;
   }
};

template <typename graph_type>
struct subArgs {
   vector< vector< bool > >* basePool;
   vector< size_t >* vals;
   size_t pos;
   graph_type* g;
   size_t* nEvals;
   size_t s;
   uniform_int_distribution< size_t >* dist;
   vector< vector< size_t > >* basePoolIds;
   mt19937* gen;
   subArgs( vector< vector< bool > >* inBP,
	    vector< size_t >* vals_in,
	    size_t pos_in,
	    graph_type* g_in,
	    size_t* nEvals_in,
	    size_t s_in,
	    uniform_int_distribution< size_t >* dist_in,
	    vector< vector< size_t > >* inBPid,
	    mt19937* genIn) {
      basePool = inBP;
      vals = vals_in;
      pos = pos_in;
      nEvals = nEvals_in;
      g = g_in;
      s = s_in;
      dist = dist_in;
      basePoolIds=inBPid;
      gen=genIn;
   }
};

template <typename graph_type>
bool replace( vector< bool >& ss,
	      vector< size_t >& ssId,
	      vector< vector< bool > >& pool,
	      vector< vector< size_t > >& poolId,
	      vector< size_t >& vals, size_t& tmpPos, size_t& tmpVal,
	      size_t maxSize, size_t& nEvals,
	      size_t (*func)( size_t&, graph_type&, vector<size_t>& ),
	      graph_type& g ) {
   tmpPos = ssId.size();
   tmpVal = 0;

   if (tmpPos <= maxSize) {
      size_t valPos = vals[ tmpPos ];
      tmpVal = func( nEvals, g, ssId );
      if (tmpVal > valPos) {
	 vals[ tmpPos ] = tmpVal;
	 pool[ tmpPos ].swap( ss );
	 poolId[ tmpPos ].swap( ssId );
	 return true;
      }
   }
   return false;
}

void mutate( vector< bool >& ss,
	     vector< size_t >& ssId,
	     size_t n,
	     binomial_distribution< size_t >& binDist,
	     uniform_int_distribution< size_t >& allDist,
	     mt19937& gen2, bool large = false ) {
   size_t nMutate = binDist( gen2 );

   if (large) {
      do {
	 nMutate = binDist(gen2);
      } while (nMutate < 2);
   }
   
   vector< size_t > mutateIdx;

   if (nMutate >= n)
      nMutate = 2;
   
   for (size_t i = 0; i < nMutate; ++i) {
      size_t idx;
      do {
	 idx = allDist( gen2 );
      } while ( vcontains( mutateIdx, idx ) );

      mutateIdx.push_back( idx );
   }

   for (size_t j = 0; j < nMutate; ++j) {
      size_t& i = mutateIdx[ j ];
      if ( ss[i] ) {
	 ss[i] = false;
	 erase_from_vector( ssId, i );
      } else {
	 ss[i] = true;
	 ssId.push_back( i );
      }
   }
   
   // for (size_t i = 0; i < n; ++i) {
   //    rnum = uni( gen2 );
   //    if (rnum < prob) {
   // 	 if ( ss[i] ) {
   // 	    ss[i] = false;
   // 	    erase_from_vector( ssId, i );
   // 	 } else {
   // 	    ss[i] = true;
   // 	    ssId.push_back( i );
   // 	 }
   //    }
   // }
}

template <typename graph_type>
size_t lazyForward( size_t (*func)( size_t&, graph_type&, vector<size_t>& ),
		    subArgs< graph_type >& lfArgs ) {
   mt19937& genAlg = *(lfArgs.gen);
   vector< vector< bool > >& basePool = *(lfArgs.basePool);
   vector< size_t >& vals = *(lfArgs.vals);
   size_t pos = lfArgs.pos;
   graph_type& g = *(lfArgs.g);
   size_t& nEvals = *(lfArgs.nEvals);
   size_t s = lfArgs.s;
   uniform_int_distribution< size_t >& dist = *(lfArgs.dist);
   vector< vector< size_t > >& bpID = *(lfArgs.basePoolIds);

   
   vector<bool > ss = basePool[ pos ]; //seed set
   vector< size_t > ssIds = bpID[ pos ]; //seed set
   
   size_t maxInc = 0;
   size_t maxV = 0;
   size_t inc;

   //vector< bool > R( g.n, false );
   vector< size_t > R;

   for (size_t i = 0; i < s; ++i) {
      //Add an element to R
      size_t sampled;
      do {
	 sampled = dist( genAlg );
      } while ( false );

      R.push_back( sampled );
   }
   
   for (size_t i = 0; i < R.size(); ++i) {
      size_t& v = R[i];
      if (!ss[v]) {
	 ssIds.push_back( v );
	    
	 inc = func( nEvals, g, ssIds );
	 
	 if ( inc > maxInc ) {
	    maxInc = inc;
	    maxV = v;
	 }

	 ssIds.pop_back();
      }
   }

   size_t newVal = maxInc;
   if (newVal > vals[ pos + 1 ] ) {
      ss[maxV] = true;
      basePool[ pos + 1 ] = ss;
      vals[ pos + 1 ] = newVal;
      bpID[ pos + 1 ] = bpID[ pos ];
      bpID[ pos + 1 ].push_back( maxV );
      //cerr << "replace at " << pos + 1 << endl;
      return newVal;
   }
   
   return vals[ pos + 1 ];
}

template <typename graph_type>
size_t lazyForwardAndBack( size_t (*func)( size_t&, graph_type&, vector<size_t>& ),
			   subArgs< graph_type >& lfArgs ) {
   mt19937& genAlg = *(lfArgs.gen);
   vector< vector< bool > >& basePool = *(lfArgs.basePool);
   vector< size_t >& vals = *(lfArgs.vals);
   size_t pos = lfArgs.pos;
   graph_type& g = *(lfArgs.g);
   size_t& nEvals = *(lfArgs.nEvals);

   uniform_int_distribution< size_t >& dist = *(lfArgs.dist);
   vector< vector< size_t > >& bpID = *(lfArgs.basePoolIds);

   
   vector<bool > ss = basePool[ pos ]; //seed set
   vector< size_t > ssIds = bpID[ pos ]; //seed set
   
   //vector< bool > R( g.n, false );
   vector< size_t > R;

   //Add an element to R
   size_t sampled;
   //cerr << "base: ";
   //print_vector( ssIds, cerr );
   
   do {
      sampled = dist( genAlg );
   } while (ss[sampled]);

   //cerr << "sampled: " << sampled;
   ssIds.push_back( sampled );
   //cerr << "new vec: ";
   //print_vector( ssIds, cerr );
	    
   size_t newVal = func( nEvals, g, ssIds );

   //cerr << "val: " << newVal << endl;

   //char c = getchar();
   
   if (newVal > vals[ ssIds.size() ] ) {
      ss[sampled] = true;
      basePool[ ssIds.size() ] = ss;
      vals[ ssIds.size() ] = newVal;
      bpID[ ssIds.size() ] = ssIds;
      
   }

   uniform_int_distribution< size_t > dist2( 0, ssIds.size() - 1);
   sampled = dist2( genAlg );
   ss[ ssIds[ sampled ] ] = false;
   ssIds.erase( ssIds.begin() + sampled);
   
   newVal = func( nEvals, g, ssIds );
   if (newVal > vals[ ssIds.size() ]) {
      basePool[ ssIds.size() ] = ss;
      vals[ ssIds.size() ] = newVal;
      bpID[ ssIds.size() ] = ssIds;
   }

   return 0;
}

template <typename graph_type>
size_t lazyBackAndForward( size_t (*func)( size_t&, graph_type&, vector<size_t>& ),
			   subArgs< graph_type >& lfArgs ) {
   mt19937& genAlg = *(lfArgs.gen);
   vector< vector< bool > >& basePool = *(lfArgs.basePool);
   vector< size_t >& vals = *(lfArgs.vals);
   size_t pos = lfArgs.pos;
   graph_type& g = *(lfArgs.g);
   size_t& nEvals = *(lfArgs.nEvals);

   uniform_int_distribution< size_t >& dist = *(lfArgs.dist);
   vector< vector< size_t > >& bpID = *(lfArgs.basePoolIds);

   
   vector<bool > ss = basePool[ pos ]; //seed set
   vector< size_t > ssIds = bpID[ pos ]; //seed set

   uniform_int_distribution< size_t > dist2( 0, ssIds.size() - 1);
   size_t sampled = dist2( genAlg );
   ss[ ssIds[ sampled ] ] = false;
   ssIds.erase( ssIds.begin() + sampled);
   
   size_t newVal = func( nEvals, g, ssIds );
   if (newVal > vals[ ssIds.size() ]) {
      basePool[ ssIds.size() ] = ss;
      vals[ ssIds.size() ] = newVal;
      bpID[ ssIds.size() ] = ssIds;
   }
   
   do {
      sampled = dist( genAlg );
   } while (ss[sampled]);
   
   ssIds.push_back( sampled );
   ss[sampled] = true;
   newVal = func( nEvals, g, ssIds );
	 
   if (newVal > vals[ ssIds.size() ] ) {
      basePool[ ssIds.size() ] = ss;
      vals[ ssIds.size() ] = newVal;
      bpID[ ssIds.size() ] = ssIds;
   }

   return 0;
}

template< typename graph_type >
size_t greedyBackward( vector< vector< bool > >& pool,
		       vector< size_t >& vals,
		       size_t pos,
		       graph_type& g,
		       size_t& nEvals,
		       size_t (*func)( size_t&, graph_type&, vector<bool>& )
		       ) {
   vector< bool > ss = pool[ pos ];
   vector< size_t > ssIds;
   for (size_t v = 0; v < g.n; ++v) {
      if (ss[ v ] == 1) {
	 ssIds.push_back( v );
      }
   }

   size_t maxCov = 0;
   size_t maxV = 0;
   for (size_t i = 0; i < ssIds.size(); ++i) {
      size_t u = ssIds[ i ];
      ss[ u ] = false;
	    	    
      //compute value of new ss
      size_t cov = func( nEvals, g, ss );
      if (cov > maxCov) {
	 maxCov = cov;
	 maxV = u;
      }
      ss[u] = true;
   }

   size_t newVal = maxCov;
   ss[ maxV ] = false;
  if (newVal > vals[ pos - 1 ] ) {
      pool[ pos - 1 ] = ss;
      vals[ pos - 1 ] = newVal;
   }
	 
   return vals[ pos - 1 ];
}

template <typename graph_type>
size_t lazyBackward( size_t (*func)( size_t&, graph_type&, vector<size_t>& ),
		     subArgs< graph_type >& lfArgs ) {
   mt19937& genAlg = *(lfArgs.gen);
   vector< vector< bool > >& pool = *(lfArgs.basePool);
   vector< size_t >& vals = *(lfArgs.vals);
   size_t pos = lfArgs.pos;
   graph_type& g = *(lfArgs.g);
   size_t& nEvals = *(lfArgs.nEvals);
   size_t s = lfArgs.s;
   uniform_int_distribution< size_t >& dist = *(lfArgs.dist);
   vector< vector< size_t > >& bpID = *(lfArgs.basePoolIds);

   vector< size_t > R;
   vector< bool > ss = pool[ pos ];
   vector< size_t > ssIds = bpID[ pos ];
   
   if (s >= pos) {
      R = ssIds;
   } else {
      for (size_t i = 0; i < s; ++i) {
	 //Add an element to R
	 size_t sampled;
	 do {
	    sampled = dist( genAlg );
	 } while ( sampled >= ssIds.size() );

	 R.push_back( ssIds[ sampled ] );
      }
   }

   size_t maxCov = 0;
   size_t maxV = 0;
   //size_t maxPos = 0;
   for (size_t i = 0; i < R.size(); ++i) {
      size_t u = R[ i ];
      for (auto it = ssIds.begin(); it != ssIds.end(); ++it) {
	 if (*it == u) {
	    ssIds.erase( it );
	    break;
	 }
      }

      //compute value of new ss
      size_t cov = func( nEvals, g, ssIds );
      if (cov > maxCov) {
	 maxCov = cov;
	 maxV = u;
	 //maxPos = i;
      }

      ssIds.push_back( u );
   }

   size_t newVal = maxCov;
   ss[ maxV ] = false;
   if (newVal > vals[ pos - 1 ] ) {
      pool[ pos - 1 ] = ss;
      vals[ pos - 1 ] = newVal;

      for (auto it = ssIds.begin(); it != ssIds.end(); ++it) {
	 if (*it == maxV) {
	    ssIds.erase( it );
	    break;
	 }
      }
      
      bpID[ pos - 1 ] = ssIds;
      
   }
	 
   return vals[ pos - 1 ];
}

template <typename graph_type>
size_t rps_main( tArgs< graph_type >& args,
		 size_t (*func)( size_t&, graph_type&, vector<size_t>& ss ) ) {
   random_device rdAlg;
   mt19937 genAlg( rdAlg() );
   (*args.logg) << SECT;
   if (!args.randomSelection) {
     (*args.logg) << "BLPO+" << endL;
   } else {
     (*args.logg) << "BLPO" << endL;
   }
   (*args.logg) << INFO;

   if (args.g.lmax != 1) {
      args.g.lmax = 1;

      if (args.K > 0)
	 args.k = args.K;
   }

   double p = args.p;
   graph_type& g = args.g;
   
   uniform_int_distribution<size_t> distLazy(0, args.g.n - 1);

   size_t lazySize = static_cast<double>( args.g.n ) / args.k * log( 1.0 / args.epsi );
   size_t sampIter;
   if (args.ell == 0)
      sampIter = 1;
   else
      sampIter = lazySize / args.ell + 1;

   (*args.logg) << "lazySize = " << lazySize << endL;
   (*args.logg) << "sampIter = " << sampIter << endL;
   
   bool boutput = false;
   if (args.outputFileName != "") {
      //ofile.open( args.outputFileName.c_str() );
      boutput = true;
   }

   size_t valGreedy;
   if (args.valGreedy > 0.0)
      valGreedy = args.valGreedy;
   else {
      valGreedy = greedy( args );
      args.valGreedy = valGreedy;
      (*args.logg) << "Greedy finished with val: " << valGreedy << endL;
   }

   vector< vector< bool > > pool;
   vector< vector< size_t > > poolID;
   vector< size_t > vals;
   vector< size_t > sampsForPool;
   vector<bool> ss;
   vector<size_t> ssIds;
   size_t k = args.k;
   size_t n = args.g.n;
   size_t maxSize = 2 * k;
   size_t inductiveBin = 0;
   uniform_int_distribution<size_t> distLazyBack(0, maxSize);
   vector< size_t > backPos;
   {
      vector< bool > empty( args.g.n, false );
      pool.assign( maxSize + 1, empty );
      vals.assign( maxSize + 1,  0 );
      vector< size_t > emptyID;
      poolID.assign( maxSize + 1, emptyID );
      //basePool = pool;
      sampsForPool = vals;
      backPos.assign( maxSize + 1, 0 );
   }

   size_t maxVal = 0;
   size_t nEvals = 0;
   size_t toss, tmpVal, pos;
   size_t T = args.T * n * k;
   uniform_int_distribution< size_t > selectDist( 0, maxSize );
   uniform_int_distribution< size_t > coinToss( 0, 1 );

   size_t iter = 0;
   double outTenth = 0.0;
   bool bcont;
   vector< size_t > notConverged;
   size_t nEvalsAtRestart = 0;
   size_t nRestarts = 1;
   while (nEvals <= T) {
      tmpVal = 0;
      
      do {
	 if (!args.randomSelection) {
	    if (nEvals - nEvalsAtRestart > args.restartInterval * n * k) {
	       ++nRestarts;
	       //let's restart
	       nEvalsAtRestart = nEvals;
	       {
		  vector< bool > empty( args.g.n, false );
		  pool.assign( maxSize + 1, empty );
		  vals.assign( maxSize + 1,  0 );
		  //basePool = pool;
		  sampsForPool = vals;
		  //inductiveBin = 0;
	       }
	    }

	    notConverged.clear();
	    for (size_t i = 0; i < pool.size() - 1; ++i) {
	       if (sampsForPool[ i ] <= lazySize || backPos[ i ] < i ) {
		  notConverged.push_back( i );
	       } 
	    }

      
	    if (notConverged.size() == 0) {
	       
	       //(*args.logg) << "Pareto converged." << endL;
	       //let's restart
	       ++nRestarts;
	       //cerr << '\n';
	       double prog = nEvals / (static_cast<double>(k) * n);
	       if (args.nThreads == 1) {
		  cerr << "\r                                                                       \r";
		  cerr << prog << '\t' << maxVal / ( (double) valGreedy); // << '\t' << inductiveBin
		  //<< '\t' << nRestarts << '\t' << maxVal;
	       }
	       
	       nEvalsAtRestart = nEvals;
	       {
		  vector< bool > empty( args.g.n, false );
		  pool.assign( maxSize + 1, empty );
		  vals.assign( maxSize + 1,  0 );
		  backPos.assign( maxSize + 1,  0 );
		  //basePool = pool;
		  sampsForPool = vals;
		  inductiveBin = 0;
		  vector< size_t > emptyID;
		  poolID.assign( maxSize + 1, emptyID );
	       }
	       pos = 0;
	       bcont = false;
	       continue;
	    }
	    
	    if (uni( genAlg ) <= p) {
	       if (inductiveBin < k) {
		  pos = inductiveBin;
	       } else {
		  uniform_int_distribution< size_t > distUnConv(0, notConverged.size() - 1);
		  //pos = notConverged[ 0 ];
		  pos = notConverged[ distUnConv( genAlg ) ];
	       }
	    } else {
	       uniform_int_distribution< size_t > distUnConv(0, notConverged.size() - 1);
	       //pos = notConverged[ 0 ];
	       pos = notConverged[ distUnConv( genAlg ) ];
	    }

	    if (vals[pos] == 0 && pos > 0) 
	       pos = 0;
	    

	    if (pos > maxSize)
	       pos = 0;
	    
	    break;
	 }
	 
	 bcont = false;
	 if (uni( genAlg ) < p) {
	    if (inductiveBin < k)
	       pos = inductiveBin;
	    else
	       pos = selectDist(genAlg);
	 } else {
	    pos = selectDist(genAlg);
	 }

	 // if (sampsForPool[ pos ] > lazySize ) {
	 //    if (!args.randomSelection) {
	  
	 // 	  bcont = true;
	 //    }
	 // }
	 
	 
	 if (pos > 0 && vals[pos] == 0)
	    bcont = true;
	 
      } while( bcont  ); //only select from nontrivial elements and bias towards "unconverged" elements
      
      if (pos == 0) {
	 toss = 1;
      } else {
	 if (pos == maxSize) {
	    toss = 0;
	 } else {
	    toss = coinToss( genAlg );
	 }
      }

      if (toss == 1) {
	 //we know pos < maxSize
	 size_t oldVal = vals[ pos + 1 ];
	 subArgs< graph_type > subargs ( &pool, &vals, pos, &g, &nEvals, sampIter, &distLazy, &poolID, &genAlg );
	 
	 tmpVal = lazyForward< graph_type >( func, subargs );

	 if (tmpVal > oldVal) {
	    if (pos + 1 != inductiveBin || inductiveBin > k) {
	       sampsForPool[ pos + 1 ] = 0;
	       backPos[ pos + 1 ] = 0;
	    } 
	 }

	 if ((tmpVal > maxVal) && (pos + 1 <= k) ) {
	    maxVal = tmpVal;
	    ss = pool[ pos + 1 ];
	    ssIds = poolID[ pos + 1 ];
	    //if (args.nThreads == 1) {
	    //cerr << "\r                                                                       \r";
	    double prog = nEvals / (static_cast<double>(k) * n);
	    if (args.nThreads == 1) {
	       cerr << "\r                                                                       \r";
	       cerr << prog << '\t' << maxVal / ( (double) valGreedy); // << '\t' << inductiveBin
	       //<< '\t' << nRestarts << '\t' << maxVal;
	    }
	       
	       //cerr << prog << '\t' << maxVal / ( (double) valGreedy) << '\t' << inductiveBin
	       //		    << '\t' << nRestarts << '\t' << maxVal;
	       //}
		       
	 }

	 sampsForPool[ pos ] += sampIter;

	 if (sampsForPool[ pos ] >= lazySize) {
	    if (pos == inductiveBin)
	       ++inductiveBin;
	 }
	 
      } else {
	 size_t oldVal = vals[ pos - 1 ];
	 subArgs< graph_type > subargs ( &pool, &vals, pos, &g, &nEvals, sampIter, &distLazyBack, &poolID, &genAlg );
	 if (args.randomSelection) {
	    tmpVal = lazyBackward< graph_type >( func, subargs );
	    if (tmpVal > oldVal) {
	       if (pos - 1 != inductiveBin || inductiveBin > k) {
		  sampsForPool[ pos - 1 ] = 0;
		  backPos[ pos - 1 ] = 0;
	       }
	    }
	 }
	 else {
	    size_t& backPoss = backPos[ pos ];
	    if (backPoss < pos) {
	       vector< size_t > ssIdTmp = poolID[ pos ];
	       
	       ssIdTmp.erase( ssIdTmp.begin() + backPoss );
	       
	       tmpVal = func( nEvals, g, ssIdTmp );
	       if (tmpVal > oldVal) {
		  vector< bool > ssTmp = pool[ pos ];
		  ssTmp[ poolID[ pos ][ backPoss ] ] = false;
		  pool[ pos - 1 ] = ssTmp;
		  random_shuffle( ssIdTmp.begin(), ssIdTmp.end() );
		  poolID[ pos - 1 ] = ssIdTmp;
		  vals[ pos - 1 ] = tmpVal;
		  sampsForPool[ pos - 1 ] = 0;
		  backPos[ pos - 1 ] = 0;
	       } else {
		  tmpVal = 0;
	       }
	       ++backPoss;
	    }
	 }
	 
	 

	 if ((tmpVal > maxVal) && (pos - 1 <= k) ) {
	    maxVal = tmpVal;
	    ss = pool[ pos - 1 ];
	    ssIds = poolID[ pos - 1 ];
	    //if (args.nThreads == 1) {
	       //cerr << "\r                                                                     \r";
	       double prog = nEvals / (static_cast<double>(k) * n);
	       if (args.nThreads == 1) {
		  cerr << "\r                                                                       \r";
		  cerr << prog << '\t' << maxVal / ( (double) valGreedy); // << '\t' << inductiveBin
		  //<< '\t' << nRestarts << '\t' << maxVal;
	       }
	       //cerr << prog << '\t' << maxVal / ( (double) valGreedy) << '\t' << inductiveBin
	       //		    << '\t' << nRestarts << '\t' << maxVal;
	       //}
		       
	 }
	 
      }

      if (boutput) {
	 //output every tenth of a greedy iteration
	 bool outputNow = false;
	 double prog = nEvals / (static_cast<double>(k) * n);
	 while (outTenth < prog) {
	    outTenth += args.outputInterval;
	    outputNow = true;
	 }
	 if (outputNow) {
	    mtx.lock();
	    simpleResults.init( outTenth - args.outputInterval );
	    simpleResults.add( outTenth - args.outputInterval, maxVal / ( (double) valGreedy) );
	    //ofile << args.k << '\t' << iter << '\t' << prog << '\t' << outTenth - args.outputInterval << '\t' 
	    //<< maxVal / ( (double) valGreedy)
	    //<< ' ' << args.alg << ' ' << args.ell << '\n';
	    mtx.unlock();
	 }
      }

      ++iter;
   }

   size_t totCovered = func( nEvals, args.g, ssIds );

   (*args.logg) << "RPS value: " << totCovered << endL;

   // print_vector_vector( poolID );
   // size_t throwAway = 0;
   // for (size_t i = 0; i < poolID.size(); ++i) {
   //    cout << func( throwAway, g, poolID[i] ) << ' ';
   // }
   // cout << endl;
   // cout << maxVal << ' ' << valGreedy << endl;
   // print_vector( ssIds );

   // cout << func( nEvals, args.g, ssIds ) << endl;
   
   return totCovered;
}

template <typename graph_type>
size_t rps_main_lm( tArgs< graph_type >& args,
		    size_t (*func)( size_t&, graph_type&, vector<size_t>& ss ) ) {
   random_device rdAlg;
   mt19937 genAlg( rdAlg() );
   (*args.logg) << SECT;
   (*args.logg) << "RANDOMIZED POOL SEARCH (with large mutations)" << endL;
   (*args.logg) << INFO;

   if (args.g.lmax != 1) {
      args.g.lmax = 1;

      if (args.K > 0)
	 args.k = args.K;
   }

   double p = args.p;
   graph_type& g = args.g;
   
   uniform_int_distribution<size_t> distLazy(0, args.g.n - 1);

   size_t lazySize = static_cast<double>( args.g.n ) / args.k * log( 1.0 / args.epsi );
   size_t sampIter;
   if (args.ell == 0)
      sampIter = 1;
   else
      sampIter = lazySize / args.ell + 1;

   (*args.logg) << "lazySize = " << lazySize << endL;
   (*args.logg) << "sampIter = " << sampIter << endL;
   
   bool boutput = false;
   if (args.outputFileName != "") {
      //ofile.open( args.outputFileName.c_str() );
      boutput = true;
   }

   size_t valGreedy;
   if (args.valGreedy > 0.0)
      valGreedy = args.valGreedy;
   else {
      valGreedy = greedy( args );
      args.valGreedy = valGreedy;
      (*args.logg) << "Greedy finished with val: " << valGreedy << endL;
   }

   vector< vector< bool > > pool;
   vector< vector< size_t > > poolID;
   vector< size_t > vals;
   vector< size_t > sampsForPool;
   vector<bool> ss;
   vector<size_t> ssIds;
   size_t k = args.k;
   size_t n = args.g.n;
   size_t maxSize = 2 * k;
   size_t inductiveBin = 0;
   uniform_int_distribution<size_t> distLazyBack(0, maxSize);
   {
      vector< bool > empty( args.g.n, false );
      pool.assign( maxSize + 1, empty );
      vals.assign( maxSize + 1,  0 );
      vector< size_t > emptyID;
      poolID.assign( maxSize + 1, emptyID );
      //basePool = pool;
      sampsForPool = vals;
   }

   size_t maxVal = 0;
   size_t nEvals = 0;
   size_t toss, tmpVal, pos;
   size_t T = args.T * n * k;
   uniform_int_distribution< size_t > selectDist( 0, maxSize );
   uniform_int_distribution< size_t > coinToss( 0, 1 );

   size_t iter = 0;
   double outTenth = 0.0;
   bool bcont;
   vector< size_t > notConverged;
   size_t nEvalsAtRestart = 0;
   size_t nRestarts = 1;
   while (nEvals <= T) {
      
      notConverged.clear();
      for (size_t i = 0; i < pool.size() - 1; ++i) {
	 if (sampsForPool[ i ] <= lazySize) {
	    notConverged.push_back( i );
	    break;
	 }
      }
      
      if (notConverged.size() == 0) {
	 pos = selectDist( genAlg ); 
      } else {
	 if (uni( genAlg ) <= p) {
	    if (inductiveBin < k) {
	       pos = inductiveBin;
	    } else {
	       pos = notConverged[ 0 ];
	    }
	 } else {
	    
	    pos = notConverged[ 0 ];
	 }
      }
      
      if (vals[pos] == 0 && pos > 0) 
	 pos = 0;
	    

      if (pos > maxSize)
	 pos = 0;
	    
      if (pos == 0) {
	 toss = 1;
      } else {
	 if (pos == maxSize) {
	    toss = 0;
	 } else {
	    toss = coinToss( genAlg );
	 }
      }

      if (notConverged.size() > 0) {
	 //do single hop mutation
	 if (toss == 1) {
	    //we know pos < maxSize
	    size_t oldVal = vals[ pos + 1 ];
	    subArgs< graph_type > subargs ( &pool, &vals, pos, &g, &nEvals, sampIter, &distLazy, &poolID, &genAlg );
	 
	    tmpVal = lazyForward< graph_type >( func, subargs );

	    if (tmpVal > oldVal) {
	       if (pos + 1 != inductiveBin || inductiveBin > k) {
		  sampsForPool[ pos + 1 ] = 0;
	       } 
	    }

	    if ((tmpVal > maxVal) && (pos + 1 <= k) ) {
	       maxVal = tmpVal;
	       ss = pool[ pos + 1 ];
	       ssIds = poolID[ pos + 1 ];
	       if (args.nThreads == 1) {
	       cerr << "\r                                                                       \r";
	       double prog = nEvals / (static_cast<double>(k) * n);
	       cerr << prog << '\t' << maxVal / ( (double) valGreedy);// << '\t' << inductiveBin
	       //<< '\t' << nRestarts << '\t' << maxVal;
	       }
		       
	    }

	    sampsForPool[ pos ] += sampIter;

	    if (sampsForPool[ pos ] >= lazySize) {
	       if (pos == inductiveBin)
		  ++inductiveBin;
	    }
	 
	 } else {
	    size_t oldVal = vals[ pos - 1 ];
	    subArgs< graph_type > subargs ( &pool, &vals, pos, &g, &nEvals, sampIter, &distLazyBack, &poolID, &genAlg );
	    tmpVal = lazyBackward< graph_type >( func, subargs );
	 
	    if (tmpVal > oldVal) {
	       if (pos - 1 != inductiveBin || inductiveBin > k) {
		  sampsForPool[ pos - 1 ] = 0;
	       } 
	    }

	    if ((tmpVal > maxVal) && (pos - 1 <= k) ) {
	       maxVal = tmpVal;
	       ss = pool[ pos - 1 ];
	       ssIds = poolID[ pos - 1 ];
	       //if (args.nThreads == 1) {
	       //cerr << "\r                                                                     \r";
	       double prog = nEvals / (static_cast<double>(k) * n);
	       if (args.nThreads == 1) {
		  cerr << "\r                                                                       \r";
		  cerr << prog << '\t' << maxVal / ( (double) valGreedy);// << '\t' << inductiveBin
		  //<< '\t' << nRestarts << '\t' << maxVal;
	       }
	       //cerr << prog << '\t' << maxVal / ( (double) valGreedy) << '\t' << inductiveBin
	       //<< '\t' << nRestarts << '\t' << maxVal;
	       //}
		       
	    }
	 }
      } else {
	 //do multihop
	 size_t newpos,newval;
	 ss = pool[ pos ];
	 ssIds = poolID[ pos ];
	 binomial_distribution< size_t > binDist( args.g.n, 1.0 / args.g.n );
	 mutate( ss, ssIds, n, binDist, distLazy, genAlg, true ); //true for large mutations
	 if (ssIds.size() != pos) {
	    if (replace( ss, ssIds, pool, poolID, vals, newpos, newval,maxSize,nEvals,func,g )) {
	       sampsForPool[ newpos ] = 0;
	    }
	    if (newval > maxVal && newpos <= k) {
	       maxVal = newval;
	       
	       //cerr << "\r                                                                     \r";
	       double prog = nEvals / (static_cast<double>(k) * n);
	       if (args.nThreads == 1) {
		  cerr << "\r                                                                       \r";
		  cerr << prog << '\t' << maxVal / ( (double) valGreedy);// << '\t' << inductiveBin
		  //<< '\t' << nRestarts << '\t' << maxVal;
	       }
	       //cerr << prog << '\t' << maxVal / ( (double) valGreedy) << '\t' << inductiveBin
	       //		    << '\t' << nRestarts << '\t' << maxVal;

	    }
	 }
      }

      if (boutput) {
	 //output every tenth of a greedy iteration
	 bool outputNow = false;
	 double prog = nEvals / (static_cast<double>(k) * n);
	 while (outTenth < prog) {
	    outTenth += args.outputInterval;
	    outputNow = true;
	 }
	 if (outputNow) {
	    mtx.lock();
	    simpleResults.init( outTenth - args.outputInterval );
	    simpleResults.add( outTenth - args.outputInterval, maxVal / ( (double) valGreedy) );
	    //ofile << args.k << '\t' << iter << '\t' << prog << '\t' << outTenth - args.outputInterval << '\t' 
	    //<< maxVal / ( (double) valGreedy)
	    //<< ' ' << args.alg << ' ' << args.ell << '\n';
	    mtx.unlock();
	 }
      }

      // if ( (double) maxVal / valGreedy > 1.5 ) {
      // 	 print_vector_vector( poolID );
      // 	 for (size_t i = 0; i < poolID.size(); ++i) {
      // 	    cout << func( iter, g, poolID[i] ) << ' ';
      // 	 }
      // 	 cout << endl;
      // 	 cout << maxVal << ' ' << valGreedy << endl;
      // }
      
      ++iter;
   }

   //size_t totCovered = func( nEvals, args.g, ssIds );

   //(*args.logg) << "RPS value: " << totCovered << endL;

   // //cerr << endl;
   // for (size_t i = 0; i < maxSize; ++i) {
   //    //cerr << vals[i] << ' ';
   // }
   // //cerr << endl;

   // for (size_t i = 0; i < maxSize; ++i) {
   //    //cerr << i << ": ";
   //    for (size_t j = 0; j < args.g.n; ++j) {
   // 	 if (basePool[i][j])
   // 	    //cerr << j << ' ';
   //    }
   //    //cerr << endl;
   // }
   
   
   // if (isStable( args.g, basePool, vals )) {
   //    //cerr << "basepool is stable\n";
   // } else {
   //    //cerr << "basepool is unstable\n";
   // }
   
   return 0;
}

template <typename graph_type>
size_t rps_main_fb( tArgs< graph_type >& args,
		    size_t (*func)( size_t&, graph_type&, vector<size_t>& ss ) ) {
   random_device rdAlg;
   mt19937 genAlg( rdAlg() );
   (*args.logg) << SECT;
   (*args.logg) << "RANDOMIZED POOL SEARCH (FB)" << endL;
   (*args.logg) << INFO;

   if (args.g.lmax != 1) {
      args.g.lmax = 1;

      if (args.K > 0)
	 args.k = args.K;
   }

   double p = args.p;
   graph_type& g = args.g;
   
   uniform_int_distribution<size_t> distLazy(0, args.g.n - 1);

   size_t lazySize = static_cast<double>( args.g.n ) / args.k * log( 1.0 / args.epsi );
   size_t sampIter;
   if (args.ell == 0)
      sampIter = 1;
   else
      sampIter = lazySize / args.ell + 1;

   (*args.logg) << "lazySize = " << lazySize << endL;
   (*args.logg) << "sampIter = " << sampIter << endL;
   
   bool boutput = false;
   if (args.outputFileName != "") {
      //ofile.open( args.outputFileName.c_str() );
      boutput = true;
   }

   size_t valGreedy;
   if (args.valGreedy > 0.0)
      valGreedy = args.valGreedy;
   else {
      valGreedy = greedy( args );
      args.valGreedy = valGreedy;
      (*args.logg) << "Greedy finished with val: " << valGreedy << endL;
   }

   vector< vector< bool > > pool;
   vector< vector< size_t > > poolID;
   vector< size_t > vals;
   vector< size_t > sampsForPool;
   vector<bool> ss;
   vector<size_t> ssIds;
   size_t k = args.k;
   size_t n = args.g.n;
   size_t maxSize = 2 * k;
   size_t inductiveBin = 0;
   uniform_int_distribution<size_t> distLazyBack(0, maxSize);
   {
      vector< bool > empty( args.g.n, false );
      pool.assign( maxSize + 1, empty );
      vals.assign( maxSize + 1,  0 );
      vector< size_t > emptyID;
      poolID.assign( maxSize + 1, emptyID );
      //basePool = pool;
      sampsForPool = vals;
   }

   size_t maxVal = 0;
   size_t nEvals = 0;
   size_t toss, tmpVal, pos;
   size_t T = args.T * n * k;
   uniform_int_distribution< size_t > selectDist( 0, maxSize );
   uniform_int_distribution< size_t > coinToss( 0, 1 );

   size_t iter = 0;
   double outTenth = 0.0;
   bool bcont;
   vector< size_t > notConverged;
   size_t nEvalsAtRestart = 0;
   size_t nRestarts = 1;

   size_t skip = 1;
   while (nEvals <= T) {
      
      do {
	 if (!args.randomSelection) {
	    if (nEvals - nEvalsAtRestart > args.restartInterval * n * k) {
	       ++nRestarts;
	       //let's restart
	       nEvalsAtRestart = nEvals;
	       {
		  vector< bool > empty( args.g.n, false );
		  pool.assign( maxSize + 1, empty );
		  vals.assign( maxSize + 1,  0 );
		  //basePool = pool;
		  sampsForPool = vals;
		  inductiveBin = 0;
	       }
	    }

	    notConverged.clear();
	    for (size_t i = 0; i < pool.size() - 1; ++i) {
	       if (sampsForPool[ i ] <= lazySize) {
		  notConverged.push_back( i );
		  break;
	       }
	    }

      
	    if (notConverged.size() == 0) {
	       //(*args.logg) << "Pareto converged." << endL;
	       //let's restart
	       ++nRestarts;
	       //cerr << '\n';
	       double prog = nEvals / (static_cast<double>(k) * n);
	       	       
	       nEvalsAtRestart = nEvals;
	       {
		  vector< bool > empty( args.g.n, false );
		  pool.assign( maxSize + 1, empty );
		  vals.assign( maxSize + 1,  0 );
		  //basePool = pool;
		  sampsForPool = vals;
		  inductiveBin = 0;
		  vector< size_t > emptyID;
		  poolID.assign( maxSize + 1, emptyID );
	       }
	       pos = 0;
	       bcont = false;
	       continue;
	    }
	    
	    if (uni( genAlg ) <= p) {
	       if (inductiveBin < k) {
		  pos = inductiveBin;
	       } else {
		  pos = notConverged[ 0 ];
	       }
	    } else {
	       
	       pos = notConverged[ 0 ];
	    }

	    if (vals[pos] == 0 && pos > 0) 
	       pos = 0;
	    

	    if (pos > maxSize)
	       pos = 0;
	    
	    break;
	 }
	 
	 bcont = false;
	 if (uni( genAlg ) < p) {
	    if (inductiveBin < k)
	       pos = inductiveBin;
	    else {
	       pos = k;
	       inductiveBin = k;
	    }
	 } else {
	    pos = selectDist(genAlg);
	 }

	 // if (sampsForPool[ pos ] > lazySize ) {
	 //    if (!args.randomSelection) {
	  
	 // 	  bcont = true;
	 //    }
	 // }
	 
	 
	 if (pos > 0 && vals[pos] == 0)
	    bcont = true;
	 
      } while( bcont  ); //only select from nontrivial elements and bias towards "unconverged" elements
      
      if (pos == 0) {
	 toss = 1;
      } else {
	 if (pos == maxSize) {
	    toss = 0;
	 } else {
	    toss = coinToss( genAlg );
	    //toss = 1;
	 }
      }

      if (toss == 1) {
	 //we know pos < maxSize
	 size_t oldVal = vals[ pos + 1 ];
	 size_t oldVal2 = vals[ pos ];
	 subArgs< graph_type > subargs ( &pool, &vals, pos, &g, &nEvals, sampIter, &distLazy, &poolID, &genAlg );

	 //if (pos == inductiveBin) {
	    lazyForwardAndBack< graph_type >( func, subargs );
	    //} else {
	    //lazyForward< graph_type >( func, subargs );
	    //}

	 size_t tmpValForward = vals[ pos + 1 ];
	 size_t tmpValBackward = vals[ pos ];
	 if (tmpValForward > oldVal) {
	    if (pos + 1 != inductiveBin || inductiveBin > k) {
	       sampsForPool[ pos + 1 ] = 0;
	    } 
	 }

	 if (tmpValBackward > oldVal2) {
	    if (pos != inductiveBin || inductiveBin > k) {
	       sampsForPool[ pos ] = 0;
	    } 
	 }
	 
	 if ((tmpValForward > maxVal) && (pos + 1 <= k) ) {
	    maxVal = tmpValForward;
	    ss = pool[ pos + 1 ];
	    ssIds = poolID[ pos + 1 ];
	    //if (args.nThreads == 1) {
	    //cerr << "\r                                                                       \r";
	    double prog = nEvals / (static_cast<double>(k) * n);
	    if (args.nThreads == 1) {
	       cerr << "\r                                                                       \r";
	       cerr << prog << '\t' << maxVal / ( (double) valGreedy);// << '\t' << inductiveBin
	       //<< '\t' << nRestarts << '\t' << maxVal;
	    }
	    //cerr << prog << '\t' << maxVal / ( (double) valGreedy) << '\t' << inductiveBin
	    //		    << '\t' << nRestarts << '\t' << maxVal;
	    //}
		       
	 }

	 if ((tmpValBackward > maxVal) && (pos <= k) ) {
	    maxVal = tmpValBackward;
	    ss = pool[ pos ];
	    ssIds = poolID[ pos ];
	    //if (args.nThreads == 1) {
	       //cerr << "\r                                                                       \r";
	       double prog = nEvals / (static_cast<double>(k) * n);
	       if (args.nThreads == 1) {
		  cerr << "\r                                                                       \r";
		  cerr << prog << '\t' << maxVal / ( (double) valGreedy);// << '\t' << inductiveBin
		  //<< '\t' << nRestarts << '\t' << maxVal;
	       }
	       //cerr << prog << '\t' << maxVal / ( (double) valGreedy) << '\t' << inductiveBin
	       //		    << '\t' << nRestarts << '\t' << maxVal;
	       //}
		       
	 }

	 sampsForPool[ pos ] += sampIter;

	 if (sampsForPool[ pos ] >= lazySize) {
	    if (pos == inductiveBin)
	       ++inductiveBin;
	 }
	 
      } else {
	 size_t oldVal = vals[ pos ];
	 size_t oldVal2 = vals[ pos - 1 ];
	 
	 subArgs< graph_type > subargs ( &pool, &vals, pos, &g, &nEvals, sampIter, &distLazyBack, &poolID, &genAlg );

	 //if (pos == inductiveBin) {
	    lazyBackAndForward< graph_type >( func, subargs );
	    //} else {
	    //lazyBackward< graph_type >( func, subargs );
	    //}
	 
	 size_t tmpValForward = vals[ pos  ];
	 size_t tmpValBackward = vals[ pos - 1 ];
	 if (tmpValForward > oldVal) {
	    if (pos + 1 != inductiveBin || inductiveBin > k) {
	       sampsForPool[ pos  ] = 0;
	    } 
	 }
	 if (tmpValBackward > oldVal2) {
	    if (pos != inductiveBin || inductiveBin > k) {
	       sampsForPool[ pos - 1] = 0;
	    } 
	 }
	 tmpVal = tmpValForward;
	 if (tmpValBackward > tmpVal)
	    tmpVal = tmpValBackward;
	 
	 
	 if ((tmpValForward > maxVal) && (pos <= k) ) {
	    maxVal = tmpValForward;
	    ss = pool[ pos ];
	    ssIds = poolID[ pos ];
	    //if (args.nThreads == 1) {
	       //cerr << "\r                                                                     \r";
	       double prog = nEvals / (static_cast<double>(k) * n);
	       if (args.nThreads == 1) {
		  cerr << "\r                                                                       \r";
		  cerr << prog << '\t' << maxVal / ( (double) valGreedy);// << '\t' << inductiveBin
		  //<< '\t' << nRestarts << '\t' << maxVal;
	       }
	       //cerr << prog << '\t' << maxVal / ( (double) valGreedy) << '\t' << inductiveBin
	       //<< '\t' << nRestarts << '\t' << maxVal;
	       //}
		       
	 }

	 if ((tmpValBackward > maxVal) && (pos - 1 <= k) ) {
	    maxVal = tmpValBackward;
	    ss = pool[ pos - 1 ];
	    ssIds = poolID[ pos - 1 ];
	    //if (args.nThreads == 1) {
	       //cerr << "\r                                                                     \r";
	       double prog = nEvals / (static_cast<double>(k) * n);
	       if (args.nThreads == 1) {
		  cerr << "\r                                                                       \r";
		  cerr << prog << '\t' << maxVal / ( (double) valGreedy);// << '\t' << inductiveBin
		  //<< '\t' << nRestarts << '\t' << maxVal;
	       }
	       //cerr << prog << '\t' << maxVal / ( (double) valGreedy) << '\t' << inductiveBin
	       //<< '\t' << nRestarts << '\t' << maxVal;
	       //}
		       
	 }
      }

      if (boutput) {
	 //output every tenth of a greedy iteration
	 bool outputNow = false;
	 double prog = nEvals / (static_cast<double>(k) * n);
	 while (outTenth < prog) {
	    outTenth += args.outputInterval;
	    outputNow = true;
	 }
	 if (outputNow) {
	    mtx.lock();
	    simpleResults.init( outTenth - args.outputInterval );
	    simpleResults.add( outTenth - args.outputInterval, maxVal / ( (double) valGreedy) );
	    //ofile << args.k << '\t' << iter << '\t' << prog << '\t' << outTenth - args.outputInterval << '\t' 
	    //<< maxVal / ( (double) valGreedy)
	    //<< ' ' << args.alg << ' ' << args.ell << '\n';
	    mtx.unlock();
	 }
      }

     
      // if (iter % skip == 0) {
      // 	 //cerr << "\npool: ";
      // 	 print_vector_vector( poolID );

      // 	 //cerr << "vals: ";
      // 	 print_vector( vals );


      // 	 //cerr << "relevant vals: " << vals[ k - 1 ]
      // 	      << ' ' << vals[k] << ' ' << vals[ k + 1 ] << endl;

      // 	 //cerr << "maxval: " << maxVal << ' ' << valGreedy << endl;
	 
      // 	 cin >> skip;
      // }
      
      ++iter;
   }

   size_t totCovered = func( nEvals, args.g, ssIds );

   (*args.logg) << "RPS value: " << totCovered << endL;

   // //cerr << endl;
   // for (size_t i = 0; i < maxSize; ++i) {
   //    //cerr << vals[i] << ' ';
   // }
   // //cerr << endl;

   // for (size_t i = 0; i < maxSize; ++i) {
   //    //cerr << i << ": ";
   //    for (size_t j = 0; j < args.g.n; ++j) {
   // 	 if (basePool[i][j])
   // 	    //cerr << j << ' ';
   //    }
   //    //cerr << endl;
   // }
   
   
   // if (isStable( args.g, basePool, vals )) {
   //    //cerr << "basepool is stable\n";
   // } else {
   //    //cerr << "basepool is unstable\n";
   // }

   // print_vector_vector( poolID );
   // size_t throwAway = 0;
   // for (size_t i = 0; i < poolID.size(); ++i) {
   //    cout << func( throwAway, g, poolID[i] ) << ' ';
   // }
   // cout << endl;
   // cout << maxVal << ' ' << valGreedy << endl;
   // print_vector( ssIds );

   // cout << func( nEvals, args.g, ssIds ) << endl;
   return totCovered;
}

template <typename graph_type>
size_t lazy_greedy( tArgs< graph_type >& args,
		    size_t (*func)( size_t&, graph_type&, vector<size_t>& ss ) ) {
   random_device rdAlg;
   mt19937 genAlg( rdAlg() );
   (*args.logg) << SECT;
   (*args.logg) << "LAZIER GREEDY" << endL;
   (*args.logg) << INFO;

   graph_type& g = args.g;
   
   uniform_int_distribution<size_t> distLazy(0, args.g.n - 1);

   size_t lazySize = static_cast<double>( args.g.n ) / args.k * log( 1.0 / args.epsi );
   size_t sampIter = 1;

   bool boutput = false;
   if (args.outputFileName != "") {
      //ofile.open( args.outputFileName.c_str() );
      boutput = true;
   }
   
   size_t valGreedy;
   if (args.valGreedy > 0.0)
      valGreedy = args.valGreedy;
   else {
      valGreedy = greedy( args );
      args.valGreedy = valGreedy;
      (*args.logg) << "Greedy finished with val: " << valGreedy << endL;
   }

   vector< vector< bool > > pool;
   vector< vector< size_t > > poolID;
   vector< size_t > vals;
   vector< size_t > sampsForPool;
   vector<bool> ss;
   vector<size_t> ssIds;
   size_t k = args.k;
   size_t n = args.g.n;
   size_t maxSize = 2 * k;
   size_t inductiveBin = 0;
   uniform_int_distribution<size_t> distLazyBack(0, maxSize);

   {
      vector< bool > empty( args.g.n, false );
      pool.assign( maxSize + 1, empty );
      vals.assign( maxSize + 1,  0 );
      vector< size_t > emptyID;
      poolID.assign( maxSize + 1, emptyID );
      //basePool = pool;
      sampsForPool = vals;
   }

   size_t maxVal = 0;
   size_t nEvals = 0;
   size_t toss, tmpVal, pos;
   size_t T = args.T * n * k;

   double outTenth = 0.0;
   size_t nRestarts = 1;

   vector< size_t > setK;
   vector< size_t > setKminus;
   size_t idxLSi = 0;
   size_t idxLSj = 0;
   size_t lsTimeSinceImprovement = 0;
   
   while (nEvals <= T) {
      if (inductiveBin > k) {
	 if (args.lazySearchHeuristic == false) {
	    // RESTARTING DISABLED
	    // Output and Terminate
	    //output every tenth of a greedy iteration
	    bool outputNow = true;
	    double prog = nEvals / (static_cast<double>(k) * n);
	    unsigned uProg = (prog * 1000);
	    prog = static_cast<double>( uProg ) / 1000;
	    if (outputNow) {
	       mtx.lock();
	       simpleResults.init( prog );
	       simpleResults.add( prog, maxVal / ( (double) valGreedy) );
	       mtx.unlock();
	    }
	    
	    break;
	    
	 } else {
	    if (false) {
	       if (lsTimeSinceImprovement > k*n + 1) {
		  ++nRestarts;
		  //restart
		  {
		     vector< bool > empty( args.g.n, false );
		     pool.assign( maxSize + 1, empty );
		     vals.assign( maxSize + 1,  0 );
		     //basePool = pool;
		     sampsForPool = vals;
		     inductiveBin = 0;
		     vector< size_t > emptyId;
		     poolID.assign( maxSize + 1, emptyId );
		  }
		  lsTimeSinceImprovement=0;
		  setK.clear();
	       }
	    }
	    //Run local search now that lazyGreedy has finished.
	    ++lsTimeSinceImprovement;
	    
	    if (setK.empty()) {
	       setK = poolID[ k ];
	       
	       idxLSi = 0;
	    }
	       
	    if (idxLSj >= n) {
	       idxLSj = 0;
	       ++idxLSi;
	    }
	    
	    if (idxLSi == k) {
	       idxLSi = 0;
	       idxLSj = 0;
	    }

	       
	    setKminus = setK;
	    if (!vcontains(setKminus, idxLSj )) {
	       setKminus.erase( setKminus.begin() + idxLSi );
	       setKminus.push_back( idxLSj );

	       tmpVal = func( nEvals, g, setKminus );
	       if (tmpVal > maxVal) {
		  lsTimeSinceImprovement=0;
		  maxVal = tmpVal;
		  setK = setKminus;
		  if (args.nThreads == 1) {
		     //cerr << "\r                                                                       \r";
		     double prog = nEvals / (static_cast<double>(k) * n);
		     if (args.nThreads == 1) {
			cerr << "\r                                                                       \r";
			cerr << prog << '\t' << maxVal / ( (double) valGreedy);// << '\t' << inductiveBin
			//<< '\t' << nRestarts << '\t' << maxVal;
		     }
									 
			
		     //cerr << prog << '\t' << maxVal / ( (double) valGreedy) << '\t' << inductiveBin
		     //		       << '\t' << nRestarts << '\t' << maxVal;
		  }
	       }
	    }
	    ++idxLSj;
	 }
	    

	 //output
	 if (boutput && (args.lazySearchHeuristic == true)) {
	    //output every tenth of a greedy iteration
	    bool outputNow = false;
	    double prog = nEvals / (static_cast<double>(k) * n);
	    while (outTenth < prog) {
	       outTenth += args.outputInterval;
	       outputNow = true;
	    }
	    if (outputNow) {
	       mtx.lock();
	       simpleResults.init( outTenth - args.outputInterval );
	       simpleResults.add( outTenth - args.outputInterval, maxVal / ( (double) valGreedy) );
	       //ofile << args.k << '\t' << iter << '\t' << prog << '\t' << outTenth - args.outputInterval << '\t' 
	       //<< maxVal / ( (double) valGreedy)
	       //<< ' ' << args.alg << ' ' << args.ell << '\n';
	       mtx.unlock();
	    }
	 }
      }
        else {
	 
      pos = inductiveBin;
      toss = 1;

      if (toss == 1) {
	 //we know pos < maxSize
	 size_t oldVal = vals[ pos + 1 ];
	 subArgs< graph_type > subargs ( &pool, &vals, pos, &g, &nEvals, sampIter, &distLazy, &poolID, &genAlg );
	 
	 tmpVal = lazyForward< graph_type >( func, subargs );

	 if (tmpVal > oldVal) {
	    if (pos + 1 != inductiveBin || inductiveBin > k) {
	       sampsForPool[ pos + 1 ] = 0;
	    } 
	 }

	 if ((tmpVal > maxVal) && (pos + 1 <= k) ) {
	    maxVal = tmpVal;
	    ss = pool[ pos + 1 ];
	    ssIds = poolID[ pos + 1 ];
	    if (args.nThreads == 1) {
	       //cerr << "\r                                                                       \r";
	       double prog = nEvals / (static_cast<double>(k) * n);
	       if (args.nThreads == 1) {
		  cerr << "\r                                                                       \r";
		  cerr << prog << '\t' << maxVal / ( (double) valGreedy);// << '\t' << inductiveBin
		  //<< '\t' << nRestarts << '\t' << maxVal;
	       }
	       //cerr << prog << '\t' << maxVal / ( (double) valGreedy) << '\t' << inductiveBin
	       //		       << '\t' << nRestarts << '\t' << maxVal;
	    }
		       
	 }

	 sampsForPool[ pos ] += sampIter;

	 if (sampsForPool[ pos ] >= lazySize) {
	    if (pos == inductiveBin)
	       ++inductiveBin;
	 }
	 
      }

      if (boutput && (args.lazySearchHeuristic == true)) {
	 //output every tenth of a greedy iteration
	 bool outputNow = false;
	 double prog = nEvals / (static_cast<double>(k) * n);
	 while (outTenth < prog) {
	    outTenth += args.outputInterval;
	    outputNow = true;
	 }
	 if (outputNow) {
	    mtx.lock();
	    simpleResults.init( outTenth - args.outputInterval );
	    simpleResults.add( outTenth - args.outputInterval, maxVal / ( (double) valGreedy) );
	    //ofile << args.k << '\t' << iter << '\t' << prog << '\t' << outTenth - args.outputInterval << '\t' 
	    //<< maxVal / ( (double) valGreedy)
	    //<< ' ' << args.alg << ' ' << args.ell << '\n';
	    mtx.unlock();
	 }
      }

	}
   }

   return maxVal;
}


template <typename graph_type>
size_t run_qian( tArgs< graph_type >& args,
		 size_t (*func)( size_t&, graph_type&, vector<size_t>& ss ) ) {
   random_device rdAlg;
   mt19937 genAlg( rdAlg() );
   (*args.logg) << SECT;
   (*args.logg) << "PARETO SEARCH (POSS)" << endL;
   (*args.logg) << INFO;

   graph_type& g = args.g;

   bool boutput = false;
   if (args.outputFileName != "") {
      //ofile.open( args.outputFileName.c_str() );
      boutput = true;
   }

   size_t valGreedy;
   if (args.valGreedy > 0.0)
      valGreedy = args.valGreedy;
   else {
      valGreedy = greedy( args );
      args.valGreedy = valGreedy;
      (*args.logg) << "Greedy finished with val: " << valGreedy << endL;
   }

   vector< vector< bool > > pool;
   vector< vector< size_t > > poolId;
   vector< size_t > vals;
   size_t maxVal = 0;
   size_t pos;
   size_t maxSize = 2 * args.k;
   uniform_int_distribution< size_t > dist( 0, maxSize );

   {

      vector< bool > empty( args.g.n, false );
      pool.assign( maxSize + 1, empty );
      vals.assign( maxSize + 1, 0 );
      vector< size_t > emptyId;
      poolId.assign( maxSize + 1, emptyId );
   }

   size_t T = args.g.n * args.k * args.T;
   size_t nEvals = 0;
   size_t tmpPos, tmpVal;

   double outTenth = 0.0;
   size_t iter = 0;
   binomial_distribution< size_t > binDist( args.g.n, 1.0 / args.g.n );
   uniform_int_distribution< size_t > allDist( 0, args.g.n - 1 );

   while (nEvals <= T) {
   
      do {
	 pos = dist( genAlg );

      } while (pos >= pool.size());

      vector< bool > ss = pool[ pos ];
      vector< size_t > ssId = poolId[ pos ];

      mutate( ss, ssId,
	      args.g.n,
	      binDist,
	      allDist,
	      genAlg );

      if (ss.size() != pos) {
	 replace( ss,
		  ssId,
		  pool,
		  poolId,
		  vals,
		  tmpPos,
		  tmpVal,
		  maxSize,
		  nEvals,
		  func,
		  args.g );

      }
      
      if ( ( tmpVal > maxVal ) && (tmpPos <= args.k) ) {
	 maxVal = tmpVal;
	 cerr << "\r                                                 \r";
	 double prog = nEvals / (static_cast<double>(args.k) * args.g.n);
	 cerr << prog << '\t' << maxVal / ( (double) valGreedy);//
	 //<< ' ' << pool.size();

	 
      }

      if (boutput) {
	 bool outputNow = false;
	 double prog = nEvals / (static_cast<double>(args.k) * args.g.n);
	 while (outTenth < prog) {
	    outTenth += args.outputInterval;
	    outputNow = true;
	 }
	 if (outputNow) {
	    mtx.lock();
	    simpleResults.init( outTenth - args.outputInterval );
	    simpleResults.add( outTenth - args.outputInterval, maxVal / ( (double) valGreedy) );
	    //ofile << args.k << '\t' << iter << '\t' << prog << '\t' << outTenth - 0.1 << '\t' 
	    //	  << maxVal / ( (double) valGreedy)
	    //	  << ' ' << args.alg << ' ' << args.ell << '\n';
	    mtx.unlock();
	 }
      }

      ++iter;
   }

   cerr << '\n';
   
   (*args.logg) << "Pareto: " << maxVal << endL;

   return maxVal;
}

template <typename graph_type>
size_t local_search( tArgs< graph_type >& args,
		     size_t (*func)( size_t&,
				     graph_type&,
				     vector<size_t>& ss ) ) {
   random_device rdAlg;
   mt19937 genAlg( rdAlg() );
   (*args.logg) << SECT;
   (*args.logg) << "LOCAL SEARCH" << endL;
   (*args.logg) << INFO;

   graph_type& g = args.g;
   
   bool boutput = false;
   if (args.outputFileName != "") {
      //ofile.open( args.outputFileName.c_str() );
      boutput = true;
   }
   
   size_t valGreedy;
   if (args.valGreedy > 0.0)
      valGreedy = args.valGreedy;
   else {
      valGreedy = greedy( args );
      args.valGreedy = valGreedy;
      (*args.logg) << "Greedy finished with val: " << valGreedy << endL;
   }

   size_t k = args.k;
   size_t n = args.g.n;

   size_t maxVal = 0;
   size_t nEvals = 0;
   size_t tmpVal;
   size_t T = args.T * n * k;

   double outTenth = 0.0;

   uniform_int_distribution<size_t> distJ(0, n - 1);
   uniform_int_distribution<size_t> distI(0, k - 1);
   
   vector< size_t > setK;
   vector< size_t > setKminus;
   size_t idxLSi = distI( genAlg );
   size_t idxLSj = distJ( genAlg );
   
   while (nEvals <= T) {
      //Run local search
      if (setK.empty()) {
	 while (setK.size() < k) {
	    uniform_int_distribution<size_t> distPick(0, args.g.n - 1);
	    size_t v = distPick( genAlg );
	    if (!vcontains( setK, v ))
	       setK.push_back(v);
	 }
       
	 idxLSi = 0;
      }

      // if (idxLSj >= n) {
      // 	 idxLSj = n - 1;
      // 	 ++idxLSi;
      // }
	    
      // if (idxLSi == k) {
      // 	 idxLSi = 0;
      // 	 idxLSj = n - 1;
      // }

      setKminus = setK;
      if (!vcontains(setKminus, idxLSj )) {
	 setKminus.erase( setKminus.begin() + idxLSi );
	 setKminus.push_back( idxLSj );

	 tmpVal = func( nEvals, g, setKminus );
	 if (tmpVal > maxVal) {
	    maxVal = tmpVal;
	    setK = setKminus;

	    //output progress
	    
	    double prog = nEvals / (static_cast<double>(k) * n);
	    if (args.nThreads == 1) {
	       cerr << "\r                                                                       \r";
	       cerr << prog << '\t' << maxVal / ( (double) valGreedy);// << '\t' << maxVal;

	    }
	    //cerr << prog << '\t' << maxVal / ( (double) valGreedy) << '\t' << maxVal;
	 }
      }
	    
      //--idxLSj;
      idxLSi = distI( genAlg );
      idxLSj = distJ( genAlg );
      
      //output
      if (boutput) {
	 //output every tenth of a greedy iteration
	 bool outputNow = false;
	 double prog = nEvals / (static_cast<double>(k) * n);
	 while (outTenth < prog) {
	    outTenth += args.outputInterval;
	    outputNow = true;
	 }
	 if (outputNow) {
	    mtx.lock();
	    simpleResults.init( outTenth - args.outputInterval );
	    simpleResults.add( outTenth - args.outputInterval, maxVal / ( (double) valGreedy) );
	    mtx.unlock();
	 }
      }
   }

   return maxVal;
}

void print_help_generic() {
   cout << "Options: " << endl;
  cout << "-g <graph filename in binary format>" << endl
       << "-k <total budget>" << endl
       << "-G (run Stochastic-Greedy)" << endl
       << "-M (run BLPO)" << endl
       << "-L (run BLPO+)" << endl
       << "-Q (run POSS)" << endl
       << "-x <max number of threads (default: 1)> " << endl
       << "-o <output filename>" << endl;
}

template <typename graph_type>
void parse_args( int argc,
		 char ** argv,
		 tArgs< graph_type >& arg ) {
   int c;
   extern char *optarg;

   if (argc == 1) {
      print_help_generic();
      exit( 2 );
   }

   string sarg;
   while ((c = getopt( argc, argv, ":g:DK:OT:k:p:vx:PHQS:EBMN:GLl:q:to:d:ri:e:b:hA") ) != -1) {
      switch(c) {
      case 'b':
	 arg.logFileName.assign( optarg );
	 break;
      case 'o':
	 arg.outputFileName.assign( optarg );
	 break;
      case 'p':
	 sarg.assign( optarg );
	 arg.p = stod( sarg );
	 break;
      case 'q':
	 sarg.assign( optarg );
	 arg.ew = stod( sarg );
	 break;
      case 'h':
	 arg.lazySearchHeuristic = true;
	 break;
      case 'e':
	 sarg.assign( optarg );
	 arg.epsi = stod( sarg );
	 break;
      case 'x':
	 sarg.assign( optarg );
	 arg.nThreads = stoi( sarg );
	 break;
      case 'l':
	 sarg.assign( optarg );
	 arg.ell = stoi( sarg );
	 break;
      case 'k':
	 sarg.assign( optarg );
	 arg.k = stoi( sarg );
	 break;
      case 'i':
	 sarg.assign( optarg );
	 arg.restartInterval = stod( sarg );
	 break;
      case 'r':
	 arg.randomSelection = true;
	 break;
      case 'K':
	 sarg.assign( optarg );
	 arg.K = stoi( sarg );
	 break;
      case 'g':
	 //graph specification
	 arg.graphFileName.assign( optarg );
	 break;
      case 'G':
	 arg.alg = Algs::GREEDY;
	 break;
      case 'A':
	 arg.alg = Algs::LOCAL;
	 break;
      case 'B':
	 arg.alg = Algs::LAPSFB;
	 break;
      case 'M':
	 arg.alg = Algs::LAPSLM;
	 break;
      case 'Q':
	 arg.alg = Algs::QIAN;
	 break;
      case 'P':
	 arg.alg = Algs::GPARETO;
	 break;
      case 'L':
	 arg.alg = Algs::LAPS;
	 break;
      case 'O':
	 arg.alg = Algs::OPT;
	 break;
      case 'N':
	 sarg.assign( optarg );
	 arg.N = stoi( sarg );
	 break;
      case 'S':
	 sarg.assign( optarg );
	 arg.S = stoi( sarg );
	 break;
      case 'T':
	 sarg.assign( optarg );
	 arg.T = stod( sarg );
	 break;
      case '?':
	 print_help_generic();
	 exit( 0 );
	 break;
      }

   }

   if (arg.logFileName != "") {
     arg.ofLog = new ofstream( arg.logFileName.c_str() );
     arg.logg = new Logger( INFO, *arg.ofLog, true );
   } else {
      arg.logg = new Logger();
   }

   arg.g.logg.enabled = false;
}


#endif
