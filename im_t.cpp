
#include "generic_alg.cpp"
#include "thresh-graph.cpp"
#include "results_handler.cpp"

#include <iostream>
#include <string>
#include <unistd.h>
#include <chrono>
#include <thread>

using namespace sampgraph;
using namespace std;

//mt19937 gen;
vector< double > vThresh;


size_t updateBlack( myGraph& g, vector<bool>& gray, vector<bool>& black) {
   size_t newBlack = 0;
   for (node_id v = 0; v < g.n; ++v) {
      if (gray[v] && (!black[ v ])) {
	 tinyNode& vv = g.adjList[v];
	 size_t blackNeis = 0;
	 for (size_t i = 0; i < vv.INneis.size(); ++i) {
	    node_id nei = vv.INneis[i].target;

	    if (black[ nei ]) {
	       ++blackNeis;
	    }
	 }
	 double fracBlack = static_cast<double>( blackNeis ) / vv.INneis.size();
	 if ( fracBlack > vThresh[ v ] ) {
	    ++newBlack;
	    black[ v ] = true;
	    gray[ v ] = false;
	 }
      }
   }
   
   return newBlack;
}

size_t updateGray( myGraph& g, vector<bool>& gray, vector<bool>& black) {
   size_t newGray = 0;

   for (node_id v = 0; v < g.n; ++v) {
      tinyNode& vv = g.adjList[v];
      if (black[v]) {
	 for ( size_t i = 0; i < vv.OUTneis.size(); ++i) {
	    node_id nei = vv.OUTneis[i].target;
	    if ( !( black[nei] || gray[ nei ] ) ) {
	       gray[nei] = true;
	       ++newGray;
	    }
	 }
      }
   }
   
   return newGray;
}

size_t margForward( size_t& nEvals,
		    myGraph& g,
		    vector< bool > ss,
		    node_id v ) {
   //ss already turned on and propagated forward
   //propagate from v
   ++nEvals;
   vector< bool > gray( g.n, false );
   if (ss[v])
      return 0;
   
   ss[v] = true;
   size_t margBlack = 1;
   
   size_t newBlack;
   do {
      updateGray( g, gray, ss );
      //cerr << newGray << endl;
      newBlack = updateBlack( g, gray, ss);
      margBlack += newBlack;
   } while (newBlack > 0);
   
   return margBlack;
}

size_t func( size_t& nEvals,
	     myGraph& g,
	     vector< bool >& ss,
	     vector< bool >* outblack = NULL) {
   ++nEvals;
   
   vector< bool > black( g.n, false );
   vector< bool > gray( g.n, false );

   size_t totBlack = 0;
   for (node_id u = 0; u < g.n; ++u) {
      if (ss[u]) {
	 black[u] = true;
	 ++totBlack;
      }
   }

   size_t newBlack;
   do {
      updateGray( g, gray, black );
      //cerr << newGray << endl;
      newBlack = updateBlack( g, gray, black);
      totBlack += newBlack;
   } while (newBlack > 0);

   if (outblack != NULL)
      (*outblack).swap( black );
   
   return totBlack;
}

size_t func_plain( size_t& nEvals,
		   myGraph& g,
		   vector< bool >& ss ) {
   ++nEvals;
   vector < bool > black( g.n, false );
   vector < size_t > blackNeis( g.n, 0 );
   queue< size_t > Q;
   size_t fVal = 0;
   for (size_t i = 0; i < ss.size(); ++i) {
      if (ss[i])
	  Q.push( i );
      
   }

   while (!Q.empty()) {
      size_t v = Q.front();
      black[ v ] = true;
      ++fVal;
      
      Q.pop();
      tinyNode& vv = g.adjList[v];
      for (size_t i = 0; i < vv.OUTneis.size(); ++i) {
	 node_id nei = vv.OUTneis[i].target;
	 if (!black[nei]) {
	    ++blackNeis[ nei ];
	    double fracBlack = static_cast<double>( blackNeis[nei] ) / g.adjList[ nei ].INneis.size();
	    if (fracBlack > vThresh[ nei ]) {
	       black[nei] = true;
	       Q.push( nei );
	    }
	 }
      }
   }

   return fVal;
}

size_t func_alt( size_t& nEvals,
		 myGraph& g,
		 vector< size_t >& ss ) {
   ++nEvals;
   vector < bool > black( g.n, false );
   vector < size_t > blackNeis( g.n, 0 );
   queue< size_t > Q;
   size_t fVal = 0;
   for (size_t i = 0; i < ss.size(); ++i) {
      if (!black[ss[i]]) {
	 Q.push( ss[i] );      
	 black[ ss[i ] ] = true;
      }
   }

   while (!Q.empty()) {
      size_t v = Q.front();
      black[ v ] = true;
      ++fVal;
      
      Q.pop();
      tinyNode& vv = g.adjList[v];
      for (size_t i = 0; i < vv.OUTneis.size(); ++i) {
	 node_id nei = vv.OUTneis[i].target;
	 if (!black[nei]) {
	    ++blackNeis[ nei ];
	    double fracBlack = static_cast<double>( blackNeis[nei] ) / g.adjList[ nei ].INneis.size();
	    if (fracBlack > vThresh[ nei ]) {
	       black[nei] = true;
	       Q.push( nei );
	    }
	 }
      }
   }

   return fVal;
}


typedef tArgs< myGraph > Args;

void runTest( Args& args ) {
   ifstream ifs( args.testVectorFName );
   node_id tmp;
   vector< bool > ss( args.g.n, false );
   while (ifs >> tmp) {
      ss[ tmp ] = true;
   }

   size_t nEvals = 0;

   (*args.logg) << "Value: " << func( nEvals, args.g, ss ) << endL;
}


size_t greedy( Args& args ) {
   vector< bool > ss( args.g.n, false );
   vector< size_t > ssId;
   
   (*args.logg) << "Starting Greedy..." << endL;

   size_t nEvals = 0;
   size_t valS = 0;
   size_t maxInc;
   node_id maxV;
   
   for (size_t i = 0; i < args.k; ++i) {
      maxInc = 0;
      maxV = 0;
      size_t inc;
      for (node_id v = 0; v < args.g.n; ++v) {
	 if (!ss[v]) {
	    ss[v] = true;
	    inc = func_plain( nEvals, args.g, ss );
	 
	    if ( inc > maxInc ) {
	       maxInc = inc;
	       maxV = v;
	    }
	    ss[v] = false;
	 }
      }

      ss[ maxV ] = true;
      ssId.push_back( maxV );
      //(*args.logg) << "Adding " << maxV << ", with val " << maxInc << endL;
	       
   }

   valS = func_plain( nEvals, args.g, ss );
   // valS = 
   // if (valS != func_alt( nEvals, args.g, ssId )) {
   //    (*args.logg) << ERROR << "Oracles do not agree." << endL;
   //    (*args.logg) << "f2 = " << func_alt( nEvals, args.g, ssId ) << endL;
   //    (*args.logg) << "f1 = " << func( nEvals, args.g, ss ) << endL;
   //    return 0;
   // }

   (*args.logg) << "Greedy value: " << valS << endL;

   return valS;
}

   
size_t greedyRaw( Args& args ) {
   vector< bool > ss( args.g.n, false );

   vector< uint16_t > vSol( args.g.n, 0 );

   (*args.logg) << "Starting Greedy..." << endL;

   size_t nEvals = 0;
   size_t valS = 0;
   size_t maxInc;
   node_id maxV;
   for (size_t i = 0; i < args.k; ++i) {
      maxInc = 0;
      maxV = 0;
      size_t inc;
      for (node_id v = 0; v < args.g.n; ++v) {
	 if (!ss[v]) {
	    ss[ v ] = true;
	 
	    inc = func( nEvals, args.g, ss );
	    //cerr << v << ' ' << inc << endl;
	 
	    if ( inc > maxInc ) {
	       maxInc = inc;
	       maxV = v;

	    }

	    ss[ v ] = false;
	 }
      }

      ss[ maxV ] = true;
      vSol[ maxV ] = 1;
      //valS += maxInc;
      valS = func( nEvals, args.g, ss );
      (*args.logg) << "Adding " << maxV << ", with inc " << maxInc << endL;
	       
   }

   (*args.logg) << "Greedy value: " << valS << endL;
   args.g.nodeLevels = vSol;

   return valS;
}

size_t greedyForward( vector< vector< bool > >& pool,
		      vector< size_t >& vals,
		      size_t pos,
		      myGraph& g,
		      size_t& nEvals ) {
   vector<bool > ss = pool[ pos ];
   
   size_t maxInc = 0;
   size_t maxV = 0;
   size_t inc;
   size_t valS;
   for (node_id v = 0; v < g.n; ++v) {
      if (!ss[v]) {
	 ss[ v ] = true;
	    
	 inc = func( nEvals, g, ss );
	 
	 if ( inc > maxInc ) {
	    maxInc = inc;
	    maxV = v;
	 }

	 ss[ v ] = false;
      }
   }

   ss[ maxV ] = true;
   
   valS = maxInc;

   if ( pos == pool.size() - 1 ) {
      pool.push_back( ss );
      vals.push_back( valS );
   } else {
      size_t newVal = valS;
      if (newVal > vals[ pos + 1 ] ) {
	 pool[ pos + 1 ] = ss;
	 vals[ pos + 1 ] = newVal;
      }
   }

   return vals[ pos + 1 ];
}

size_t lazyGreedyForward( vector< vector< bool > >& basePool,
			  vector< size_t >& vals,
			  size_t pos,
			  myGraph& g,
			  size_t& nEvals,
			  size_t s,
			  uniform_int_distribution< size_t >& dist ) {
   //vector< vector< bool > >& pool ) {
   vector<bool > ss = basePool[ pos ]; //seed set
   
   size_t maxInc = 0;
   size_t maxV = 0;
   size_t inc;

   vector< bool > R( g.n, false );

   for (size_t i = 0; i < s; ++i) {
      //Add an element to R
      size_t sampled;
      do {
	 sampled = dist( gen );
      } while ( false );

      R[ sampled ] = true;
   }
   
   for (node_id v = 0; v < g.n; ++v) {
      if (R[ v ] && (!ss[v])) {
	 ss[ v ] = true;
	    
	 inc = func( nEvals, g, ss );
	 
	 if ( inc > maxInc ) {
	    maxInc = inc;
	    maxV = v;
	 }

	 ss[ v ] = false;
      }
   }

   size_t newVal = maxInc;
   if (newVal > vals[ pos + 1 ] ) {
      ss[maxV] = true;
      basePool[ pos + 1 ] = ss;
      vals[ pos + 1 ] = newVal;

      return newVal;
   }
   
   return vals[ pos + 1 ];
}





size_t greedyBackward( vector< vector< bool > >& pool,
		       vector< size_t >& vals,
		       size_t pos,
		       myGraph& g,
		       size_t& nEvals ) {
   vector< bool > ss = pool[ pos ];
   vector< node_id > ssIds;
   for (node_id v = 0; v < g.n; ++v) {
      if (ss[ v ] == 1) {
	 ssIds.push_back( v );
      }
   }

   size_t maxCov = 0;
   node_id maxV = 0;
   for (size_t i = 0; i < ssIds.size(); ++i) {
      node_id u = ssIds[ i ];
      ss[ u ] = false;
	    	    
      //compute value of new ss
      size_t cov = func( nEvals, g, ss );
      if (cov > maxCov) {
	 maxCov = cov;
	 maxV = u;
      }
      ss[u] = true;
   }

   size_t newVal = maxCov;
   ss[ maxV ] = false;
  if (newVal > vals[ pos - 1 ] ) {
      pool[ pos - 1 ] = ss;
      vals[ pos - 1 ] = newVal;
   }
	 
   return vals[ pos - 1 ];
} 

void initThreshs( Args& args ) {
   mt19937 gen; // default seed, generate the same instance each time
   vThresh.assign( args.g.n, 0.0 );
   for (size_t i = 0; i < args.g.n; ++i) {
      vThresh[ i ] = uni( gen );
   }
}

void mutate( vector< bool >& ss,
	     vector< size_t >& ssId,
	     size_t n, double weight, mt19937& gen2 ) {
   binomial_distribution< size_t > binDist( n, 1.0 / n );
   uniform_int_distribution<size_t> allDist(0, n - 1);
   size_t nMutate = binDist( gen2 );
   
   vector< size_t > mutateIdx;

   if (nMutate >= n)
      nMutate = 1;
   
   for (size_t i = 0; i < nMutate; ++i) {
      size_t idx;
      do {
	 idx = allDist( gen2 );
      } while ( vcontains( mutateIdx, idx ) );

      mutateIdx.push_back( idx );
   }

   for (size_t j = 0; j < nMutate; ++j) {
      size_t& i = mutateIdx[ j ];
      if ( ss[i] ) {
	 ss[i] = false;
	 erase_from_vector( ssId, i );
      } else {
	 ss[i] = true;
	 ssId.push_back( i );
      }
   }
}

bool isStable( myGraph& g,
	       vector< vector< bool > >& pool,
	       vector< size_t >& vals ) {
   size_t nEvals = 0;
   size_t valForward,valBackward;
   valForward = vals[ 1];
   if (greedyForward( pool, vals, 0, g, nEvals) > valForward)
      return false;
   
   for (size_t i = 1; i < pool.size() - 1; ++i) {
      valForward = vals[ i + 1];
      if (greedyForward( pool, vals, i, g, nEvals) > valForward)
	 return false;
      valBackward = vals[ i - 1];
      if (greedyBackward( pool, vals, i, g, nEvals) > valBackward)
	 return false;
      
   }

   valBackward = vals[ pool.size() - 2 ];
   if (greedyBackward( pool, vals, pool.size() - 1, g, nEvals) > valBackward)
      return false;
   
   return true;
}

bool replace( vector< bool >& ss, vector< vector< bool > >& pool,
	      vector< size_t >& vals, size_t& tmpPos, size_t& tmpVal,
	      size_t maxSize, size_t& nEvals, myGraph& g, vector< vector< size_t > >& poolId, vector< size_t >& ssId ) {
   tmpPos = ssId.size();
   tmpVal = 0;
   

   if (tmpPos <= maxSize) {
      size_t valPos = vals[ tmpPos ];
      tmpVal = func_plain( nEvals, g, ss );
      if (tmpVal > valPos) {
	 vals[ tmpPos ] = tmpVal;
	 pool[ tmpPos ] = ss;
	 poolId[tmpPos].swap( ssId );
	 return true;
      }
   }
   return false;
}

size_t runQian( Args& args ) {

   //run Greedy first, for comparison

   size_t valGreedy;
   if (args.valGreedy > 0.0)
      valGreedy = args.valGreedy;
   else {
      valGreedy = greedy( args );
      args.valGreedy = valGreedy;
   }
   
   vector< vector< bool > > pool;
   vector< vector< size_t > > poolId;
   vector< size_t > vals;
   size_t maxVal = 0;
   size_t pos;
   size_t maxSize = 2 * args.k;
   uniform_int_distribution< size_t > dist( 0, maxSize );

   bool boutput = false;
   ofstream ofile;
   if (args.outputFileName != "") {
      ofile.open( args.outputFileName.c_str() );
      boutput = true;
   }
   
   {

      vector< bool > empty( args.g.n, false );
      pool.assign( maxSize + 1, empty );
      vals.assign( maxSize + 1, 0 );
      vector< size_t > emptyId;
      poolId.assign( maxSize + 1, emptyId );
   }

   size_t T = args.g.n * args.k * args.T;
   size_t nEvals = 0;
   size_t tmpPos, tmpVal;

   double outTenth = 0.0;
   size_t iter = 0;
   while (nEvals <= T) {
   
      do {
	 pos = dist( gen );

      } while (pos >= pool.size());

      vector< bool > ss = pool[ pos ];
      vector< size_t > ssId = poolId[ pos ];

      mutate( ss, ssId, args.g.n, 1.0, gen );

      if (ss.size() != pos) {
	 replace( ss,
		  pool,
		  vals,
		  tmpPos,
		  tmpVal,
		  maxSize,
		  nEvals,
		  args.g, poolId, ssId );
      }
      
      if ( ( tmpVal > maxVal ) && (tmpPos <= args.k) ) {
	 maxVal = tmpVal;
	 cerr << "\r                                                 \r";
	 double prog = nEvals / (static_cast<double>(args.k) * args.g.n);
	 cerr << prog << '\t' << maxVal / ( (double) valGreedy)
	      << ' ' << pool.size();

	 
      }

      if (boutput) {
	 bool outputNow = false;
	 double prog = nEvals / (static_cast<double>(args.k) * args.g.n);
	 while (outTenth < prog) {
	    outTenth += 0.1;
	    outputNow = true;
	 }
	 if (outputNow) {
	    mtx.lock();
	    simpleResults.init( outTenth - 0.1 );
	    simpleResults.add( outTenth - 0.1, maxVal / ( (double) valGreedy) );
	    //ofile << args.k << '\t' << iter << '\t' << prog << '\t' << outTenth - 0.1 << '\t' 
	    //	  << maxVal / ( (double) valGreedy)
	    //	  << ' ' << args.alg << ' ' << args.ell << '\n';
	    mtx.unlock();
	 }
      }

      ++iter;
   }

   cerr << '\n';
   
   (*args.logg) << "Qian Pareto: " << maxVal << endL;

   if (boutput)
      ofile.close();
   
   return maxVal;
}

size_t lazyGreedyBackward( vector< vector< bool > >& pool,
			   vector< size_t >& vals,
			   size_t pos,
			   myGraph& g,
			   size_t& nEvals,
			   size_t s,
			   uniform_int_distribution< size_t >& dist ) {
   if (s >= pos) {
      return greedyBackward( pool, vals, pos, g, nEvals );
   }

   vector< bool > R( g.n, false );

   vector< bool > ss = pool[ pos ];
   vector< node_id > ssIds;
   for (node_id v = 0; v < g.n; ++v) {
      if (ss[ v ] == 1) {
	 ssIds.push_back( v );
      }
   }
   
   for (size_t i = 0; i < s; ++i) {
      //Add an element to R
      size_t sampled;
      do {
	 sampled = dist( gen );
      } while ( sampled >= ssIds.size() );

      R[ ssIds[ sampled ] ] = true;
   }
   


   size_t maxCov = 0;
   node_id maxV = 0;
   for (size_t i = 0; i < ssIds.size(); ++i) {
      if (R[ ssIds[i] ]) {
	 node_id u = ssIds[ i ];
	 ss[ u ] = false;
	    	    
	 //compute value of new ss
	 size_t cov = func( nEvals, g, ss );
	 if (cov > maxCov) {
	    maxCov = cov;
	    maxV = u;
	 }
	 ss[u] = true;
      }
   }

   size_t newVal = maxCov;
   ss[ maxV ] = false;
   if (newVal > vals[ pos - 1 ] ) {
      pool[ pos - 1 ] = ss;
      vals[ pos - 1 ] = newVal;
   }
	 
   return vals[ pos - 1 ];
}





size_t runLazyPS( Args& args ) {
   (*args.logg) << "Starting LAPS..." << endL;

   if (args.g.lmax != 1) {
      args.g.lmax = 1;

      if (args.K > 0)
	 args.k = args.K;
   }

   double p = args.p;
   
   uniform_int_distribution<size_t> distLazy(0, args.g.n);
   size_t lazySize = static_cast<double>( args.g.n ) / args.k * log( 1.0 / args.epsi );
   size_t sampIter;
   if (args.ell == 0)
      sampIter = 1;
   else
      sampIter = lazySize / args.ell + 1;

   (*args.logg) << "lazySize = " << lazySize << endL;
   (*args.logg) << "sampIter = " << sampIter << endL;
   
   bool boutput = false;
   ofstream ofile;
   if (args.outputFileName != "") {
      ofile.open( args.outputFileName.c_str() );
      boutput = true;
   }

   size_t valGreedy;
   if (args.valGreedy > 0.0)
      valGreedy = args.valGreedy;
   else {
      valGreedy = greedy( args );
      args.valGreedy = valGreedy;
      (*args.logg) << "Greedy finished with val: " << valGreedy << endL;
   }

   vector< vector< bool > > pool;
   //vector< vector< bool > > basePool;
   vector< size_t > vals;
   vector< size_t > sampsForPool;
   vector<bool> ss;
   size_t k = args.k;
   size_t n = args.g.n;
   size_t maxSize = 2 * k;
   size_t inductiveBin = 0;
   
   {
      vector< bool > empty( args.g.n, false );
      pool.assign( maxSize + 1, empty );
      vals.assign( maxSize + 1,  0 );
      //basePool = pool;
      sampsForPool = vals;
   }

   size_t maxVal = 0;
   size_t nEvals = 0;
   size_t toss, tmpVal, pos;
   size_t T = 50 * n * k;
   uniform_int_distribution< size_t > selectDist( 0, maxSize );
   uniform_int_distribution< size_t > coinToss( 0, 1 );

   size_t iter = 0;
   double outTenth = 0.0;
   bool bcont;
   vector< size_t > notConverged;
   size_t nEvalsAtRestart = 0;
   while (nEvals <= T) {
      
      

      do {
	 if (!args.randomSelection) {
	    if (nEvals - nEvalsAtRestart > 2 * n * k) {
	       //let's restart
	       nEvalsAtRestart = nEvals;
	       {
		  vector< bool > empty( args.g.n, false );
		  pool.assign( maxSize + 1, empty );
		  vals.assign( maxSize + 1,  0 );
		  //basePool = pool;
		  sampsForPool = vals;
		  inductiveBin = 0;
	       }
	    }

	    notConverged.clear();
	    for (size_t i = 0; i < pool.size(); ++i) {
	       if (sampsForPool[ i ] <= lazySize) 
		  notConverged.push_back( i );
	    }

      
	    if (notConverged.size() == 0) {
	       (*args.logg) << "Pareto converged." << endL;
	       pos = selectDist(gen);
	       break;
	    }
	    
	    if (uni( gen ) <= p) {
	       if (inductiveBin < k) {
		  pos = inductiveBin;
	       } else {
		  pos = notConverged[ 0 ];
	       }
	    } else {
	       pos = notConverged[ 0 ];
	    }
	    
	    break;
	 }
	 
	 bcont = false;
	 if (uni( gen ) < p) {
	    if (inductiveBin < k)
	       pos = inductiveBin;
	    else
	       pos = selectDist(gen);
	 } else {
	    pos = selectDist(gen);
	 }

	 // if (sampsForPool[ pos ] > lazySize ) {
	 //    if (!args.randomSelection) {
	  
	 // 	  bcont = true;
	 //    }
	 // }
	 
	 
	 if (pos > 0 && vals[pos] == 0)
	    bcont = true;
	 
      } while( bcont  ); //only select from nontrivial elements and bias towards "unconverged" elements
      
      if (pos == 0) {
	 toss = 1;
      } else {
	 if (pos == maxSize) {
	    toss = 0;
	 } else {
	    toss = coinToss( gen );
	 }
      }

      if (toss == 1) {
	 //we know pos < maxSize
	 size_t oldVal = vals[ pos + 1 ];
	 tmpVal = lazyGreedyForward( pool,
				     vals,
				     pos,
				     args.g,
				     nEvals,
				     sampIter,
				     distLazy );

	 if (tmpVal > oldVal) {
	    if (pos + 1 != inductiveBin || inductiveBin > k) {
	       sampsForPool[ pos + 1 ] = 0;
	    } 
	 }

	 if ((tmpVal > maxVal) && (pos + 1 <= k) ) {
	    maxVal = tmpVal;
	    ss = pool[ pos + 1 ];

	    cerr << "\r                                                 \r";
	    double prog = nEvals / (static_cast<double>(k) * n);
	    cerr << prog << '\t' << maxVal / ( (double) valGreedy) << '\t' << inductiveBin;
		       
	 }

	 sampsForPool[ pos ] += sampIter;

	 if (sampsForPool[ pos ] >= lazySize) {
	    if (pos == inductiveBin)
	       ++inductiveBin;
	 }
	 
      } else {
	 size_t oldVal = vals[ pos - 1 ];
	 tmpVal = lazyGreedyBackward( pool,
				      vals,
				      pos,
				      args.g,
				      nEvals,
				      sampIter,
				      distLazy );
	 if (tmpVal > oldVal) {
	    if (pos - 1 != inductiveBin || inductiveBin > k) {
	       sampsForPool[ pos - 1 ] = 0;
	    } 
	 }

	 if ((tmpVal > maxVal) && (pos - 1 <= k) ) {
	    maxVal = tmpVal;
	    ss = pool[ pos - 1 ];

	    cerr << "\r                                                 \r";
	    double prog = nEvals / (static_cast<double>(k) * n);
	    cerr << prog << '\t' << maxVal / ( (double) valGreedy) << '\t' << inductiveBin;
		       
	 }
      }

      if (boutput) {
	 //output every tenth of a greedy iteration
	 bool outputNow = false;
	 double prog = nEvals / (static_cast<double>(k) * n);
	 while (outTenth < prog) {
	    outTenth += 0.1;
	    outputNow = true;
	 }
	 if (outputNow) {
	    simpleResults.init( outTenth - 0.1 );
	    simpleResults.add( outTenth - 0.1, maxVal / ( (double) valGreedy) );
	    //ofile << args.k << '\t' << iter << '\t' << prog << '\t' << outTenth - 0.1 << '\t' 
	    //<< maxVal / ( (double) valGreedy)
	    //<< ' ' << args.alg << ' ' << args.ell << '\n';
	 }
      }

      ++iter;
   }

   size_t totCovered = func( nEvals, args.g, ss );

   (*args.logg) << "LAPS value: " << totCovered << endL;
   if (boutput) {
      ofile.flush();
      ofile.close();
   }

   // cerr << endl;
   // for (size_t i = 0; i < maxSize; ++i) {
   //    cerr << vals[i] << ' ';
   // }
   // cerr << endl;

   // for (size_t i = 0; i < maxSize; ++i) {
   //    cerr << i << ": ";
   //    for (size_t j = 0; j < args.g.n; ++j) {
   // 	 if (basePool[i][j])
   // 	    cerr << j << ' ';
   //    }
   //    cerr << endl;
   // }
   
   
   // if (isStable( args.g, basePool, vals )) {
   //    cerr << "basepool is stable\n";
   // } else {
   //    cerr << "basepool is unstable\n";
   // }
   
   return totCovered;
}


size_t runGpareto( Args& args ) {
   
   if (args.g.lmax != 1) {
      args.g.lmax = 1;

      if (args.K > 0)
	 args.k = args.K;
   }

   bool boutput = false;
   ofstream ofile;
   if (args.outputFileName != "") {
      ofile.open( args.outputFileName.c_str() );
      boutput = true;
   }

   size_t valGreedy = greedy( args );

   vector< vector< bool > > pool;
   vector< size_t > vals;
   vector<bool> ss;
   {

      vector< bool > empty( args.g.n, false );
      pool.push_back( empty );
      vals.push_back( 0 );
      
   }

   size_t k = args.k;
   size_t maxSize = 2 * k; 
   size_t n = args.g.n;
   size_t T = args.g.n * args.k * 50;
   
   size_t nEvals = 0;
   uniform_int_distribution< size_t > dist( 0, maxSize );
   uniform_int_distribution< size_t > coinToss( 0, 1 );
   vector< bool > forward( maxSize + 1, false );
   vector< bool > backward( maxSize + 1, false );
   forward[ maxSize ] = true;
   backward[ 0 ] = true;
   size_t pos, toss, cov;
	 
   size_t maxCovered = 0;
   size_t backImps = 0;
   vector< size_t > notConverged;
   //for (size_t t = 0; t < T; ++t) {
   while (nEvals < T) {
      notConverged.clear();
      for (size_t i = 0; i < pool.size(); ++i) {
	 if (!forward[i]) {
	    notConverged.push_back( i );

	 } else {
	    if (!backward[i]) {
	       notConverged.push_back( i );
	    }
	 }
      }

      if (notConverged.size() == 0) {
	 (*args.logg) << "Pareto converged." << endL;
	 break;
      }
      
    
	    
      do {
	 //pos = dist( gen );
	 pos = 0;

      } while (pos >= notConverged.size());

      pos = notConverged[ pos ];

      if (forward[pos])
	 toss = 0;
      else
	 toss = 1;
      
      if (toss == 1) {
	 if (!args.randomSelection)
	    forward[ pos ] = true;

	 size_t valNext;
	 if (pos + 1 < vals.size())
	    valNext = vals[ pos + 1 ];
	 else
	    valNext = 0;

	 cov = greedyForward( pool, vals, pos, args.g, nEvals );
 	 
	 if (cov > valNext) {
	    if (pos + 1 < maxSize)
	       forward[ pos + 1 ] = false;
	    backward[ pos + 1 ] = false;

	    if (cov > maxCovered) {
	       if (pos + 1 <= k) {
		  maxCovered = cov;
		  ss = pool[ pos + 1 ];

		  cerr << "\r                                                 \r";
		  double prog = nEvals / (static_cast<double>(k) * n);
		  cerr << prog << '\t' << maxCovered / ( (double) valGreedy)
		       << ' ' << pool.size() << ' ' << backImps;

		  
	       }

	    }
	  
	 }
      } else {
	 if (!args.randomSelection)
	    backward[pos] = true;
	 
	 size_t valPrev = vals[ pos - 1 ];
	 cov = greedyBackward( pool, vals, pos, args.g, nEvals );

	 if ( cov  > valPrev ) {
	    forward[ pos - 1 ] = false;
	    if (pos - 1 > 0)
	       backward[ pos - 1 ] = false;
		  
	    ++backImps;


	    if (cov > maxCovered) {
	       if (pos - 1 <= k) {
		  maxCovered = cov;
		  ss = pool[ pos - 1 ];

		  cerr << "\r                                                 \r";
		  double prog = nEvals / (static_cast<double>(k) * n);
		  cerr << prog << '\t' << maxCovered / ( (double) valGreedy)
		       << ' ' << pool.size() << ' ' << backImps;
	       }
	    }


	 }
	       
      }

      if (boutput) {
	 //	 if (nEvals % args.k == 0) {
	 double prog = nEvals / (static_cast<double>(k) * n);
	 simpleResults.init( prog );
	 simpleResults.add( prog, maxCovered / ( (double) valGreedy) );
	 //ofile << args.k << '\t' << prog << '\t' << maxCovered / ( (double) valGreedy)
	 //<< ' ' << pool.size() << ' ' << backImps << endl;
	    //}
      }
   }

   //return
   size_t totCovered = func( nEvals, args.g, ss );

   (*args.logg) << "Pareto value: " << totCovered << endL;
   if (boutput)
      ofile.close();
   return totCovered;

   
}


void print_help() {
   cout << "Options: " << endl;
  cout << "-g <graph filename in binary format>" << endl
       << "-k <total budget>" << endl
       << "-G (run Stochastic-Greedy)" << endl
       << "-M (run BLPO)" << endl 
       << "-L (run BLPO+)" << endl
       << "-Q (run POSS)" << endl
       << "-x <max number of threads (default: 1)> " << endl
       << "-q (quiet mode, suppress logging)" << endl
       << "-o <output filename>" << endl;
}

void parseArgs( int argc, char** argv, Args& arg ) {
  int c;
  extern char *optarg;

  if (argc == 1) {
    print_help();
    exit( 2 );
  }

  string sarg;
  while ((c = getopt( argc, argv, ":g:DK:OT:k:p:vx:PHSQEBMN:GLl:qto:d:rhb:") ) != -1) {
    switch(c) {
    case 'b':
       arg.logFileName.assign( optarg );
       break;
    case 'o':
       arg.outputFileName.assign( optarg );
       break;
    case 'p':
       sarg.assign( optarg );
       arg.p = stod( sarg );
       break;
    // case 'T':
    //    arg.alg = Algs::TEST;
    //    sarg.assign( optarg );
    //    arg.testVectorFName = sarg;
    //    break;
    case 'q':
       arg.quiet = true;
       break;
    case 'h':
       arg.lazySearchHeuristic = true;
       break;
    case 'r':
       arg.randomSelection = true;
       break;
    case 'x':
       sarg.assign( optarg );
       arg.nThreads = stoi( sarg );
       break;
    case 'l':
       sarg.assign( optarg );
       arg.ell = stoi( sarg );
       break;
    case 'k':
       sarg.assign( optarg );
       arg.k = stoi( sarg );
       break;
    case 'K':
       sarg.assign( optarg );
       arg.K = stoi( sarg );
       break;
    case 'N':
       sarg.assign( optarg );
       arg.N = stoi( sarg );
       break;
    case 'g':
      //graph specification
      arg.graphFileName.assign( optarg );
      break;
    case 'G':
       arg.alg = Algs::GREEDY;
       break;
    case 'P':
       arg.alg = Algs::GPARETO;
       break;
    case 'L':
       arg.alg = Algs::LAPS;
       break;
    case 'M':
       arg.alg = Algs::LAPS;
       arg.randomSelection = true;
       break;
    case 'Q':
       arg.alg = Algs::QIAN;
       break;
    case 'T':
       sarg.assign( optarg );
       arg.T = stoi( sarg );
       break;
    case '?':
      print_help();
      exit( 0 );
      break;
    }
  }

  arg.g.lmax = 1;

  
  if (arg.K > 0) {
     arg.k = arg.K * arg.g.lmax;
     
  }

  if (arg.logFileName != "") {
     arg.ofLog = new ofstream( arg.logFileName.c_str() );
     arg.logg = new Logger( INFO, *arg.ofLog, true );
  } else {
     arg.logg = new Logger();
  }
  
}

void readGraph( Args& args ) {
   args.g.read_bin( args.graphFileName );
   (*args.logg) << "n = " << args.g.n << endL;
   initThreshs( args );
}

void runAlg( Args& args ) {
   for (size_t i = 0; i < args.N; ++i) {
      (*args.logg) << "Repetition = " << i << endL;
      switch (args.alg) {
      case Algs::GREEDY:
	 lazy_greedy< myGraph >( args, func_alt );
	 break;

      case Algs::GPARETO:
	 runGpareto( args );
	 break;

      case Algs::LAPS:
	 //runLazyPS( args );
	 rps_main< myGraph >( args, func_alt );
	 break;

      case Algs::LAPSFB:
	 //runLazyPS( args );
	 rps_main_fb< myGraph >( args, func_alt );
	 break;

      case Algs::LAPSLM:
	 //runLazyPS( args );
	 rps_main_lm< myGraph >( args, func_alt );
	 break;

      case Algs::LOCAL:
	 //runLazyPS( args );
	 local_search< myGraph >( args, func_alt );
	 break;

      case Algs::QIAN:
	 //runQian( args );
	 run_qian< myGraph >( args, func_alt );
	 break;
      // case Algs::TEST:
      // 	 runTest( args );
      // 	 break;
      default:
	 (*args.logg) << "Unrecognized algorithm." << endL;
	 break;
      }
   }
   
}

void runAlgMaster( Args& args ) {
   if (args.nThreads > 1 && args.alg != OPT) {
      (*args.logg) << "Initializing " << args.nThreads << " threads..." << endL;
      vector< Args > threadArgs( args.nThreads, args );
      thread* workThreads = new thread[ args.nThreads ];
      for (size_t i = 0; i < args.nThreads; ++i) {
	 
	 if (i < args.nThreads - 1) {
	    threadArgs[i].N = args.N / args.nThreads;
	 } else {
	    threadArgs[i].N = args.N - (args.nThreads - 1)* args.N / args.nThreads;
	 }
	 workThreads[i] = thread( runAlg,
				  ref( threadArgs[i] ) );
				 
      }


      for (size_t i = 0; i < args.nThreads; ++i) {
	 workThreads[i].join();
      }

      delete [] workThreads;
      
   } else {
      runAlg( args );
   }
}

void outputResults( Args& args, ostream& os ) {
   args.print( os );
      
   os << "#results: <frac of Greedy iter> <mean> <stdDev>" << endl;

   simpleResults.print( os, true );
}

int main(int argc, char** argv) {
   Args args;
   parse_args< myGraph >( argc, argv, args );
   readGraph( args );
   runAlgMaster( args );
   if (args.outputFileName != "") {
      ofstream of( args.outputFileName.c_str(), ofstream::out | ofstream::app );

      outputResults( args, of );
      of.close();
   }
   // } else {
   //       if (args.alg != Algs::TEST) 
   // 	 outputResults( args, cout );
   //    }

   return 0;
}
